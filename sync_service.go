package account

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/canopei/account/config"
	accountPb "bitbucket.org/canopei/account/protobuf"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/mindbody"
	mbHelpers "bitbucket.org/canopei/mindbody/helpers"
	clientMb "bitbucket.org/canopei/mindbody/services/client"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
)

// SyncService is the implementation of a sync service
type SyncService struct {
	Logger               *logrus.Entry
	ClientMBClient       *clientMb.Client_x0020_ServiceSoap
	Queue                *nsq.Producer
	ElasticsearchService *elasticsearch.Service
	AccountService       accountPb.AccountServiceClient
	ClientService        accountPb.ClientServiceClient
	SiteService          sitePb.SiteServiceClient
	StaffService         staffPb.StaffServiceClient
	ClassService         classPb.ClassServiceClient
	MindbodyConfig       *config.MindbodyConfig
}

// NewSyncService creates a new SyncService instance
func NewSyncService(
	logger *logrus.Entry,
	clientMBClient *clientMb.Client_x0020_ServiceSoap,
	queue *nsq.Producer,
	elasticsearchService *elasticsearch.Service,
	accountService accountPb.AccountServiceClient,
	clientService accountPb.ClientServiceClient,
	siteService sitePb.SiteServiceClient,
	staffService staffPb.StaffServiceClient,
	classService classPb.ClassServiceClient,
	mindbodyConfig *config.MindbodyConfig,
) *SyncService {
	return &SyncService{
		Logger:               logger,
		ClientMBClient:       clientMBClient,
		Queue:                queue,
		ElasticsearchService: elasticsearchService,
		AccountService:       accountService,
		ClientService:        clientService,
		SiteService:          siteService,
		StaffService:         staffService,
		ClassService:         classService,
		MindbodyConfig:       mindbodyConfig,
	}
}

// SyncSiteClients synchronizes the clients for the given site (UUID) to DB and ES from MB
func (ss *SyncService) SyncSiteClients(siteUUID string) error {
	ss.Logger.Infof("Will sync site UUID: %s", siteUUID)

	// Get the site from DB
	dbSite, err := ss.SiteService.GetSite(context.Background(), &sitePb.GetSiteRequest{Uuid: siteUUID})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the site by UUIDs: %v", err)
		return fmt.Errorf("Cannot fetch the site by UUIDs: %v", err)
	}

	ss.Logger.Debugf("Syncing clients for site MB ID %d.", dbSite.MbId)

	// Sync the clients page by page
	mbRequest := mbHelpers.CreateClientMBRequest([]int32{dbSite.MbId}, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)

	credsUsername := "_" + ss.MindbodyConfig.SourceName
	credsPassword := ss.MindbodyConfig.SourcePassword
	// This is an exception ONLY for the Sandbox Site, where we cannot authorize our master API creds
	if dbSite.MbId == -99 {
		credsUsername = "Siteowner"
		credsPassword = "apitest1234"
	}

	mbRequest.UserCredentials = &clientMb.UserCredentials{Username: credsUsername, Password: credsPassword, SiteIDs: mbRequest.SourceCredentials.SiteIDs}

	totalPages := int32(1)
	foundClients := []int64{}
	pageSize := 1000
	for currentPage := 1; int32(currentPage) <= totalPages; currentPage++ {
		// Get the clients from Mindbody
		mbRequest.CurrentPageIndex = &mindbody.CustomInt32{Int: int32(currentPage - 1)}
		mbRequest.PageSize = &mindbody.CustomInt32{Int: int32(pageSize)}

		mbClientsResponse, err := ss.ClientMBClient.GetClients(&clientMb.GetClients{Request: &clientMb.GetClientsRequest{
			MBRequest:  mbRequest,
			SearchText: "",
		}})
		if err != nil {
			ss.Logger.Errorf("Cannot fetch the clients from Mindbody: %v", err)
			return fmt.Errorf("Cannot fetch the clients from Mindbody: %v", err)
		}
		mbClientsResult := mbClientsResponse.GetClientsResult
		if mbClientsResult.ErrorCode.Int != 200 {
			ss.Logger.Errorf("Mindbody API response error: %s", mbClientsResult.Message)
			return fmt.Errorf("Mindbody API response error: %s", mbClientsResult.Message)
		}
		totalPages = mbClientsResult.TotalPageCount.Int

		ss.Logger.Infof("Syncing %d clients (page %d of %d)...", len(mbClientsResult.Clients.Client), currentPage, totalPages)

		if len(mbClientsResult.Clients.Client) > 0 {
			for _, c := range mbClientsResult.Clients.Client {
				foundClients = append(foundClients, c.UniqueID.Int)
			}

			err := ss.SyncClients(siteUUID, mbClientsResult.Clients.Client)
			if err != nil {
				ss.Logger.Errorf("Failed to fully sync page.")
				return err
			}
		}
	}

	ss.Logger.Infof("Synced %d clients in total.", len(foundClients))

	ss.Logger.Debugf("Cleaning removed clients...")
	_, err = ss.ClientService.DeleteClientsForSiteIDExceptMBIDs(context.Background(), &accountPb.DeleteClientsForSiteIDExceptMBIDsRequest{
		SiteID: dbSite.Id,
		MBIDs:  foundClients,
	})
	if err != nil {
		ss.Logger.Errorf("Failed to clean the removed clients from DB.")
		return err
	}

	return nil
}

// SyncClients syncs the given MB clients to DB and ES
func (ss *SyncService) SyncClients(siteUUID string, mbClients []*clientMb.Client) error {
	// Get the site from DB
	dbSite, err := ss.SiteService.GetSite(context.Background(), &sitePb.GetSiteRequest{Uuid: siteUUID})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the site by UUID: %v", err)
		return fmt.Errorf("Cannot fetch the site by UUID: %v", err)
	}

	mbIDs := []int64{}
	for _, c := range mbClients {
		mbIDs = append(mbIDs, c.UniqueID.Int)
	}

	// Fetch the clients from DB
	dbClientsResult, err := ss.ClientService.GetClientsByMBID(context.Background(), &accountPb.GetClientsByMBIDRequest{
		SiteId: dbSite.Id,
		MbIds:  mbIDs,
	})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch staff members from DB: %v", err)
		return fmt.Errorf("Cannot fetch staff members from DB: %v", err)
	}

	ss.Logger.Debugf("Got %d clients from DB.", len(dbClientsResult.Clients))

	dbClients := map[int64]*accountPb.Client{}
	for _, c := range dbClientsResult.Clients {
		dbClients[c.MbId] = c
	}

	for _, mbClient := range mbClients {
		dbClient, ok := dbClients[mbClient.UniqueID.Int]
		if !ok {
			ss.Logger.Infof("Adding new client MB ID %d in DB.", mbClient.UniqueID.Int)

			newClient := MbClientToPbClient(mbClient)
			newClient.SiteId = dbSite.Id

			dbClient, err = ss.ClientService.CreateClient(context.Background(), newClient)
			if err != nil {
				ss.Logger.Errorf("Cannot create client in DB: %v", err)
				continue
			}
		} else {
			currentClient := MbClientToPbClient(mbClient)

			if hasClientChanged(dbClient, currentClient) {
				ss.Logger.Infof("Updating client MB ID %d (%s) in DB.", mbClient.UniqueID.Int, dbClient.Uuid)

				currentClient.Uuid = dbClient.Uuid

				dbClient, err = ss.ClientService.UpdateClient(context.Background(), currentClient)
				if err != nil {
					ss.Logger.Errorf("Cannot update client '%s' in DB: %v", currentClient.Uuid, err)
					continue
				}
			}
		}
	}

	return nil
}

// SyncAccountsClientsPurchasesHistory syncs the given accounts clients purchase history from MB
func (ss *SyncService) SyncAccountsClientsPurchasesHistory(accountUUIDs []string) ([]string, error) {
	var messagesToFinish []string

	ctx := context.Background()

	ss.Logger.Infof("SyncAccountsClientsPurchasesHistory for clients: %s", strings.Join(accountUUIDs, ", "))

	accountsClientsResponse, err := ss.ClientService.GetAccountsClients(ctx, &accountPb.GetAccountsClientsRequest{
		AccountUuids: strings.Join(accountUUIDs, ","),
	})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the accounts clients: %v", err)
		return messagesToFinish, fmt.Errorf("Cannot fetch the accounts clients: %v", err)
	}

	ss.Logger.Infof("Fetching the sites for the clients...")

	// Batch fetch the sites
	siteIDs := []int32{}
	for _, clients := range accountsClientsResponse.AccountClients {
		for _, dbClient := range clients.Clients {
			siteIDs = append(siteIDs, dbClient.SiteId)
		}
	}
	sitesResponse, err := ss.SiteService.GetSites(ctx, &sitePb.GetSitesRequest{Ids: siteIDs})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the sites: %v", err)
		return messagesToFinish, fmt.Errorf("Cannot fetch the sites: %v", err)
	}
	sitesMap := map[int32]*sitePb.Site{}
	for _, dbSite := range sitesResponse.Sites {
		sitesMap[dbSite.Id] = dbSite
	}

	ss.Logger.Debugf("Started looping through the accounts...")

	// Fetch from MB
AccountsLoop:
	for _, accountUUID := range accountUUIDs {
		if accountClients, ok := accountsClientsResponse.AccountClients[accountUUID]; ok {
			if len(accountClients.Clients) > 0 {
				ss.Logger.Infof("Syncing purchase history of account '%s' (%d clients)...", accountUUID, len(accountClients.Clients))
			}
			for _, dbClient := range accountClients.Clients {
				if dbSite, ok := sitesMap[dbClient.SiteId]; ok {
					ss.Logger.Debugf("Fetchig site's %s locations...", dbSite.Uuid)

					// fetch the site locations
					dbLocationsResult, err := ss.SiteService.GetSitesLocations(ctx, &sitePb.GetSitesLocationsRequest{Uuids: dbSite.Uuid})
					if err != nil {
						ss.Logger.Errorf("Cannot fetch the site ID %d locations: %v", dbSite.Id, err)
						continue AccountsLoop
					}
					dbLocationsMap := map[int32]*sitePb.Location{}
					for _, dbLocation := range dbLocationsResult.Locations {
						dbLocationsMap[dbLocation.MbId] = dbLocation
					}

					var lastSyncTime time.Time
					if dbClient.PurchasesSyncedAt != "0000-00-00 00:00:00" {
						lastSyncTime, err = time.Parse(sqlDatetimeFormat, dbClient.PurchasesSyncedAt)
						if err != nil {
							ss.Logger.Errorf("Failed parsing SQL datetime '%s': %v", dbClient.PurchasesSyncedAt, err)
							continue AccountsLoop
						}
					} else {
						lastSyncTime, _ = time.Parse(sqlDatetimeFormat, "1990-01-01 00:00:00")
					}

					ss.Logger.Infof("Syncing purchase history of client '%s' (ID %d) - last sync at '%s'...", dbClient.Uuid, dbClient.Id, lastSyncTime.Format(sqlDatetimeFormat))

					// Sync the clients page by page
					mbRequest := mbHelpers.CreateClientMBRequest([]int32{dbSite.MbId}, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)

					totalPages := int32(1)
					pageSize := 50
					for currentPage := 1; int32(currentPage) <= totalPages; currentPage++ {
						// Get the clients from Mindbody
						mbRequest.CurrentPageIndex = &mindbody.CustomInt32{Int: int32(currentPage - 1)}
						mbRequest.PageSize = &mindbody.CustomInt32{Int: int32(pageSize)}

						mbClientPurchasesResponse, err := ss.ClientMBClient.GetClientPurchases(&clientMb.GetClientPurchases{Request: &clientMb.GetClientPurchasesRequest{
							MBRequest: mbRequest,
							ClientID:  strconv.FormatInt(dbClient.MbId, 10),
							StartDate: &mindbody.CustomTime{Time: lastSyncTime},
						}})
						if err != nil {
							ss.Logger.Errorf("Cannot fetch the client purchase history from Mindbody: %v", err)
							continue AccountsLoop
						}
						mbClientPurchasesResult := mbClientPurchasesResponse.GetClientPurchasesResult
						if mbClientPurchasesResult.ErrorCode.Int != 200 {
							ss.Logger.Errorf("Mindbody API response error: %s", mbClientPurchasesResult.Message)
							continue AccountsLoop
						}
						totalPages = mbClientPurchasesResult.TotalPageCount.Int

						ss.Logger.Infof("Syncing %d client purchases (page %d of %d)...", len(mbClientPurchasesResult.Purchases.SaleItem), currentPage, totalPages)

						if len(mbClientPurchasesResult.Purchases.SaleItem) > 0 {
							purchaseMBIDs := []int64{}
							for _, mbPuchaseResult := range mbClientPurchasesResult.Purchases.SaleItem {
								purchaseMBIDs = append(purchaseMBIDs, mbPuchaseResult.Sale.ID.Int)
							}
							ss.Logger.Debugf("Checking in DB %d purchases...", len(purchaseMBIDs))
							dbPurchases, err := ss.ClientService.GetClientPurchasesByMBID(ctx, &accountPb.GetClientsPurchasesByMBIDRequest{
								SiteId: dbSite.Id,
								MbIds:  purchaseMBIDs,
							})
							if err != nil {
								ss.Logger.Errorf("Cannot fetch purchase for client %d: %v", dbClient.Id, err)
								continue AccountsLoop
							}
							ss.Logger.Debugf("Got %d purchases from DB %d.", len(dbPurchases.ClientPurchases))
							dbPurchasesMap := map[int64]*accountPb.ClientPurchase{}
							for _, dbPurchase := range dbPurchases.ClientPurchases {
								dbPurchasesMap[dbPurchase.SaleMbId] = dbPurchase
							}

							for _, mbPuchaseResult := range mbClientPurchasesResult.Purchases.SaleItem {
								ss.Logger.Infof("Syncing purchase MB ID %d...", mbPuchaseResult.Sale.ID.Int)

								mbPurchase := mbClientPurchaseToPbClientPurchase(mbPuchaseResult)
								ss.Logger.Debugf("%#v", mbPurchase)
								mbPurchase.ClientId = dbClient.Id
								mbPurchase.SiteId = dbSite.Id
								if dbLocation, ok := dbLocationsMap[mbPuchaseResult.Sale.Location.ID.Int]; ok {
									mbPurchase.LocationId = dbLocation.Id
								} else {
									ss.Logger.Errorf(
										"Cannot find location MB ID %d for client %d, purchase MB ID %d.",
										mbPuchaseResult.Sale.Location.ID.Int,
										dbClient.Id,
										mbPurchase.SaleMbId,
									)
									continue AccountsLoop
								}

								if dbPurchase, ok := dbPurchasesMap[mbPuchaseResult.Sale.ID.Int]; ok {
									if hasClientPurchaseChanged(mbPurchase, dbPurchase) {
										mbPurchase.Uuid = dbPurchase.Uuid

										dbPurchase, err = ss.ClientService.UpdateClientPurchase(ctx, mbPurchase)
										if err != nil {
											ss.Logger.Errorf("Cannot update purchase MB ID %d for client ID %d: %v", mbPurchase.SaleMbId, dbClient.Id, err)
											continue AccountsLoop
										}
										ss.Logger.Infof("Updated client purchase ID %d.", dbPurchase.Id)
									}
									ss.Logger.Infof("Skipping (no changes) purchase ID %d.", dbPurchase.Id)
								} else {
									dbPurchase, err = ss.ClientService.CreateClientPurchase(ctx, mbPurchase)
									if err != nil {
										ss.Logger.Errorf("Cannot create purchase MB ID %d for client ID %d: %v", mbPurchase.SaleMbId, dbClient.Id, err)
										continue AccountsLoop
									}
									ss.Logger.Infof("Created client purchase ID %d.", dbPurchase.Id)
								}
							}
						}
					}
				} else {
					ss.Logger.Errorf("Cannot find the site (ID %d) for client ID %d: %v", dbClient.SiteId, dbClient.Id, err)
					continue AccountsLoop
				}

				// Updating client's purchases_synced_at
				dbClient.PurchasesSyncedAt = time.Now().Format(sqlDatetimeFormat)
				_, err := ss.ClientService.UpdateClient(ctx, dbClient)
				if err != nil {
					ss.Logger.Errorf("Failed to update client %d in DB (purchases_synced_at): %v", dbClient.Id, err)
				}
				ss.Logger.Infof("Finished client ID %d.", dbClient.Id)
			}
		}
		ss.Logger.Infof("Finished account UUID %s.", accountUUID)
		messagesToFinish = append(messagesToFinish, accountUUID)
	}

	return messagesToFinish, nil
}

// SyncAccountsClientsVisitsHistory syncs the given accounts clients the vists history from MB
func (ss *SyncService) SyncAccountsClientsVisitsHistory(accountUUIDs []string) ([]string, error) {
	var messagesToFinish []string

	ctx := context.Background()

	ss.Logger.Infof("SyncAccountsClientsVisitsHistory for clients: %s", strings.Join(accountUUIDs, ", "))

	accountsClientsResponse, err := ss.ClientService.GetAccountsClients(ctx, &accountPb.GetAccountsClientsRequest{
		AccountUuids: strings.Join(accountUUIDs, ","),
	})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the accounts clients: %v", err)
		return messagesToFinish, fmt.Errorf("Cannot fetch the accounts clients: %v", err)
	}

	ss.Logger.Infof("Fetching the sites for the clients...")

	// Batch fetch the sites
	siteIDs := []int32{}
	for _, clients := range accountsClientsResponse.AccountClients {
		for _, dbClient := range clients.Clients {
			siteIDs = append(siteIDs, dbClient.SiteId)
		}
	}
	sitesResponse, err := ss.SiteService.GetSites(ctx, &sitePb.GetSitesRequest{Ids: siteIDs})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the sites: %v", err)
		return messagesToFinish, fmt.Errorf("Cannot fetch the sites: %v", err)
	}
	sitesMap := map[int32]*sitePb.Site{}
	for _, dbSite := range sitesResponse.Sites {
		sitesMap[dbSite.Id] = dbSite
	}

	ss.Logger.Debugf("Started looping through the accounts...")

	// Fetch from MB
AccountsLoop:
	for _, accountUUID := range accountUUIDs {
		if accountClients, ok := accountsClientsResponse.AccountClients[accountUUID]; ok {
			if len(accountClients.Clients) > 0 {
				ss.Logger.Infof("Syncing visits history of account '%s' (%d clients)...", accountUUID, len(accountClients.Clients))
			}
			for _, dbClient := range accountClients.Clients {
				if dbSite, ok := sitesMap[dbClient.SiteId]; ok {
					ss.Logger.Debugf("Fetchig site's %s locations...", dbSite.Uuid)

					// fetch the site locations
					dbLocationsResult, err := ss.SiteService.GetSitesLocations(ctx, &sitePb.GetSitesLocationsRequest{Uuids: dbSite.Uuid})
					if err != nil {
						ss.Logger.Errorf("Cannot fetch the site ID %d locations: %v", dbSite.Id, err)
						continue AccountsLoop
					}
					dbLocationsMap := map[int32]*sitePb.Location{}
					for _, dbLocation := range dbLocationsResult.Locations {
						dbLocationsMap[dbLocation.MbId] = dbLocation
					}

					var lastSyncTime time.Time
					if dbClient.PurchasesSyncedAt != "0000-00-00 00:00:00" {
						lastSyncTime, err = time.Parse(sqlDatetimeFormat, dbClient.PurchasesSyncedAt)
						if err != nil {
							ss.Logger.Errorf("Failed parsing SQL datetime '%s': %v", dbClient.PurchasesSyncedAt, err)
							continue AccountsLoop
						}
						// Fetch 30 days behind for updates
						lastSyncTime = lastSyncTime.AddDate(0, -1, 0)
					} else {
						lastSyncTime, _ = time.Parse(sqlDatetimeFormat, "1990-01-01 00:00:00")
					}

					ss.Logger.Infof("Syncing visits history of client '%s' (ID %d) - last sync at '%s'...", dbClient.Uuid, dbClient.Id, lastSyncTime.Format(sqlDatetimeFormat))

					// Sync the clients page by page
					mbRequest := mbHelpers.CreateClientMBRequest([]int32{dbSite.MbId}, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)

					totalPages := int32(1)
					pageSize := 20
					for currentPage := 1; int32(currentPage) <= totalPages; currentPage++ {
						// Get the clients from Mindbody
						mbRequest.CurrentPageIndex = &mindbody.CustomInt32{Int: int32(currentPage - 1)}
						mbRequest.PageSize = &mindbody.CustomInt32{Int: int32(pageSize)}

						mbClientVisitsResponse, err := ss.ClientMBClient.GetClientVisits(&clientMb.GetClientVisits{Request: &clientMb.GetClientVisitsRequest{
							MBRequest: mbRequest,
							ClientID:  strconv.FormatInt(dbClient.MbId, 10),
							StartDate: &mindbody.CustomTime{Time: lastSyncTime},
						}})
						if err != nil {
							ss.Logger.Errorf("Cannot fetch the client visits history from Mindbody: %v", err)
							continue AccountsLoop
						}
						mbClientVisitsResult := mbClientVisitsResponse.GetClientVisitsResult
						if mbClientVisitsResult.ErrorCode.Int != 200 {
							ss.Logger.Errorf("Mindbody API response error: %s", mbClientVisitsResult.Message)
							continue AccountsLoop
						}
						totalPages = mbClientVisitsResult.TotalPageCount.Int

						ss.Logger.Infof("Syncing %d client visits (page %d of %d)...", len(mbClientVisitsResult.Visits.Visit), currentPage, totalPages)

						if len(mbClientVisitsResult.Visits.Visit) > 0 {
							visitsMBIDs := []int64{}
							for _, mbVisitResult := range mbClientVisitsResult.Visits.Visit {
								visitsMBIDs = append(visitsMBIDs, mbVisitResult.ID.Int)
							}
							ss.Logger.Debugf("Checking in DB %d visits...", len(visitsMBIDs))
							dbVisits, err := ss.ClientService.GetClientVisitsByMBID(ctx, &accountPb.GetClientsVisitsByMBIDRequest{
								SiteId: dbSite.Id,
								MbIds:  visitsMBIDs,
							})
							if err != nil {
								ss.Logger.Errorf("Cannot fetch visits for client %d: %v", dbClient.Id, err)
								continue AccountsLoop
							}
							ss.Logger.Debugf("Got %d visits from DB %d.", len(dbVisits.ClientVisits))
							dbVisitsMap := map[int64]*accountPb.ClientVisit{}
							for _, dbVisit := range dbVisits.ClientVisits {
								dbVisitsMap[dbVisit.MbId] = dbVisit
							}

							// Fetch a map with the staff members
							staffMbIds := []int64{}
							for _, mbVisitResult := range mbClientVisitsResult.Visits.Visit {
								// Skip appointments
								if mbVisitResult.AppointmentID.Int == 0 {
									staffMbIds = append(staffMbIds, mbVisitResult.Staff.ID.Int)
								}
							}
							staffResponse, err := ss.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{SiteId: dbSite.Id, MbIds: staffMbIds})
							if err != nil {
								ss.Logger.Errorf("Cannot fetch the staff by MB IDs: %v", err)
								continue AccountsLoop
							}
							dbStaffMap := map[int64]*staffPb.Staff{}
							for _, dbStaff := range staffResponse.Staff {
								dbStaffMap[dbStaff.MbId] = dbStaff
							}

							// Fetch a map with the classes
							classesMbIds := []int32{}
							for _, mbVisitResult := range mbClientVisitsResult.Visits.Visit {
								// Skip appointments
								if mbVisitResult.AppointmentID.Int == 0 {
									classesMbIds = append(classesMbIds, mbVisitResult.ClassID.Int)
								}
							}
							ss.Logger.Debugf("Fetching classes MB ID %v for site ID %d from DB", classesMbIds, dbSite.Id)
							classesResponse, err := ss.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{SiteId: dbSite.Id, MbIds: classesMbIds})
							if err != nil {
								ss.Logger.Errorf("Cannot fetch the classes by MB IDs: %v", err)
								continue AccountsLoop
							}
							ss.Logger.Debugf("Found classes: %#v", classesResponse)

							dbClassMap := map[int32]*classPb.Class{}
							for _, dbClass := range classesResponse.Classes {
								dbClassMap[dbClass.MbId] = dbClass
							}

							for _, mbVisitResult := range mbClientVisitsResult.Visits.Visit {
								// Skip appointments
								if mbVisitResult.AppointmentID.Int > 0 {
									ss.Logger.Infof("Appointment MB ID %d skipped...", mbVisitResult.AppointmentID.Int)
									continue
								}

								ss.Logger.Infof("Syncing visit MB ID %d...", mbVisitResult.ID.Int)

								mbVisit := mbClientVisitToPbClientVisit(mbVisitResult)
								ss.Logger.Debugf("%#v", mbVisitResult)
								mbVisit.ClientId = dbClient.Id
								mbVisit.SiteId = dbSite.Id

								if dbLocation, ok := dbLocationsMap[mbVisitResult.Location.ID.Int]; ok {
									mbVisit.LocationId = dbLocation.Id
								} else {
									ss.Logger.Errorf("Cannot find location MB ID %d for client %d, visit MB ID %d.",
										mbVisitResult.Location.ID.Int, dbClient.Id, mbVisit.MbId)
									continue AccountsLoop
								}

								if dbStaff, ok := dbStaffMap[mbVisitResult.Staff.ID.Int]; ok {
									mbVisit.StaffId = dbStaff.Id
								} else {
									ss.Logger.Errorf("Cannot find staff MB ID %d for client %d, visit MB ID %d.",
										mbVisitResult.Staff.ID.Int, dbClient.Id, mbVisit.MbId)
									continue AccountsLoop
								}

								if dbClass, ok := dbClassMap[mbVisitResult.ClassID.Int]; ok {
									mbVisit.ClassId = dbClass.Id
								} else {
									ss.Logger.Errorf("Cannot find class MB ID %d for client %d, visit MB ID %d.",
										mbVisitResult.ClassID.Int, dbClient.Id, mbVisit.MbId)
									continue AccountsLoop
								}

								if dbVisit, ok := dbVisitsMap[mbVisitResult.ID.Int]; ok {
									if hasClientVisitChanged(mbVisit, dbVisit) {
										mbVisit.Uuid = dbVisit.Uuid

										dbVisit, err = ss.ClientService.UpdateClientVisit(ctx, mbVisit)
										if err != nil {
											ss.Logger.Errorf("Cannot update visit MB ID %d for client ID %d: %v", mbVisit.MbId, dbClient.Id, err)
											continue AccountsLoop
										}
										ss.Logger.Infof("Updated client visit ID %d.", dbVisit.Id)
									}
									ss.Logger.Infof("Skipping (no changes) visit ID %d.", dbVisit.Id)
								} else {
									dbVisit, err = ss.ClientService.CreateClientVisit(ctx, mbVisit)
									if err != nil {
										ss.Logger.Errorf("Cannot create visit MB ID %d for client ID %d: %v", mbVisit.MbId, dbClient.Id, err)
										continue AccountsLoop
									}
									ss.Logger.Infof("Created client visit ID %d.", dbVisit.Id)
								}
							}
						}
					}
				} else {
					ss.Logger.Errorf("Cannot find the site (ID %d) for client ID %d: %v", dbClient.SiteId, dbClient.Id, err)
					continue AccountsLoop
				}

				// Updating client's purchases_synced_at
				dbClient.PurchasesSyncedAt = time.Now().Format(sqlDatetimeFormat)
				_, err := ss.ClientService.UpdateClient(ctx, dbClient)
				if err != nil {
					ss.Logger.Errorf("Failed to update client %d in DB (purchases_synced_at): %v", dbClient.Id, err)
				}
				ss.Logger.Infof("Finished client ID %d.", dbClient.Id)
			}
		}
		ss.Logger.Infof("Finished account UUID %s.", accountUUID)
		messagesToFinish = append(messagesToFinish, accountUUID)
	}

	return messagesToFinish, nil
}
