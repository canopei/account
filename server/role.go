package main

import (
	"time"

	accountPb "bitbucket.org/canopei/account/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// GetAccountRoles retrieves the role for the given Account
func (s *Server) GetAccountRoles(ctx context.Context, req *accountPb.GetAccountRolesRequest) (*accountPb.RolesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetAccountRoles: %v", req)
	defer timeTrack(logger, time.Now(), "GetAccountRoles")

	// Validations
	if req.SiteId == 0 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	if req.Uuid == "" {
		logger.Error("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}

	// fetch the roles
	rows, err := s.DB.Queryx(`SELECT r.*
		FROM account_role ar
		INNER JOIN account a ON ar.account_id = a.account_id
		INNER JOIN role r ON ar.role_id = r.role_id
		WHERE a.uuid = UNHEX(REPLACE(?,'-',''))
			AND r.site_id = ?
			AND a.deleted_at = '0'
			AND r.deleted_at = '0'`, req.Uuid, req.SiteId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the roles")
	}

	rolesList := &accountPb.RolesList{}
	for rows.Next() {
		role := accountPb.Role{}
		err := rows.StructScan(&role)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the roles")
		}
		rolesList.Roles = append(rolesList.Roles, &role)
	}

	return rolesList, nil
}
