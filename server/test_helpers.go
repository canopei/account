package main

import (
	"io/ioutil"

	"github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
	nsq "github.com/nsqio/go-nsq"
)

// NewServerMock creates a mock of Server
func NewServerMock(db *sqlx.DB) *Server {
	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	queueProducer := &nsq.Producer{}

	return NewServer(logger, db, queueProducer)
}
