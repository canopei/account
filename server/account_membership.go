package main

import (
	"database/sql"
	"time"

	"golang.org/x/net/context"

	accountPb "bitbucket.org/canopei/account/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// CreateAccountMembership creates a new AccountMembership
func (s *Server) CreateAccountMembership(ctx context.Context, req *accountPb.AccountMembership) (*accountPb.AccountMembership, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateAccountMembership: %v", req)
	defer timeTrack(logger, time.Now(), "CreateAccountMembership")

	// Validations
	if req.AccountId < 0 {
		logger.Error("The account ID is invalid.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is invalid.")
	}
	if req.MembershipId < 0 {
		logger.Error("The membership ID is invalid.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The membership ID is invalid.")
	}

	// create the account membership
	result, err := s.DB.NamedExec(`INSERT INTO account_membership
			(account_id, membership_id, remaining, valid_until)
		VALUES
			(:account_id, :membership_id, :remaining, :valid_until)`, req)
	if err != nil {
		logger.Errorf("Cannot create database entry: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot get LAST_INSERT_ID.")
	}

	logger.Debugf("Created new account membership ID %d.", lastInsertID)

	membership := &accountPb.AccountMembership{}
	err = s.DB.Get(membership, "SELECT * FROM account_membership WHERE account_membership_id = ?", lastInsertID)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the new membership.")
	}

	return membership, nil
}

// UpdateAccountMembership updates an existing AccountMembership
func (s *Server) UpdateAccountMembership(ctx context.Context, req *accountPb.AccountMembership) (*accountPb.AccountMembership, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateAccountMembership: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateAccountMembership")

	// Validations
	if req.Id < 1 {
		logger.Error("The account membership ID is invalid.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account membership ID is invalid.")
	}

	// update the account membership
	_, err := s.DB.NamedExec(`UPDATE account_membership
		SET remaining = :remaining
		WHERE account_membership_id = :account_membership_id`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	membership := &accountPb.AccountMembership{}
	err = s.DB.Get(membership, "SELECT * FROM account_membership WHERE account_membership_id = ?", req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateAccountMembership - account membership not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}

	return membership, nil
}

// GetAccountMemberships retrieves the memberships for the given Account
func (s *Server) GetAccountMemberships(ctx context.Context, req *accountPb.AccountMembership) (*accountPb.AccountMembershipsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetAccountMemberships: %v", req)
	defer timeTrack(logger, time.Now(), "GetAccountMemberships")

	// Validations
	if req.AccountId < 0 {
		logger.Error("The account ID is invalid.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is invalid.")
	}

	// fetch the permissions
	rows, err := s.DB.Queryx(`SELECT *
		FROM account_membership
		WHERE account_id = ?`, req.AccountId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the memberships")
	}

	membershipsList := &accountPb.AccountMembershipsList{}
	for rows.Next() {
		accountMembership := accountPb.AccountMembership{}
		err := rows.StructScan(&accountMembership)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the membership")
		}
		membershipsList.Memberships = append(membershipsList.Memberships, &accountMembership)
	}

	return membershipsList, nil
}
