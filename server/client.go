package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/empty"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	pb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"bitbucket.org/canopei/golibs/slices"
)

const (
	// MaxClientsFetched is the limit of number of clients that can be fetched
	// at once.
	MaxClientsFetched int32 = 50
)

// CreateClient creates a Client
func (s *Server) CreateClient(ctx context.Context, req *pb.Client) (*pb.Client, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateClient: %v", req)
	defer timeTrack(logger, time.Now(), "CreateClient")

	if req.MbId < 1 {
		logger.Error("The MB ID is invalid.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The MB ID is invalid.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is invalid.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is invalid.")
	}

	// Check if a Client with this MBID already exists
	err := s.DB.Get(req, "SELECT * FROM client WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'", req.MbId, req.SiteId)
	if err != sql.ErrNoRows {
		if err == nil {
			logger.Errorf("Client with MB ID %d already exists on site ID %d.", req.MbId, req.SiteId)
			return nil, grpc.Errorf(codes.Aborted, "Client already exists.")
		}
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the client.")
	}

	// Create the client
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	_, err = s.DB.NamedExec(`INSERT INTO client
			(uuid, mb_id, site_id, first_name, last_name, middle_name, gender, email, email_optin, city, state, country, status,
			username, is_company, inactive)
		VALUES
			(unhex(replace(:uuid,'-','')), :mb_id, :site_id, :first_name, :last_name, :middle_name, :gender, :email, :email_optin,
			:city, :state, :country, :status, :username, :is_company, :inactive)`, req)
	if err != nil {
		logger.Errorf("Cannot create database entry: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Created client '%s'.", req.Uuid)

	err = s.DB.Get(req, "SELECT * FROM client WHERE uuid = unhex(replace(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the new client.")
	}
	req.Uuid = uuid.String()

	return req, nil
}

// GetClient retrieves a Client by UUID or ID
func (s *Server) GetClient(ctx context.Context, req *pb.GetClientRequest) (*pb.Client, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClient: %v", req)
	defer timeTrack(logger, time.Now(), "GetClient")

	if req.Uuid == "" {
		logger.Error("The client UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client UUID is required.")
	}

	ID := int32(0)
	if !strings.Contains(req.Uuid, "-") {
		rid, err := strconv.ParseInt(req.Uuid, 10, 32)
		if err != nil {
			logger.Error("The provided ID is our of range.")
			return nil, grpc.Errorf(codes.InvalidArgument, "The provided ID is our of range.")
		}
		ID = int32(rid)
	}

	var client pb.Client
	err := s.DB.Get(&client, `SELECT *
		FROM client
		WHERE (uuid = UNHEX(REPLACE(?,'-','')) OR client_id = ?)
			AND deleted_at = '0'`, req.Uuid, ID)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find client '%s' (%d).", req.Uuid, ID)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching client")
	}

	unpackedUUID, err := crypto.Parse([]byte(client.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	client.Uuid = unpackedUUID.String()

	return &client, nil
}

// GetClients fetches a list of clients
func (s *Server) GetClients(ctx context.Context, req *pb.GetClientsRequest) (*pb.ClientsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClients: %v", req)
	defer timeTrack(logger, time.Now(), "GetClients")

	if req.Limit == 0 || req.Limit > MaxClientsFetched {
		req.Limit = 20
	}

	logger.Debugf("Fetching %d clients, offset %d...", req.Limit, req.Offset)

	// Fetch the sites
	queryParams := map[string]interface{}{
		"limit":  req.Limit,
		"offset": req.Offset,
	}

	condition := ""
	if len(req.Ids) > 0 {
		condition = " AND uuid IN ("
		for k, v := range req.Ids {
			paramKey := "id" + strconv.Itoa(k)
			condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ")"
	}

	// Search by e-mail
	if req.Email != "" {
		queryParams["email"] = req.Email
		condition += " AND email = :email"
	}

	if req.SiteId > 0 {
		queryParams["site_id"] = req.SiteId
		condition += " AND site_id = :site_id"
	}

	rows, err := s.DB.NamedQuery(`SELECT *
		FROM client
		WHERE deleted_at = '0' `+condition+`
		ORDER BY client_id ASC
		LIMIT :limit
		OFFSET :offset`, queryParams)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		return nil, err
	}

	clientsList := &pb.ClientsList{}
	for rows.Next() {
		client := pb.Client{}
		err := rows.StructScan(&client)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the clients")
		}

		unpackedUUID, err := crypto.Parse([]byte(client.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		client.Uuid = unpackedUUID.String()

		clientsList.Clients = append(clientsList.Clients, &client)
	}

	return clientsList, nil
}

// UpdateClient updates a Client
func (s *Server) UpdateClient(ctx context.Context, req *pb.Client) (*pb.Client, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateClient: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateClient")

	if req.Uuid == "" {
		logger.Error("The client UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client UUID is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE client SET
			first_name = :first_name, last_name = :last_name, middle_name = :middle_name, email = :email, email_optin = :email_optin,
			city = :city, state = :state, country = :country, status = :status, username = :username, is_company = :is_company,
			inactive = :inactive, gender = :gender,
			purchases_synced_at = CASE WHEN :purchases_synced_at = '0000-00-00' THEN purchases_synced_at ELSE :purchases_synced_at END
		WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var client pb.Client
	err = s.DB.Get(&client, "SELECT * FROM client WHERE uuid = unhex(replace(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateClient - client not found: %s", req.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	client.Uuid = req.Uuid

	return &client, nil
}

// DeleteClient removes a Client (sets deleted_at)
func (s *Server) DeleteClient(ctx context.Context, req *pb.DeleteClientRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClient: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteClient")

	if req.Uuid == "" {
		logger.Error("The client UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client UUID is required.")
	}

	result, err := s.DB.Exec("UPDATE client SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteClient - client not found: %s", req.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// GetClientsByMBID fetches a list of clients by their MB IDs
func (s *Server) GetClientsByMBID(ctx context.Context, req *pb.GetClientsByMBIDRequest) (*pb.ClientsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClientsByMBID: %d IDs", len(req.MbIds))
	defer timeTrack(logger, time.Now(), "GetClientsByMBID")

	// Validations
	if len(req.MbIds) == 0 {
		logger.Error("At least one MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one MB ID is required.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}

	// Fetch the clients
	queryParams := map[string]interface{}{
		"site_id": req.SiteId,
	}
	condition := " AND mb_id IN ("
	for k, v := range req.MbIds {
		paramKey := "id" + strconv.Itoa(k)
		condition += ":" + paramKey + ", "
		queryParams[paramKey] = v
	}
	condition = strings.TrimSuffix(condition, ", ") + ")"

	rows, err := s.DB.NamedQuery(`SELECT *
		FROM client
		WHERE deleted_at = '0'
			AND site_id = :site_id`+condition+`
		ORDER BY client_id ASC`, queryParams)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		return nil, err
	}

	clientsList := &pb.ClientsList{}
	for rows.Next() {
		client := pb.Client{}
		err := rows.StructScan(&client)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the clients")
		}

		unpackedUUID, err := crypto.Parse([]byte(client.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		client.Uuid = unpackedUUID.String()

		clientsList.Clients = append(clientsList.Clients, &client)
	}

	return clientsList, nil
}

// DeleteClientsForSiteIDExceptMBIDs cleans the clients for a site ID except the given list
func (s *Server) DeleteClientsForSiteIDExceptMBIDs(ctx context.Context, req *pb.DeleteClientsForSiteIDExceptMBIDsRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClientsForSiteIDExceptMBIDs: %v IDs", len(req.MBIDs))
	defer timeTrack(logger, time.Now(), "DeleteClientsForSiteIDExceptMBIDs")

	if req.SiteID == 0 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	if len(req.MBIDs) == 0 {
		logger.Error("At least one client MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one client MB ID is required.")
	}

	// Fetch the clients
	queryParams := map[string]interface{}{
		"site_id": req.SiteID,
	}
	condition := "AND mb_id NOT IN ("
	for k, v := range req.MBIDs {
		paramKey := "id" + strconv.Itoa(k)
		condition += ":" + paramKey + ", "
		queryParams[paramKey] = v
	}
	condition = strings.TrimSuffix(condition, ", ") + ")"

	_, err := s.DB.NamedExec(`UPDATE client
		SET deleted_at = CURRENT_TIMESTAMP
		WHERE site_id = :site_id AND deleted_at = '0' `+condition, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}

	return &empty.Empty{}, nil
}

// CreateAccountClientLink links a client with an account
func (s *Server) CreateAccountClientLink(ctx context.Context, req *pb.AccountClientLinkRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateAccountClientLink: %v", req)
	defer timeTrack(logger, time.Now(), "CreateAccountClientLink")

	if req.AccountUuid == "" {
		logger.Error("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}
	if req.ClientUuid == "" {
		logger.Error("The client UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client UUID is required.")
	}

	// Check if a Link already exists
	var count int
	row := s.DB.QueryRowx(`SELECT COUNT(*)
		FROM account_client ac
		INNER JOIN account a ON a.account_id = ac.account_id
		INNER JOIN client c ON c.client_id = ac.client_id
		WHERE
			a.deleted_at = '0'
			AND a.uuid = UNHEX(REPLACE(?,'-',''))
			AND c.deleted_at = '0'
			AND c.uuid = UNHEX(REPLACE(?,'-',''))`, req.AccountUuid, req.ClientUuid)
	if row.Err() != nil {
		return nil, grpcUtils.InternalError(logger, row.Err(), "Error while checking for existing links.")
	}
	err := row.Scan(&count)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, row.Err(), "Error while scanning the count.")
	}
	if count > 0 {
		logger.Error("The link already exists.")
		return nil, grpc.Errorf(codes.AlreadyExists, "The link already exists.")
	}

	// Create the link
	_, err = s.DB.Exec(`INSERT INTO account_client (account_id, client_id)
		VALUES (
			(SELECT account_id from account WHERE uuid = UNHEX(REPLACE(?,'-','')) AND deleted_at = '0' LIMIT 1),
			(SELECT client_id from client WHERE uuid = UNHEX(REPLACE(?,'-','')) AND deleted_at = '0' LIMIT 1)
		)`, req.AccountUuid, req.ClientUuid)
	if err != nil {
		logger.Errorf("Cannot create database entry: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Created link for account '%s' and client '%s'.", req.AccountUuid, req.ClientUuid)

	return &empty.Empty{}, nil
}

// DeleteAccountClientLink links a client with an account
func (s *Server) DeleteAccountClientLink(ctx context.Context, req *pb.AccountClientLinkRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteAccountClientLink: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteAccountClientLink")

	// Validations
	if req.AccountUuid == "" {
		logger.Error("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}
	if req.ClientUuid == "" {
		logger.Error("The client UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client UUID is required.")
	}

	result, err := s.DB.Exec(`DELETE FROM account_client
		WHERE account_id = (SELECT account_id from account WHERE uuid = UNHEX(REPLACE(?,'-','')) AND deleted_at = '0' LIMIT 1)
			AND client_id = (SELECT client_id from client WHERE uuid = UNHEX(REPLACE(?,'-','')) AND deleted_at = '0' LIMIT 1)`, req.AccountUuid, req.ClientUuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteAccountClientLink - link not found: %s / %s", req.AccountUuid, req.ClientUuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// GetAccountsClients fetches a list of clients
func (s *Server) GetAccountsClients(ctx context.Context, req *pb.GetAccountsClientsRequest) (*pb.GetAccountsClientsResponse, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetAccountsClients: %v", req)
	defer timeTrack(logger, time.Now(), "GetAccountsClients")

	req.AccountUuids = strings.TrimSpace(req.AccountUuids)
	reqUUIDs := []string{}
	if req.AccountUuids != "" {
		uuids := strings.Split(req.AccountUuids, ",")
		for _, uuid := range uuids {
			uuid := strings.TrimSpace(uuid)
			if uuid != "" && !slices.Scontains(reqUUIDs, uuid) {
				reqUUIDs = append(reqUUIDs, uuid)
			}
		}
	}

	if len(reqUUIDs) == 0 {
		logger.Error("At least one account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one account UUID is required.")
	}

	logger.Debugf("Fetching clients for accounts %s...", strings.Join(reqUUIDs, ","))

	// Fetch the locations
	queryParams := map[string]interface{}{}
	condition := " AND a.uuid IN ("
	for idx, k := range reqUUIDs {
		paramKey := "uuid" + strconv.Itoa(idx)
		condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
		queryParams[paramKey] = k
	}
	condition = strings.TrimSuffix(condition, ", ") + ") "

	rows, err := s.DB.NamedQuery(`SELECT c.*, a.uuid "account_uuid"
		FROM account_client ac
		INNER JOIN account a ON a.account_id = ac.account_id
		INNER JOIN client c ON c.client_id = ac.client_id
		WHERE
			a.deleted_at = '0'
			AND c.deleted_at = '0'
			`+condition+`
		ORDER BY c.client_id ASC`, queryParams)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		return nil, err
	}

	response := &pb.GetAccountsClientsResponse{
		AccountClients: map[string]*pb.ClientsList{},
	}
	for rows.Next() {
		client := &pb.Client{}
		err := rows.StructScan(&client)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the clients")
		}

		unpackedUUID, err := crypto.Parse([]byte(client.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the client UUID")
		}
		client.Uuid = unpackedUUID.String()

		unpackedUUID, err = crypto.Parse([]byte(client.AccountUuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the account UUID")
		}
		client.AccountUuid = unpackedUUID.String()

		if _, ok := response.AccountClients[client.AccountUuid]; !ok {
			response.AccountClients[client.AccountUuid] = &pb.ClientsList{}
		}
		response.AccountClients[client.AccountUuid].Clients = append(response.AccountClients[client.AccountUuid].Clients, client)
	}

	return response, nil
}
