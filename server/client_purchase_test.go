package main

import (
	"context"
	"testing"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	clientPurchaseColumns = []string{"client_purchase_id", "uuid", "sale_mb_id", "client_id", "site_id", "location_id",
		"description", "price", "amount_paid", "discount", "tax", "returned", "quantity", "sale_time", "payment_id",
		"payment_type", "payment_method", "payment_notes", "created_at", "modified_at"}
)

func TestCreateClientPurchaseValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ClientPurchase
		ExpectedCode codes.Code
	}{
		{&accountPb.ClientPurchase{}, codes.InvalidArgument},
		{&accountPb.ClientPurchase{ClientId: 1, SiteId: 2, Price: 12.34, Quantity: 2, AmountPaid: 12.34, SaleMbId: 123}, codes.InvalidArgument},
		{&accountPb.ClientPurchase{PaymentId: 1, SiteId: 2, Price: 12.34, Quantity: 2, AmountPaid: 12.34, SaleMbId: 123}, codes.InvalidArgument},
		{&accountPb.ClientPurchase{PaymentId: 1, ClientId: 1, Price: 12.34, Quantity: 2, AmountPaid: 12.34, SaleMbId: 123}, codes.InvalidArgument},
		{&accountPb.ClientPurchase{PaymentId: 1, ClientId: 1, SiteId: 2, Price: 12.34, AmountPaid: 12.34, SaleMbId: 123}, codes.InvalidArgument},
		{&accountPb.ClientPurchase{PaymentId: 1, ClientId: 1, SiteId: 2, Price: 12.34, Quantity: 2, AmountPaid: 12.34}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ClientPurchase{PaymentId: 1, ClientId: 1, SiteId: 2, Price: 12.34, Quantity: 2, AmountPaid: 12.34, SaleMbId: 123}, codes.Unknown},
		{&accountPb.ClientPurchase{PaymentId: 1, ClientId: 1, SiteId: 2, Price: 0, Quantity: 2, AmountPaid: 12.34, SaleMbId: 123}, codes.Unknown},
		{&accountPb.ClientPurchase{PaymentId: 1, ClientId: 1, SiteId: 2, Price: 0, Quantity: 2, AmountPaid: 0, SaleMbId: 123}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		clientPurchase, err := server.CreateClientPurchase(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(clientPurchase, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateClientPurchaseExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	request := &accountPb.ClientPurchase{PaymentId: 1, ClientId: 1, SiteId: 2, Price: 12.34, Quantity: 2, AmountPaid: 12.34, SaleMbId: 123}

	rows := sqlmock.NewRows(clientPurchaseColumns).AddRow(1, "1234567890123456", 123, 1, 1, 1, "description", 12.23, 12.23, 0.34, 1.23, 0, 1, "2017-01-01", 1, "type", 1,
		"notes", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WillReturnRows(rows)

	clientPurchase, err := server.CreateClientPurchase(context.Background(), request)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(clientPurchase)
	assert.NotNil(err)
	assert.Equal(codes.Aborted, grpc.Code(err))
}

func TestCreateClientPurchaseSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	request := &accountPb.ClientPurchase{PaymentId: 1, ClientId: 1, SiteId: 2, Price: 12.34, Quantity: 2, AmountPaid: 12.34, SaleMbId: 123}

	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WillReturnRows(sqlmock.NewRows(clientPurchaseColumns))
	mock.ExpectExec("^INSERT INTO client_purchase").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(clientPurchaseColumns).AddRow(1, "1234567890123456", 123, 1, 1, 1, "description", 12.23, 12.23, 0.34, 1.23,
		0, 1, "2017-01-01", 1, "type", 1, "notes", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WithArgs(sqlmock.AnyArg()).WillReturnRows(rows)

	clientPurchase, err := server.CreateClientPurchase(context.Background(), request)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientPurchase)
	// Sanity checks
	if clientPurchase != nil {
		assert.Equal(float64(12.23), clientPurchase.Price)
	}
}

func TestGetClientPurchaseValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ClientPurchaseRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.ClientPurchaseRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ClientPurchaseRequest{Uuid: "test-uuid"}, codes.Unknown},
		// If it has no dash, it will be converted to int
		{&accountPb.ClientPurchaseRequest{Uuid: "nodash"}, codes.InvalidArgument},
		{&accountPb.ClientPurchaseRequest{Uuid: "12345"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		clientPurchase, err := server.GetClientPurchase(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(clientPurchase, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClientPurchaseMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Test with UUID
	// No rows returned
	rows := sqlmock.NewRows(clientPurchaseColumns)
	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WithArgs("test-uuid", 0).WillReturnRows(rows)

	clientPurchase, err := server.GetClientPurchase(context.Background(), &accountPb.ClientPurchaseRequest{Uuid: "test-uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(clientPurchase)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Test with ID
	// No rows returned
	rows = sqlmock.NewRows(clientPurchaseColumns)
	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WithArgs("12345", 12345).WillReturnRows(rows)

	clientPurchase, err = server.GetClientPurchase(context.Background(), &accountPb.ClientPurchaseRequest{Uuid: "12345"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(clientPurchase)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClientPurchaseSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// The account SELECT query
	rows := sqlmock.NewRows(clientPurchaseColumns).AddRow(1, "1234567890123456", 123, 1, 1, 1, "description", 12.23, 12.23, 0.34, 1.23,
		0, 1, "2017-01-01", 1, "type", 1, "notes", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WithArgs("test-uuid", 0).WillReturnRows(rows)

	clientPurchase, err := server.GetClientPurchase(context.Background(), &accountPb.ClientPurchaseRequest{Uuid: "test-uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientPurchase)
	// Sanity checks
	if clientPurchase != nil {
		assert.Equal(float64(12.23), clientPurchase.Price)
	}
}

func TestGetClientPrchasesSimpleEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WillReturnRows(sqlmock.NewRows(clientPurchaseColumns))
	clientPurchases, err := server.GetClientPurchases(context.Background(), &accountPb.GetClientPurchasesRequest{Limit: 10, Offset: 15})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientPurchases)
	if clientPurchases != nil {
		assert.Empty(clientPurchases.ClientPurchases)
	}
}

func TestGetClientPurchasesSimpleNotEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	clientPurchases := []*accountPb.ClientPurchase{
		&accountPb.ClientPurchase{
			Id: 1, Uuid: "1234567890123456", SiteId: 1, SaleMbId: 123, ClientId: 1, LocationId: 1, Price: 12.123, AmountPaid: 12.123,
			Discount: 1.123, Tax: 0.23, Returned: false, Quantity: 1, SaleTime: "2017-01-02", PaymentId: 1, PaymentType: "card",
			PaymentMethod: 12, PaymentNotes: "notes", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", Description: "description 1",
		},
		&accountPb.ClientPurchase{
			Id: 1, Uuid: "1234567890123457", SiteId: 1, SaleMbId: 123, ClientId: 1, LocationId: 1, Price: -12.123, AmountPaid: 12.123,
			Discount: 0, Tax: 0, Returned: true, Quantity: 1, SaleTime: "2017-01-02", PaymentId: 1, PaymentType: "card",
			PaymentMethod: 12, PaymentNotes: "notes", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", Description: "description 2",
		},
		&accountPb.ClientPurchase{
			Id: 1, Uuid: "1234567890123458", SiteId: 1, SaleMbId: 123, ClientId: 1, LocationId: 1, Price: 12.123, AmountPaid: 12.123,
			Discount: 1.123, Tax: 0.23, Returned: false, Quantity: 1, SaleTime: "2017-01-02", PaymentId: 1, PaymentType: "card",
			PaymentMethod: 12, PaymentNotes: "notes", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", Description: "description 3",
		},
	}

	rows := sqlmock.NewRows(clientPurchaseColumns)
	for _, c := range clientPurchases {
		rows.AddRow(c.Id, c.Uuid, c.SaleMbId, c.ClientId, c.SiteId, c.LocationId, c.Description, c.Price, c.AmountPaid, c.Discount,
			c.Tax, c.Returned, c.Quantity, c.SaleTime, c.PaymentId, c.PaymentType, c.PaymentMethod, c.PaymentNotes,
			c.CreatedAt, c.ModifiedAt)
	}

	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WillReturnRows(rows)
	response, err := server.GetClientPurchases(context.Background(), &accountPb.GetClientPurchasesRequest{Limit: 10, Offset: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.ClientPurchases, len(clientPurchases))
	}
}

func TestGetClientPurchasesWithFilters(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WillReturnRows(sqlmock.NewRows(clientPurchaseColumns))
	clientPurchases, err := server.GetClientPurchases(context.Background(), &accountPb.GetClientPurchasesRequest{
		Limit: 10, Offset: 15, ClientUuid: "uuid",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientPurchases)
	if clientPurchases != nil {
		assert.Empty(clientPurchases.ClientPurchases)
	}
}

func TestGetClientPurchasesByMBIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetClientsPurchasesByMBIDRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetClientsPurchasesByMBIDRequest{}, codes.InvalidArgument},
		{&accountPb.GetClientsPurchasesByMBIDRequest{MbIds: []int64{1, 2}}, codes.InvalidArgument},
		{&accountPb.GetClientsPurchasesByMBIDRequest{SiteId: 1}, codes.InvalidArgument},
		{&accountPb.GetClientsPurchasesByMBIDRequest{SiteId: 1, MbIds: []int64{}}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetClientsPurchasesByMBIDRequest{SiteId: 1, MbIds: []int64{1, 2}}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		clientPurchases, err := server.GetClientPurchasesByMBID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(clientPurchases, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClientPurchasesByMBID(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	clientPurchases := []*accountPb.ClientPurchase{
		&accountPb.ClientPurchase{
			Id: 1, Uuid: "1234567890123456", SiteId: 1, SaleMbId: 123, ClientId: 1, LocationId: 1, Price: 12.123, AmountPaid: 12.123,
			Discount: 1.123, Tax: 0.23, Returned: false, Quantity: 1, SaleTime: "2017-01-02", PaymentId: 1, PaymentType: "card",
			PaymentMethod: 12, PaymentNotes: "notes", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", Description: "description 1",
		},
		&accountPb.ClientPurchase{
			Id: 1, Uuid: "1234567890123457", SiteId: 1, SaleMbId: 123, ClientId: 1, LocationId: 1, Price: -12.123, AmountPaid: 12.123,
			Discount: 0, Tax: 0, Returned: true, Quantity: 1, SaleTime: "2017-01-02", PaymentId: 1, PaymentType: "card",
			PaymentMethod: 12, PaymentNotes: "notes", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", Description: "description 2",
		},
		&accountPb.ClientPurchase{
			Id: 1, Uuid: "1234567890123458", SiteId: 1, SaleMbId: 123, ClientId: 1, LocationId: 1, Price: 12.123, AmountPaid: 12.123,
			Discount: 1.123, Tax: 0.23, Returned: false, Quantity: 1, SaleTime: "2017-01-02", PaymentId: 1, PaymentType: "card",
			PaymentMethod: 12, PaymentNotes: "notes", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", Description: "description 3",
		},
	}

	rows := sqlmock.NewRows(clientPurchaseColumns)
	for _, c := range clientPurchases {
		rows.AddRow(c.Id, c.Uuid, c.SaleMbId, c.ClientId, c.SiteId, c.LocationId, c.Description, c.Price, c.AmountPaid, c.Discount,
			c.Tax, c.Returned, c.Quantity, c.SaleTime, c.PaymentId, c.PaymentType, c.PaymentMethod, c.PaymentNotes,
			c.CreatedAt, c.ModifiedAt)
	}

	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WillReturnRows(rows)

	response, err := server.GetClientPurchasesByMBID(context.Background(), &accountPb.GetClientsPurchasesByMBIDRequest{SiteId: 1, MbIds: []int64{123, 456}})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.Len(response.ClientPurchases, len(clientPurchases))
	// Sanity check
	assert.Equal(clientPurchases[1].Price, response.ClientPurchases[1].Price)
}

func TestUpdateClientPurchaseValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ClientPurchase
		ExpectedCode codes.Code
	}{
		{&accountPb.ClientPurchase{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ClientPurchase{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		clientPurchase, err := server.UpdateClientPurchase(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(clientPurchase, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateClientPurchaseMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE client_purchase SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WillReturnRows(sqlmock.NewRows(clientPurchaseColumns))

	clientPurchase, err := server.UpdateClientPurchase(context.Background(), &accountPb.ClientPurchase{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(clientPurchase)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateClientPurchaseSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClientPurchase := &accountPb.ClientPurchase{Uuid: "123456789012345", PaymentId: 1, ClientId: 1, SiteId: 2, Price: 12.34, Quantity: 2, AmountPaid: 12.34, SaleMbId: 123}

	mock.ExpectExec("^UPDATE client_purchase SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(clientPurchaseColumns).
		AddRow(1, "1234567890123456", 123, 1, 1, 1, "description", 12.23, 12.23, 0.34, 1.23,
			0, 1, "2017-01-01", 1, "type", 1, "notes", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client_purchase").WithArgs(testClientPurchase.Uuid).WillReturnRows(rows)

	clientPurchase, err := server.UpdateClientPurchase(context.Background(), testClientPurchase)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientPurchase)
}

func TestDeleteClientPurchaseValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ClientPurchaseRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.ClientPurchaseRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ClientPurchaseRequest{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.DeleteClientPurchase(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClientPurchase(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^DELETE FROM client_purchase").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteClientPurchase(context.Background(), &accountPb.ClientPurchaseRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^DELETE FROM client_purchase").WillReturnResult(sqlmock.NewResult(0, 1))

	_, err = server.DeleteClientPurchase(context.Background(), &accountPb.ClientPurchaseRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
