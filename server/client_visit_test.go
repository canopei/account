package main

import (
	"context"
	"testing"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	clientVisitColumns = []string{"client_visit_id", "uuid", "mb_id", "client_id", "site_id", "name", "staff_id", "location_id",
		"class_id", "service_mb_id", "service_count", "service_remaining", "late_cancelled", "make_up", "web_signup", "signed_in",
		"start_datetime", "end_datetime", "created_at", "modified_at"}
)

func TestCreateClientVisitValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ClientVisit
		ExpectedCode codes.Code
	}{
		{&accountPb.ClientVisit{}, codes.InvalidArgument},
		{&accountPb.ClientVisit{Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1}, codes.InvalidArgument},
		{&accountPb.ClientVisit{ClientId: 1, SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1}, codes.InvalidArgument},
		{&accountPb.ClientVisit{ClientId: 1, Name: "name", LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1}, codes.InvalidArgument},
		{&accountPb.ClientVisit{ClientId: 1, Name: "name", SiteId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1}, codes.InvalidArgument},
		{&accountPb.ClientVisit{ClientId: 1, Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ServiceMbId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ClientVisit{ClientId: 1, Name: "name", SiteId: 1, LocationId: 1, ClassId: 1}, codes.Unknown},
		{&accountPb.ClientVisit{ClientId: 1, Name: "name", SiteId: 1, LocationId: 1, ClassId: 1, ServiceMbId: 1}, codes.Unknown},
		{&accountPb.ClientVisit{ClientId: 1, Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1}, codes.Unknown},
		{&accountPb.ClientVisit{ClientId: 1, MbId: 123, Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		clientVisit, err := server.CreateClientVisit(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(clientVisit, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateClientVisitExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	request := &accountPb.ClientVisit{ClientId: 1, MbId: 123, Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1,
		ServiceCount: 1, ServiceRemaining: 2}

	rows := sqlmock.NewRows(clientVisitColumns).AddRow(1, "1234567890123456", 123, 1, 1, "name", 1, 1, 1, 234, 1, 5, 0, 0, 1, 1,
		"2017-01-02 18:00:00", "2017-01-03 18:30:00", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(rows)

	clientVisit, err := server.CreateClientVisit(context.Background(), request)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(clientVisit)
	assert.NotNil(err)
	assert.Equal(codes.Aborted, grpc.Code(err))
}

func TestCreateClientVisitSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	request := &accountPb.ClientVisit{ClientId: 1, MbId: 123, Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1,
		ServiceCount: 1, ServiceRemaining: 2}

	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(sqlmock.NewRows(clientVisitColumns))
	mock.ExpectExec("^INSERT INTO client_visit").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(clientVisitColumns).AddRow(1, "1234567890123456", 123, 1, 1, "name", 1, 1, 1, 234, 1, 5, 0, 0, 1, 1,
		"2017-01-02 18:00:00", "2017-01-03 18:30:00", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(rows)

	clientVisit, err := server.CreateClientVisit(context.Background(), request)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientVisit)
	// Sanity checks
	if clientVisit != nil {
		assert.Equal(int64(234), clientVisit.ServiceMbId)
	}
}

func TestGetClientVisitValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ClientVisitRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.ClientVisitRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ClientVisitRequest{Uuid: "test-uuid"}, codes.Unknown},
		// If it has no dash, it will be converted to int
		{&accountPb.ClientVisitRequest{Uuid: "nodash"}, codes.InvalidArgument},
		{&accountPb.ClientVisitRequest{Uuid: "12345"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		clientVisit, err := server.GetClientVisit(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(clientVisit, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClientVisitMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Test with UUID
	// No rows returned
	rows := sqlmock.NewRows(clientVisitColumns)
	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(rows)

	clientVisit, err := server.GetClientVisit(context.Background(), &accountPb.ClientVisitRequest{Uuid: "test-uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(clientVisit)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Test with ID
	// No rows returned
	rows = sqlmock.NewRows(clientVisitColumns)
	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(rows)

	clientVisit, err = server.GetClientVisit(context.Background(), &accountPb.ClientVisitRequest{Uuid: "12345"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(clientVisit)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClientVisitSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// The account SELECT query
	rows := sqlmock.NewRows(clientVisitColumns).AddRow(1, "1234567890123456", 123, 1, 1, "name", 1, 1, 1, 234, 1, 5, 0, 0, 1, 1,
		"2017-01-02 18:00:00", "2017-01-03 18:30:00", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(rows)

	clientVisit, err := server.GetClientVisit(context.Background(), &accountPb.ClientVisitRequest{Uuid: "test-uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientVisit)
	// Sanity checks
	if clientVisit != nil {
		assert.Equal(int64(234), clientVisit.ServiceMbId)
	}
}

func TestGetClientVisitsSimpleEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(sqlmock.NewRows(clientPurchaseColumns))
	clientVisits, err := server.GetClientVisits(context.Background(), &accountPb.GetClientVisitsRequest{Limit: 10, Offset: 15})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientVisits)
	if clientVisits != nil {
		assert.Empty(clientVisits.ClientVisits)
	}
}

func TestGetClientVisitsSimpleNotEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	clientVisits := []*accountPb.ClientVisit{
		&accountPb.ClientVisit{Id: 1, Uuid: "1234567890123456", ClientId: 1, MbId: 123, Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1,
			ServiceCount: 1, ServiceRemaining: 2},
		&accountPb.ClientVisit{Id: 2, Uuid: "1234567890123457", ClientId: 1, MbId: 123, Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1,
			ServiceCount: 1, ServiceRemaining: 2},
		&accountPb.ClientVisit{Id: 3, Uuid: "1234567890123458", ClientId: 1, MbId: 123, Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1,
			ServiceCount: 1, ServiceRemaining: 2},
	}

	rows := sqlmock.NewRows(clientVisitColumns)
	for _, c := range clientVisits {
		rows.AddRow(c.Id, c.Uuid, c.MbId, c.ClientId, c.SiteId, c.Name, c.StaffId, c.LocationId, c.ClassId, c.ServiceMbId, c.ServiceCount, c.ServiceRemaining,
			c.LateCancelled, c.MakeUp, c.WebSignup, c.SignedIn, "2017-01-02 18:00:00", "2017-01-03 18:30:00", "2017-01-02", "2017-01-03")
	}

	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(rows)
	response, err := server.GetClientVisits(context.Background(), &accountPb.GetClientVisitsRequest{Limit: 10, Offset: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.ClientVisits, len(clientVisits))
	}
}

func TestGetClientVisitsWithFilters(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(sqlmock.NewRows(clientVisitColumns))
	clientVisits, err := server.GetClientVisits(context.Background(), &accountPb.GetClientVisitsRequest{
		Limit: 10, Offset: 15, ClientUuids: "uuid",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientVisits)
	if clientVisits != nil {
		assert.Empty(clientVisits.ClientVisits)
	}
}

func TestGetClientVisitsByMBIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetClientsVisitsByMBIDRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetClientsVisitsByMBIDRequest{}, codes.InvalidArgument},
		{&accountPb.GetClientsVisitsByMBIDRequest{MbIds: []int64{1, 2}}, codes.InvalidArgument},
		{&accountPb.GetClientsVisitsByMBIDRequest{SiteId: 1}, codes.InvalidArgument},
		{&accountPb.GetClientsVisitsByMBIDRequest{SiteId: 1, MbIds: []int64{}}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetClientsVisitsByMBIDRequest{SiteId: 1, MbIds: []int64{1, 2}}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		clientVisits, err := server.GetClientVisitsByMBID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(clientVisits, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClientVisitsByMBID(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	clientVisits := []*accountPb.ClientVisit{
		&accountPb.ClientVisit{Id: 1, Uuid: "1234567890123456", ClientId: 1, MbId: 123, Name: "name 1", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1,
			ServiceCount: 1, ServiceRemaining: 2},
		&accountPb.ClientVisit{Id: 2, Uuid: "1234567890123457", ClientId: 1, MbId: 123, Name: "name 2", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1,
			ServiceCount: 1, ServiceRemaining: 2},
		&accountPb.ClientVisit{Id: 3, Uuid: "1234567890123458", ClientId: 1, MbId: 123, Name: "name 3", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1,
			ServiceCount: 1, ServiceRemaining: 2},
	}

	rows := sqlmock.NewRows(clientVisitColumns)
	for _, c := range clientVisits {
		rows.AddRow(c.Id, c.Uuid, c.MbId, c.ClientId, c.SiteId, c.Name, c.StaffId, c.LocationId, c.ClassId, c.ServiceMbId, c.ServiceCount, c.ServiceRemaining,
			c.LateCancelled, c.MakeUp, c.WebSignup, c.SignedIn, "2017-01-02 18:00:00", "2017-01-03 18:30:00", "2017-01-02", "2017-01-03")
	}

	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(rows)

	response, err := server.GetClientVisitsByMBID(context.Background(), &accountPb.GetClientsVisitsByMBIDRequest{SiteId: 1, MbIds: []int64{123, 456}})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.Len(response.ClientVisits, len(clientVisits))
	// Sanity check
	assert.Equal(clientVisits[1].Name, response.ClientVisits[1].Name)
}

func TestUpdateClientVisitValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ClientVisit
		ExpectedCode codes.Code
	}{
		{&accountPb.ClientVisit{}, codes.InvalidArgument},
		{&accountPb.ClientVisit{Uuid: "foo"}, codes.InvalidArgument},
		{&accountPb.ClientVisit{Name: "name"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ClientVisit{Uuid: "foo", Name: "name"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		clientVisit, err := server.UpdateClientVisit(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(clientVisit, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateClientVisitMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE client_visit SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WillReturnRows(sqlmock.NewRows(clientVisitColumns))

	clientVisit, err := server.UpdateClientVisit(context.Background(), &accountPb.ClientVisit{Uuid: "foo", Name: "new name"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(clientVisit)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateClientVisitSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClientVisit := &accountPb.ClientVisit{Uuid: "1234567890123456", ClientId: 1, MbId: 123, Name: "name", SiteId: 1, LocationId: 1, StaffId: 1, ClassId: 1, ServiceMbId: 1,
		ServiceCount: 1, ServiceRemaining: 2}

	mock.ExpectExec("^UPDATE client_visit SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(clientVisitColumns).
		AddRow(1, "1234567890123456", 123, 1, 1, "name", 1, 1, 1, 234, 1, 5, 0, 0, 1, 1,
			"2017-01-02 18:00:00", "2017-01-03 18:30:00", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client_visit").WithArgs(testClientVisit.Uuid).WillReturnRows(rows)

	clientVisit, err := server.UpdateClientVisit(context.Background(), testClientVisit)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clientVisit)
}

func TestDeleteClientVisitValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ClientVisitRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.ClientVisitRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ClientVisitRequest{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteClientVisit(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClientVisit(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^DELETE FROM client_visit").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteClientVisit(context.Background(), &accountPb.ClientVisitRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^DELETE FROM client_visit").WillReturnResult(sqlmock.NewResult(0, 1))

	_, err = server.DeleteClientVisit(context.Background(), &accountPb.ClientVisitRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
