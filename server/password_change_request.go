package main

import (
	"database/sql"
	"strings"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"golang.org/x/net/context"

	pb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
)

// RequestValidTTL is for how long is a password change request valid
const RequestValidTTL = 3600 * 24 * time.Second

// CreatePasswordChangeRequest creates a new PasswordChangeRequest
func (s *Server) CreatePasswordChangeRequest(ctx context.Context, req *pb.CreatePasswordChangeRequestRequest) (*pb.PasswordChangeRequest, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreatePasswordChangeRequest: %v", req)
	defer timeTrack(logger, time.Now(), "CreatePasswordChangeRequest")

	// Validations
	req.AccountUuid = strings.TrimSpace(req.AccountUuid)
	if req.AccountUuid == "" {
		logger.Errorf("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}

	// Get the account
	var account pb.Account
	err := s.DB.Get(&account, `SELECT *
		FROM account
		WHERE uuid = UNHEX(REPLACE(?,'-',''))
			AND deleted_at = '0'`, req.AccountUuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("The account doesn't exist.")
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching account")
	}

	code, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate the code: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate the code")
	}

	newRequest := &pb.PasswordChangeRequest{
		AccountId:  account.Id,
		Code:       code.String(),
		ValidUntil: time.Now().Add(RequestValidTTL).Format(sqlDatetimeFormat),
	}
	_, err = s.DB.NamedExec(`INSERT INTO password_change_request (account_id, code, valid_until)
		VALUES (:account_id, :code, :valid_until)`, newRequest)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	logger.Debugf("Created new password change request '%s' for account ID %d.", newRequest.Code, newRequest.AccountId)

	return newRequest, nil
}

// GetAccountByPasswordChangeRequest retrieves the Account of a PasswordChangeRequest
func (s *Server) GetAccountByPasswordChangeRequest(ctx context.Context, req *pb.GetAccountByPasswordChangeRequestRequest) (*pb.Account, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetAccountByPasswordChangeRequest: %v", req)
	defer timeTrack(logger, time.Now(), "GetAccountByPasswordChangeRequest")

	// Validations
	req.Code = strings.TrimSpace(req.Code)
	if req.Code == "" {
		logger.Errorf("The request code is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The request code is required.")
	}

	// Get the account
	var account pb.Account
	err := s.DB.Get(&account, `SELECT a.*
		FROM account a
		INNER JOIN password_change_request pcr ON a.account_id = pcr.account_id
		WHERE pcr.code = ?
			AND pcr.valid_until > NOW()
			AND a.deleted_at = '0'`, req.Code)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("The account doesn't exist or invalid code.")
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching account")
	}

	unpackedUUID, err := crypto.Parse([]byte(account.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	account.Uuid = unpackedUUID.String()

	return &account, nil
}
