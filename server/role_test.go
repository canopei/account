package main

import (
	"context"
	"testing"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var (
	roleColumns = []string{"role_id", "code", "name", "site_id", "created_at", "modified_at", "deleted_at"}
)

func TestGetAccountRolesValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetAccountRolesRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetAccountRolesRequest{SiteId: 1}, codes.InvalidArgument},
		{&accountPb.GetAccountRolesRequest{Uuid: "foo"}, codes.InvalidArgument},
		{&accountPb.GetAccountRolesRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetAccountRolesRequest{SiteId: 1, Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		rolesList, err := server.GetAccountRoles(context.Background(), validationTest.Request)
		// we will always get nil rolesList
		assert.Nil(rolesList, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetAccountRolesSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	roles := []*accountPb.Role{
		&accountPb.Role{
			Id: 1, Code: "foo", Name: "foo", SiteId: 1, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
		&accountPb.Role{
			Id: 2, Code: "foo", Name: "foo2", SiteId: 1, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
		&accountPb.Role{
			Id: 4, Code: "bar", Name: "bar", SiteId: 1, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
	}
	rows := sqlmock.NewRows(roleColumns)
	for _, role := range roles {
		rows.AddRow(role.Id, role.Code, role.Name, role.SiteId, role.CreatedAt, role.ModifiedAt, role.DeletedAt)
	}
	mock.ExpectQuery("^SELECT (.+) FROM account_role (.+) WHERE").WithArgs("foo", 1).WillReturnRows(rows)

	rolesList, err := server.GetAccountRoles(context.Background(), &accountPb.GetAccountRolesRequest{SiteId: 1, Uuid: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.Len(rolesList.Roles, len(roles))
	for _, role := range roles {
		assert.Contains(rolesList.Roles, role)
	}
}

func TestGetAccountRolesWithEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(permissionColumns)
	mock.ExpectQuery("^SELECT (.+) FROM account_role (.+) WHERE").WithArgs("foo", 1).WillReturnRows(rows)

	rolesList, err := server.GetAccountRoles(context.Background(), &accountPb.GetAccountRolesRequest{SiteId: 1, Uuid: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(rolesList)
	assert.Len(rolesList.Roles, 0)
}
