package main

import (
	"context"
	"testing"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	passwordChangeRequestColumns = []string{"password_change_request_id", "account_id", "code", "valid_until", "created_at"}
)

func TestCreatePasswordChangeRequestValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.CreatePasswordChangeRequestRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.CreatePasswordChangeRequestRequest{}, codes.InvalidArgument},
		{&accountPb.CreatePasswordChangeRequestRequest{AccountUuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.CreatePasswordChangeRequestRequest{AccountUuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		request, err := server.CreatePasswordChangeRequest(context.Background(), validationTest.Request)
		// we will always get nil permissionsList
		assert.Nil(request, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreatePasswordChangeRequestSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(accountColumns).
		AddRow(1, "123456789012345", "foo@bar.com", "hash", "salt", "m", "name", "http://www.example.com/image.jpg", "0000-00-00 00:00:00", "ccode",
			"2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account").WillReturnRows(rows)
	mock.ExpectExec("^INSERT INTO password_change_request").WillReturnResult(sqlmock.NewResult(1, 1))

	request, err := server.CreatePasswordChangeRequest(context.Background(), &accountPb.CreatePasswordChangeRequestRequest{
		AccountUuid: "foo",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(request)
	if request != nil {
		assert.NotEmpty(request.Code)
		assert.NotEmpty(request.ValidUntil)
	}
}

func TestCreatePasswordChangeRequestMissingAccount(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM account").WillReturnRows(sqlmock.NewRows(accountColumns))

	request, err := server.CreatePasswordChangeRequest(context.Background(), &accountPb.CreatePasswordChangeRequestRequest{
		AccountUuid: "foo",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(request)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetAccountByPasswordChangeRequestValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetAccountByPasswordChangeRequestRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetAccountByPasswordChangeRequestRequest{}, codes.InvalidArgument},
		{&accountPb.GetAccountByPasswordChangeRequestRequest{Code: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetAccountByPasswordChangeRequestRequest{Code: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.GetAccountByPasswordChangeRequest(context.Background(), validationTest.Request)
		// we will always get nil permissionsList
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetAccountByPasswordChangeRequestSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(accountColumns).
		AddRow(1, "1234567890123456", "foo@bar.com", "hash", "salt", "m", "name", "http://www.example.com/image.jpg", "0000-00-00 00:00:00", "ccode",
			"2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account").WillReturnRows(rows)

	account, err := server.GetAccountByPasswordChangeRequest(context.Background(), &accountPb.GetAccountByPasswordChangeRequestRequest{
		Code: "foo",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(account)
	if account != nil {
		assert.Equal("name", account.FullName)
	}
}

func TestGetAccountByPasswordChangeRequestMising(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM account").WillReturnRows(sqlmock.NewRows(accountColumns))

	account, err := server.GetAccountByPasswordChangeRequest(context.Background(), &accountPb.GetAccountByPasswordChangeRequestRequest{
		Code: "foo",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}
