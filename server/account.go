package main

import (
	"database/sql"
	"encoding/json"
	"regexp"
	"strings"
	"time"

	"golang.org/x/net/context"

	"fmt"

	pb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	queueHelper "bitbucket.org/canopei/golibs/queue"
	strHelper "bitbucket.org/canopei/golibs/strings"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	// PasswordMinLength is the minimum password length
	PasswordMinLength = 6
	// NameMinLength is the minimum name length
	NameMinLength = 3
)

var (
	emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
)

// CreateAccount creates an Account
func (s *Server) CreateAccount(ctx context.Context, req *pb.CreateAccountRequest) (*pb.Account, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateAccount: %v", req)
	defer timeTrack(logger, time.Now(), "CreateAccount")

	// validations
	req.Email = strings.TrimSpace(req.Email)
	if req.Email == "" {
		logger.Error("The email is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The email is required.")
	}
	req.Password = strings.TrimSpace(req.Password)
	if req.Password == "" {
		logger.Error("The password is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The password is required.")
	}
	if len(req.Password) < PasswordMinLength {
		logger.Errorf("A password of minimum %d characters is required.", PasswordMinLength)
		return nil, grpc.Errorf(codes.InvalidArgument, "A password of minimum %d characters is required.", PasswordMinLength)
	}
	req.FullName = strings.TrimSpace(req.FullName)
	if req.FullName == "" {
		logger.Error("The name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The name is required.")
	}
	if len(req.FullName) < NameMinLength {
		logger.Errorf("A name of minimum %d characters is required.", NameMinLength)
		return nil, grpc.Errorf(codes.InvalidArgument, "A name of minimum %d characters is required.", NameMinLength)
	}
	req.Email = strings.TrimSpace(strings.ToLower(req.Email))
	if !emailRegexp.MatchString(req.Email) {
		logger.Error("The email is invalid.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The email is invalid.")
	}

	// Create the account
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}

	passwordHash, passwordSalt := crypto.HashPassword(req.Password)
	confirmationCode := strHelper.Rand(16)

	a := &pb.Account{
		Uuid:             uuid.String(),
		Email:            req.Email,
		FullName:         req.FullName,
		PasswordHash:     passwordHash,
		PasswordSalt:     passwordSalt,
		ConfirmationCode: confirmationCode,
	}

	// Check if already exists first
	rows, err := s.DB.Queryx(`SELECT account_id FROM account WHERE email = ? AND deleted_at = '0'`, req.Email)
	if err != nil {
		logger.Errorf("Error while checking for existing account: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing account.")
	}
	if rows.Next() {
		logger.Errorf("An account already exists with the e-mail address '%s'.", req.Email)
		return nil, grpc.Errorf(codes.AlreadyExists, "")
	}

	_, err = s.DB.NamedExec("INSERT INTO account (uuid, email, full_name, password_hash, password_salt, confirmation_code) VALUES (unhex(replace(:uuid,'-','')), :email, :full_name, :password_hash, :password_salt, :confirmation_code)", a)
	if err != nil {
		logger.Errorf("Cannot create database entry: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Created account '%s'.", a.GetUuid())

	err = s.DB.Get(a, "SELECT * FROM account WHERE uuid = unhex(replace(?,'-',''))", a.Uuid)
	if err != nil {
		logger.Errorf("Error while fetching the new account: %v", err)
		return nil, err
	}
	a.Uuid = uuid.String()

	return a, nil
}

// CreateAccountWithExternalLogin creates an Account with external login info
func (s *Server) CreateAccountWithExternalLogin(ctx context.Context, req *pb.CreateAccountWithExternalLoginRequest) (*pb.Account, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateAccountWithExternalLogin: %v", req)
	defer timeTrack(logger, time.Now(), "CreateAccountWithExternalLogin")

	// validations
	req.ExternalUserId = strings.TrimSpace(req.ExternalUserId)
	if req.ExternalUserId == "" {
		logger.Error("The external user ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The external user ID is required.")
	}
	req.ExternalProviderName = strings.TrimSpace(req.ExternalProviderName)
	if req.ExternalProviderName == "" {
		logger.Error("The external provider name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The external provider name is required.")
	}
	// Validate the provider name
	authProviders, err := s.GetExternalAuthProviders()
	if err != nil {
		logger.Errorf("Cannot fetch the external auth providers: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot fetch the external auth providers")
	}
	authProviderID, ok := authProviders[req.ExternalProviderName]
	if !ok {
		logger.Error("The external provider name is not valid.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The external provider name is not valid.")
	}

	// Create the account
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}

	req.ProfileImageUrl = strings.TrimSpace(req.ProfileImageUrl)

	// build the full name
	req.FirstName = strings.TrimSpace(req.FirstName)
	req.LastName = strings.TrimSpace(req.LastName)
	fullName := req.FirstName
	if req.LastName != "" {
		if fullName != "" {
			fullName += " "
		}
		fullName += req.LastName
	}

	a := &pb.Account{
		Uuid:            uuid.String(),
		Email:           req.Email,
		FullName:        fullName,
		ProfileImageUrl: req.ProfileImageUrl,
	}

	// Check if already exists first by e-mail
	req.Email = strings.TrimSpace(req.Email)
	if req.Email != "" {
		rows, err := s.DB.Queryx(`SELECT account_id FROM account WHERE email = ? AND deleted_at = '0'`, req.Email)
		if err != nil {
			logger.Errorf("Error while checking for existing account: %v", err)
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing account.")
		}
		if rows.Next() {
			logger.Errorf("An account already exists with the e-mail address '%s'.", req.Email)
			return nil, grpc.Errorf(codes.AlreadyExists, "")
		}
	}

	// Check if already exists by external user ID
	rows, err := s.DB.Queryx(`SELECT account_id
		FROM account_external_login ael
		INNER JOIN external_authentication_provider eap ON eap.external_authentication_provider_id = ael.external_authentication_provider_id
		WHERE ael.external_user_id = ? AND eap.name = ? AND ael.deleted_at = '0'`, req.ExternalUserId, req.ExternalProviderName)
	if err != nil {
		logger.Errorf("Error while checking for existing external login: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing external login.")
	}
	if rows.Next() {
		logger.Errorf("An account already exists with this external login information '%s' (%s).", req.ExternalUserId, req.ExternalProviderName)
		return nil, grpc.Errorf(codes.AlreadyExists, "")
	}

	tx := s.DB.MustBegin()

	res, err := tx.NamedExec("INSERT INTO account (uuid, email, full_name, profile_image_url, confirmed_at) VALUES (unhex(replace(:uuid,'-','')), :email, :full_name, :profile_image_url, CURRENT_TIMESTAMP)", a)
	if err != nil {
		logger.Errorf("Cannot create database entry: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	newAccountID, _ := res.LastInsertId()
	_, err = tx.Exec(
		`INSERT INTO account_external_login (account_id, external_authentication_provider_id, external_user_id, email, first_name, last_name)
		VALUES (?, ?, ?, ?, ?, ?)`,
		newAccountID, authProviderID, req.ExternalUserId, req.Email, req.FirstName, req.LastName,
	)
	if err != nil {
		tx.Rollback()

		logger.Errorf("Cannot create database account_external_login entry: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database account_external_login entry.")
	}

	tx.Commit()
	logger.Debugf("Created account '%s'.", a.GetUuid())

	err = s.DB.Get(a, "SELECT * FROM account WHERE uuid = unhex(replace(?,'-',''))", a.Uuid)
	if err != nil {
		logger.Errorf("Error while fetching the new account: %v", err)
		return nil, err
	}
	a.Uuid = uuid.String()

	return a, nil
}

// GetAccount retrieves an Account by UUID, e-mail or ID
func (s *Server) GetAccount(ctx context.Context, req *pb.GetAccountRequest) (*pb.Account, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetAccount: %v", req)
	defer timeTrack(logger, time.Now(), "GetAccount")

	// validations
	if req.Id == 0 && req.Search == "" {
		logger.Error("Either the ID, the e-mail or the UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Either the ID, the e-mail or the UUID is required.")
	}

	var account pb.Account
	err := s.DB.Get(&account, `SELECT *
		FROM account
		WHERE (uuid = UNHEX(REPLACE(?,'-','')) OR email = ? OR account_id = ?)
			AND deleted_at = '0'`, req.Search, req.Search, req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find account '%s' (%d).", req.Search, req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching account")
	}

	unpackedUUID, err := crypto.Parse([]byte(account.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	account.Uuid = unpackedUUID.String()

	// fetch the roles
	rows, err := s.DB.Queryx(`SELECT r.*
		FROM account_role ar
		INNER JOIN role r ON ar.role_id = r.role_id
		WHERE ar.account_ID =?`, account.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the roles.")
	}
	for rows.Next() {
		role := pb.Role{}
		err := rows.StructScan(&role)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the rows.")
		}
		account.Roles = append(account.Roles, &role)
	}

	return &account, nil
}

// GetAccountsToSync fetches a list of accounts to sync
func (s *Server) GetAccountsToSync(ctx context.Context, req *pb.GetAccountsToSyncRequest) (*pb.AccountsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetAccountsToSync: %v", req)
	defer timeTrack(logger, time.Now(), "GetAccountsToSync")

	rows, err := s.DB.Queryx(`SELECT a.*
		FROM account a
		INNER JOIN account_client ac ON a.account_id = ac.account_id
		INNER JOIN client c ON c.client_id = ac.client_id
		WHERE a.deleted_at = '0'
			AND c.deleted_at = '0'
			AND TIME_TO_SEC(TIMEDIFF(NOW(), purchases_synced_at)) >= ?
		GROUP BY a.account_id
		ORDER BY purchases_synced_at ASC
		LIMIT ?`, req.SyncInterval, req.Limit)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the accounts.")
	}

	accountsList := &pb.AccountsList{}
	for rows.Next() {
		account := pb.Account{}
		err := rows.StructScan(&account)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the rows.")
		}

		unpackedUUID, err := crypto.Parse([]byte(account.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		account.Uuid = unpackedUUID.String()

		accountsList.Accounts = append(accountsList.Accounts, &account)
	}

	return accountsList, nil
}

// UpdateAccount updates an Account
func (s *Server) UpdateAccount(ctx context.Context, req *pb.Account) (*pb.Account, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateAccount: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateAccount")

	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Error("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}
	req.FullName = strings.TrimSpace(req.FullName)
	if req.FullName == "" {
		logger.Error("The name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The name is required.")
	}
	if len(req.FullName) < NameMinLength {
		logger.Errorf("A name of minimum %d characters is required.", NameMinLength)
		return nil, grpc.Errorf(codes.InvalidArgument, "A name of minimum %d characters is required.", NameMinLength)
	}
	if req.Gender == "" {
		req.Gender = "NA"
	}

	_, err := s.DB.NamedExec(`UPDATE account
		SET full_name = :full_name, gender = :gender, profile_image_url = :profile_image_url
		WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var account pb.Account
	err = s.DB.Get(&account, "SELECT * FROM account WHERE uuid = unhex(replace(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateAccount - account not found: %s", req.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	account.Uuid = req.Uuid

	return &account, nil
}

// ChangeAccountPassword updates an account's password
func (s *Server) ChangeAccountPassword(ctx context.Context, req *pb.ChangeAccountPasswordRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("ChangeAccountPassword: %v", req)
	defer timeTrack(logger, time.Now(), "ChangeAccountPassword")

	// Validations
	req.AccountUuid = strings.TrimSpace(req.AccountUuid)
	if req.AccountUuid == "" {
		logger.Errorf("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}
	newPassword := strings.TrimSpace(req.NewPassword)
	if newPassword == "" {
		logger.Errorf("The new password is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The new password is required.")
	}
	if len(req.NewPassword) < PasswordMinLength {
		logger.Errorf("A password of minimum %d characters is required.", PasswordMinLength)
		return nil, grpc.Errorf(codes.InvalidArgument, "A password of minimum %d characters is required.", PasswordMinLength)
	}
	currentPassword := strings.TrimSpace(req.CurrentPassword)
	if len(req.CurrentPassword) > 0 && currentPassword == "" {
		logger.Errorf("The current password contains only spaces.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The current password contains only spaces.")
	}

	// Get the account
	var account pb.Account
	err := s.DB.Get(&account, `SELECT *
		FROM account
		WHERE uuid = UNHEX(REPLACE(?,'-',''))
			AND deleted_at = '0'`, req.AccountUuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("The account doesn't exist.")
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching account")
	}

	if req.CurrentPassword != "" {
		// Check the current password
		passwordHash := crypto.HashPasswordWithSalt(req.CurrentPassword, account.PasswordSalt)
		if passwordHash != account.PasswordHash {
			logger.Errorf("The provided current password is not correct.")
			return nil, grpc.Errorf(codes.InvalidArgument, "The provided current password is not correct.")
		}
	}

	passwordHash, passwordSalt := crypto.HashPassword(req.NewPassword)

	tx := s.DB.MustBegin()

	_, err = tx.Exec(`UPDATE account
		SET password_hash = ?, password_salt = ?
		WHERE deleted_at = '0' AND account_id = ?`, passwordHash, passwordSalt, account.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	// Invalidate all the password change requests.
	_, err = tx.Exec(`UPDATE password_change_request
		SET valid_until = created_at
		WHERE account_id = ?`, account.Id)
	if err != nil {
		tx.Rollback()

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - invalidate requests.")
	}

	tx.Commit()

	return &empty.Empty{}, nil
}

// DeleteAccount removes an Account (sets deleted_at)
func (s *Server) DeleteAccount(ctx context.Context, req *pb.DeleteAccountRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteAccount: %v", req)
	defer timeTrack(logger, time.Now(), "HasAccountPermission")

	if req.Uuid == "" {
		logger.Errorf("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}

	result, err := s.DB.Exec("UPDATE account SET email = CONCAT('deleted-', email), deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteAccount - account not found: %s", req.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// ConfirmAccount confirms an Account
func (s *Server) ConfirmAccount(ctx context.Context, req *pb.ConfirmAccountRequest) (*pb.Account, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("ConfirmAccount: %v", req)
	defer timeTrack(logger, time.Now(), "ConfirmAccount")

	req.ConfirmationCode = strings.TrimSpace(req.ConfirmationCode)
	if req.ConfirmationCode == "" {
		logger.Error("The confirmation code is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The confirmation code is required.")
	}
	req.AccountUuid = strings.TrimSpace(req.AccountUuid)
	if req.AccountUuid == "" {
		logger.Error("Account UUID must be specified.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}

	// Check the account first
	var account pb.Account
	err := s.DB.Get(&account, "SELECT * FROM account WHERE uuid = unhex(replace(?,'-',''))", req.AccountUuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("ConfirmAccount - account not found: %s", req.AccountUuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	account.Uuid = req.AccountUuid

	// Check if already confirmed
	if account.ConfirmedAt != "0000-00-00 00:00:00" {
		logger.Errorf("ConfirmAccount - account already confirmed: %s", req.AccountUuid)
		return nil, grpc.Errorf(codes.AlreadyExists, "")
	}

	_, err = s.DB.Exec(`UPDATE account
		SET confirmed_at = CURRENT_TIMESTAMP
		WHERE
			deleted_at = '0'
			AND confirmed_at ='0'
			AND uuid = UNHEX(REPLACE(?,'-',''))
			AND confirmation_code = ?`, req.AccountUuid, req.ConfirmationCode)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	account.ConfirmedAt = time.Now().Format(sqlDatetimeFormat)

	return &account, nil
}

// GetAccountByExternalUserID retrieves an Account by the given external provider name
// and external user ID
func (s *Server) GetAccountByExternalUserID(ctx context.Context, req *pb.GetAccountByExternalUserIDRequest) (*pb.Account, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetAccountByExternalUserID: %v", req)
	defer timeTrack(logger, time.Now(), "GetAccountByExternalUserID")

	// validations
	req.ExternalProviderName = strings.TrimSpace(req.ExternalProviderName)
	if req.ExternalProviderName == "" {
		logger.Error("The external provider name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The external provider name is required.")
	}
	req.ExternalUserId = strings.TrimSpace(req.ExternalUserId)
	if req.ExternalUserId == "" {
		logger.Error("The external user ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The external user ID is required.")
	}

	var account pb.Account
	err := s.DB.Get(&account, `SELECT a.*
		FROM account_external_login ael
		INNER JOIN account a ON a.account_id = ael.account_id
		INNER JOIN external_authentication_provider eap ON eap.external_authentication_provider_id = ael.external_authentication_provider_id
		WHERE
			eap.name = ?
			AND ael.external_user_id = ?
			AND ael.deleted_at = '0'
			AND a.deleted_at = '0'`, req.ExternalProviderName, req.ExternalUserId)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find account for external user '%s' (%s).", req.ExternalUserId, req.ExternalProviderName)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching account")
	}

	unpackedUUID, err := crypto.Parse([]byte(account.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	account.Uuid = unpackedUUID.String()

	role := pb.Role{}
	// fetch the roles
	rows, err := s.DB.Queryx(`SELECT r.*
		FROM account_role ar
		INNER JOIN role r ON ar.role_id = r.role_id
		WHERE ar.account_ID =?`, account.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the roles.")
	}
	for rows.Next() {
		err := rows.StructScan(&role)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the rows.")
		}
		account.Roles = append(account.Roles, &role)
	}

	return &account, nil
}

// GetExternalAuthProviders retrieves the external auth providers list
// TODO: cache this into Redis
func (s *Server) GetExternalAuthProviders() (map[string]int, error) {
	providers := map[string]int{}

	rows, err := s.DB.Queryx(`SELECT external_authentication_provider_id, name FROM external_authentication_provider`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var name string

		err := rows.Scan(&id, &name)
		if err != nil {
			return nil, fmt.Errorf("Error while scanning a session type: %v", err)
		}

		providers[name] = id
	}

	return providers, nil
}

// SyncAccountsClientsWithMB queues the given Accounts to the Sync account clients queue
func (s *Server) SyncAccountsClientsWithMB(ctx context.Context, req *pb.SyncAccountsClientsWithMBRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("SyncAccountsClientsWithMB: %v", req)
	defer timeTrack(logger, time.Now(), "SyncAccountsClientsWithMB")

	if len(req.Uuids) == 0 {
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one site UUID is required.")
	}

	go func() {
		for _, uuid := range req.Uuids {
			messageData := queueHelper.NewSyncAccountClientsHistoryMessage(uuid)
			message, _ := json.Marshal(messageData)
			s.Queue.Publish(conf.Queue.SyncTopic, message)
		}
	}()

	return &empty.Empty{}, nil
}
