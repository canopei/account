package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/empty"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"golang.org/x/net/context"

	pb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
)

const (
	// MaxClientPurchasesFetched is the limit of number of client purchases that can be fetched
	// at once.
	MaxClientPurchasesFetched int32 = 50
)

// CreateClientPurchase creates a ClientPurchase
func (s *Server) CreateClientPurchase(ctx context.Context, req *pb.ClientPurchase) (*pb.ClientPurchase, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateClientPurchase: %v", req)
	defer timeTrack(logger, time.Now(), "CreateClientPurchase")

	// Validations
	if req.PaymentId < 1 {
		logger.Error("The payment ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The payment ID is required.")
	}
	if req.SaleMbId < 1 {
		logger.Error("The sale MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The sale MB ID is required.")
	}
	if req.ClientId < 1 {
		logger.Error("The client ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client ID is required.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	if req.Quantity == 0 {
		logger.Error("The quantity is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The quantity is required.")
	}

	// Check if a ClientPurchase with this sale ID already exists
	err := s.DB.Get(req, "SELECT * FROM client_purchase WHERE sale_mb_id = ?", req.SaleMbId)
	if err != sql.ErrNoRows {
		if err == nil {
			logger.Errorf("A purchase already exists for this sale ID (%d).", req.SaleMbId)
			return nil, grpc.Errorf(codes.Aborted, "A purchase already exists for this sale ID (%d).", req.SaleMbId)
		}
		return nil, grpcUtils.InternalError(logger, err, "Error while checking the client purchase.")
	}

	// Create the client
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	_, err = s.DB.NamedExec(`INSERT INTO client_purchase
			(uuid, sale_mb_id, client_id, site_id, location_id, description, price, amount_paid, discount, tax, returned, quantity, sale_time,
			payment_id, payment_type, payment_method, payment_notes)
		VALUES
			(unhex(replace(:uuid,'-','')), :sale_mb_id, :client_id, :site_id, :location_id, :description, :price,
			:amount_paid, :discount, :tax, :returned, :quantity, :sale_time, :payment_id, :payment_type, :payment_method,
			:payment_notes)`, req)
	if err != nil {
		logger.Errorf("Cannot create database entry: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Created client purchase '%s'.", req.Uuid)

	err = s.DB.Get(req, "SELECT * FROM client_purchase WHERE uuid = unhex(replace(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the new client purchase.")
	}
	req.Uuid = uuid.String()

	return req, nil
}

// GetClientPurchase retrieves a ClientPurchase by UUID or ID
func (s *Server) GetClientPurchase(ctx context.Context, req *pb.ClientPurchaseRequest) (*pb.ClientPurchase, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClientPurchase: %v", req)
	defer timeTrack(logger, time.Now(), "GetClientPurchase")

	if req.Uuid == "" {
		logger.Error("The purchase UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The purchase UUID is required.")
	}

	ID := int32(0)
	if !strings.Contains(req.Uuid, "-") {
		rid, err := strconv.ParseInt(req.Uuid, 10, 32)
		if err != nil {
			logger.Error("The provided ID is out of range.")
			return nil, grpc.Errorf(codes.InvalidArgument, "The provided ID is out of range.")
		}
		ID = int32(rid)
	}

	var clientPurchase pb.ClientPurchase
	err := s.DB.Get(&clientPurchase, `SELECT *
		FROM client_purchase
		WHERE (uuid = UNHEX(REPLACE(?,'-','')) OR client_id = ?)
			AND deleted_at = '0'`, req.Uuid, ID)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find client purchase '%s' (%d).", req.Uuid, ID)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the client purchase")
	}

	unpackedUUID, err := crypto.Parse([]byte(clientPurchase.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	clientPurchase.Uuid = unpackedUUID.String()

	return &clientPurchase, nil
}

// GetClientPurchases fetches a list of client purchases
func (s *Server) GetClientPurchases(ctx context.Context, req *pb.GetClientPurchasesRequest) (*pb.ClientPurchasesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClientPurchases: %v", req)
	defer timeTrack(logger, time.Now(), "GetClientPurchases")

	if req.Limit == 0 || req.Limit > MaxClientPurchasesFetched {
		req.Limit = MaxClientPurchasesFetched
	}

	logger.Debugf("Fetching %d client purchases, offset %d...", req.Limit, req.Offset)

	// Fetch the sites
	queryParams := map[string]interface{}{
		"limit":  req.Limit,
		"offset": req.Offset,
	}

	condition := ""
	// Search by client UUID
	if req.ClientUuid != "" {
		queryParams["client_uuid"] = req.ClientUuid
		condition += " AND c.uuid = :client_uuid"
	}

	rows, err := s.DB.NamedQuery(`SELECT cp.*, c.uuid "client_uuid"
		FROM client_purchase cp
		INNER JOIN client c ON c.client_id = cp.client_id
		WHERE 1=1 `+condition+`
		ORDER BY cp.client_purchase_id DESC
		LIMIT :limit
		OFFSET :offset`, queryParams)
	if err != nil {
		logger.Errorf("Error while fetching the client purchases: %v", err)
		return nil, err
	}

	clientPurchasesList := &pb.ClientPurchasesList{}
	for rows.Next() {
		clientPurchase := pb.ClientPurchase{}
		err := rows.StructScan(&clientPurchase)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the client purchase")
		}

		unpackedUUID, err := crypto.Parse([]byte(clientPurchase.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		clientPurchase.Uuid = unpackedUUID.String()

		clientPurchasesList.ClientPurchases = append(clientPurchasesList.ClientPurchases, &clientPurchase)
	}

	return clientPurchasesList, nil
}

// GetClientPurchasesByMBID fetches a list of client purchases by their MB IDs
func (s *Server) GetClientPurchasesByMBID(ctx context.Context, req *pb.GetClientsPurchasesByMBIDRequest) (*pb.ClientPurchasesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClientPurchasesByMBID: %d IDs", len(req.MbIds))
	defer timeTrack(logger, time.Now(), "GetClientPurchasesByMBID")

	// Validations
	if len(req.MbIds) == 0 {
		logger.Error("At least one MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one MB ID is required.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}

	// Fetch the clients
	queryParams := map[string]interface{}{
		"site_id": req.SiteId,
	}
	condition := " AND sale_mb_id IN ("
	for k, v := range req.MbIds {
		paramKey := "id" + strconv.Itoa(k)
		condition += ":" + paramKey + ", "
		queryParams[paramKey] = v
	}
	condition = strings.TrimSuffix(condition, ", ") + ")"

	rows, err := s.DB.NamedQuery(`SELECT *
		FROM client_purchase
		WHERE site_id = :site_id`+condition+`
		ORDER BY client_purchase_id DESC`, queryParams)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		return nil, err
	}

	clientPurchasesList := &pb.ClientPurchasesList{}
	for rows.Next() {
		clientPurchases := pb.ClientPurchase{}
		err := rows.StructScan(&clientPurchases)
		if err != nil {
			fmt.Println(err)
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the client purchases")
		}

		unpackedUUID, err := crypto.Parse([]byte(clientPurchases.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		clientPurchases.Uuid = unpackedUUID.String()

		clientPurchasesList.ClientPurchases = append(clientPurchasesList.ClientPurchases, &clientPurchases)
	}

	return clientPurchasesList, nil
}

// GetClientPurchasesByClientUUID fetches a list of client purchases for a given Client (UUID)
func (s *Server) GetClientPurchasesByClientUUID(ctx context.Context, req *pb.GetClientPurchasesRequest) (*pb.ClientPurchasesList, error) {
	return s.GetClientPurchases(ctx, req)
}

// UpdateClientPurchase updates a ClientPurchase
func (s *Server) UpdateClientPurchase(ctx context.Context, req *pb.ClientPurchase) (*pb.ClientPurchase, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateClientPurchase: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateClientPurchase")

	if req.Uuid == "" {
		logger.Error("The client purchase UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client purchase UUID is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE client_purchase SET
			price = :price, amount_paid = :amount_paid, discount = :discount, tax = :tax, returned = :returned, quantity = :quantity,
			payment_notes = :payment_notes, payment_id = :payment_id, payment_type = :payment_type, payment_method = :payment_method,
			description = :description,
			sale_time = CASE WHEN :sale_time = '0000-00-00' THEN sale_time ELSE :sale_time END
		WHERE uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var clientPurchase pb.ClientPurchase
	err = s.DB.Get(&clientPurchase, "SELECT * FROM client_purchase WHERE uuid = unhex(replace(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateClientPurchase - client purchase not found: %s", req.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	clientPurchase.Uuid = req.Uuid

	return &clientPurchase, nil
}

// DeleteClientPurchase removes a ClientPurchase
func (s *Server) DeleteClientPurchase(ctx context.Context, req *pb.ClientPurchaseRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClientPurchase: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteClientPurchase")

	if req.Uuid == "" {
		logger.Error("The client purchase UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client purchase UUID is required.")
	}

	result, err := s.DB.Exec("DELETE FROM client_purchase WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteClientPurchase - client purchase not found: %s", req.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
