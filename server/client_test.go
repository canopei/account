package main

import (
	"context"
	"testing"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	clientColumns = []string{"client_id", "uuid", "site_id", "mb_id", "first_name", "middle_name", "last_name", "gender",
		"email", "email_optin", "city", "country", "state", "status", "username", "is_company", "inactive", "created_at",
		"modified_at", "deleted_at"}
)

func TestCreateClientValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.Client
		ExpectedCode codes.Code
	}{
		{&accountPb.Client{SiteId: 1}, codes.InvalidArgument},
		{&accountPb.Client{MbId: 1}, codes.InvalidArgument},
		{&accountPb.Client{}, codes.InvalidArgument},
		{&accountPb.Client{SiteId: 123, MbId: -1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.Client{SiteId: 123, MbId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.CreateClient(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateClientExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	request := &accountPb.Client{SiteId: 1, MbId: 123}

	rows := sqlmock.NewRows(clientColumns).AddRow(1, "123456789012345", 1, 123, "fname", "mname", "lname", 1, "foo@bar.com",
		1, "city", "country", "state", "status", "username", 1, 0, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client WHERE").WillReturnRows(rows)

	client, err := server.CreateClient(context.Background(), request)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(client)
	assert.NotNil(err)
	assert.Equal(codes.Aborted, grpc.Code(err))
}

func TestCreateClientSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	request := &accountPb.Client{SiteId: 1, MbId: 123}

	mock.ExpectQuery("^SELECT (.+) FROM client WHERE").WillReturnRows(sqlmock.NewRows(clientColumns))
	mock.ExpectExec("^INSERT INTO client").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(clientColumns).AddRow(1, "123456789012345", 1, 123, "fname", "mname", "lname", 1, "foo@bar.com",
		1, "city", "country", "state", "status", "username", 1, 0, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client WHERE").WillReturnRows(rows)

	client, err := server.CreateClient(context.Background(), request)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(client)
	// Sanity checks
	if client != nil {
		assert.Equal("foo@bar.com", client.Email)
		assert.Equal("state", client.State)
	}
}

func TestGetClientValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetClientRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetClientRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetClientRequest{Uuid: "test-uuid"}, codes.Unknown},
		// If it has no dash, it will be converted to int
		{&accountPb.GetClientRequest{Uuid: "nodash"}, codes.InvalidArgument},
		{&accountPb.GetClientRequest{Uuid: "12345"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.GetClient(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClientMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Test with UUID
	// No rows returned
	rows := sqlmock.NewRows(clientColumns)
	mock.ExpectQuery("^SELECT (.+) FROM client WHERE").WithArgs("test-uuid", 0).WillReturnRows(rows)

	client, err := server.GetClient(context.Background(), &accountPb.GetClientRequest{Uuid: "test-uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(client)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Test with ID
	// No rows returned
	rows = sqlmock.NewRows(clientColumns)
	mock.ExpectQuery("^SELECT (.+) FROM client WHERE").WithArgs("12345", 12345).WillReturnRows(rows)

	client, err = server.GetClient(context.Background(), &accountPb.GetClientRequest{Uuid: "12345"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(client)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClientSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// The account SELECT query
	rows := sqlmock.NewRows(clientColumns).AddRow(1, "1234567890123456", 1, 123, "fname", "mname", "lname", 1, "foo@bar.com",
		1, "city", "country", "state", "status", "username", 1, 0, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client WHERE").WithArgs("test-uuid", 0).WillReturnRows(rows)

	client, err := server.GetClient(context.Background(), &accountPb.GetClientRequest{Uuid: "test-uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(client)
	// Sanity checks
	if client != nil {
		assert.Equal("foo@bar.com", client.Email)
		assert.Equal("username", client.Username)
	}
}

func TestDeleteClientValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.DeleteClientRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.DeleteClientRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.DeleteClientRequest{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.DeleteClient(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClient(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE client SET").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteClient(context.Background(), &accountPb.DeleteClientRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE client SET").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 1))

	_, err = server.DeleteClient(context.Background(), &accountPb.DeleteClientRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestUpdateClientValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.Client
		ExpectedCode codes.Code
	}{
		{&accountPb.Client{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.Client{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.UpdateClient(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateClientMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE client SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM client WHERE").WithArgs("foo").WillReturnRows(sqlmock.NewRows(clientColumns))

	account, err := server.UpdateClient(context.Background(), &accountPb.Client{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateClientSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClient := &accountPb.Client{
		Uuid:       "123456789012345",
		Gender:     1,
		Email:      "",
		EmailOptin: true,
		City:       "city",
		State:      "state",
		Country:    "country",
		Status:     "status",
		Username:   "username",
		IsCompany:  false,
		FirstName:  "fname",
		LastName:   "lname",
		MiddleName: "mname",
		Inactive:   false,
	}

	mock.ExpectExec("^UPDATE client SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(clientColumns).
		AddRow(1, "1234567890123456", 1, 123, "fname", "mname", "lname", 1, "foo@bar.com",
			1, "city", "country", "state", "status", "username", 1, 0, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM client WHERE").WithArgs(testClient.Uuid).WillReturnRows(rows)

	client, err := server.UpdateClient(context.Background(), testClient)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(client)
}

func TestGetClientsSimpleEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM client WHERE (.+) LIMIT \\? OFFSET \\?").WithArgs(10, 15).WillReturnRows(sqlmock.NewRows(clientColumns))
	clients, err := server.GetClients(context.Background(), &accountPb.GetClientsRequest{Limit: 10, Offset: 15})
	_ = clients
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clients)
	if clients != nil {
		assert.Empty(clients.Clients)
	}
}

func TestGetClientsSimpleNotEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	clients := []*accountPb.Client{
		&accountPb.Client{
			Id: 1, Uuid: "1234567890123456", SiteId: 1, MbId: 123, Gender: 1, Email: "", EmailOptin: true, City: "city", State: "state", Country: "country",
			Status: "status", Username: "username", IsCompany: false, FirstName: "fname", LastName: "lname", MiddleName: "mname", Inactive: false,
			CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03",
		},
		&accountPb.Client{
			Id: 2, Uuid: "1234567890123456", SiteId: 1, MbId: 124, Gender: 1, Email: "", EmailOptin: true, City: "city", State: "state", Country: "country",
			Status: "status", Username: "username", IsCompany: false, FirstName: "fname", LastName: "lname", MiddleName: "mname", Inactive: false,
			CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03",
		},
		&accountPb.Client{
			Id: 3, Uuid: "1234567890123456", SiteId: 2, MbId: 125, Gender: 1, Email: "", EmailOptin: true, City: "city", State: "state", Country: "country",
			Status: "status", Username: "username", IsCompany: false, FirstName: "fname", LastName: "lname", MiddleName: "mname", Inactive: false,
			CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03",
		},
	}

	rows := sqlmock.NewRows(clientColumns)
	for _, c := range clients {
		rows.AddRow(c.Id, c.Uuid, c.SiteId, c.MbId, c.FirstName, c.MiddleName, c.LastName, c.Gender, c.Email, c.EmailOptin, c.City, c.Country,
			c.State, c.Status, c.Username, c.IsCompany, c.Inactive, c.CreatedAt, c.ModifiedAt, c.DeletedAt)
	}

	mock.ExpectQuery("^SELECT (.+) FROM client WHERE (.+) LIMIT \\? OFFSET \\?").WithArgs(10, 15).WillReturnRows(rows)
	response, err := server.GetClients(context.Background(), &accountPb.GetClientsRequest{Limit: 10, Offset: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Clients, len(clients))
	}
}

func TestGetClientsWithFilters(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM client WHERE (.+) LIMIT \\? OFFSET \\?").WithArgs("uuid1", "uuid2", "foo@bar.com", 10, 15).WillReturnRows(sqlmock.NewRows(clientColumns))
	clients, err := server.GetClients(context.Background(), &accountPb.GetClientsRequest{
		Limit: 10, Offset: 15, Email: "foo@bar.com", Ids: []string{"uuid1", "uuid2"},
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(clients)
	if clients != nil {
		assert.Empty(clients.Clients)
	}
}

func TestGetClientsByMBIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetClientsByMBIDRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetClientsByMBIDRequest{}, codes.InvalidArgument},
		{&accountPb.GetClientsByMBIDRequest{MbIds: []int64{}}, codes.InvalidArgument},
		{&accountPb.GetClientsByMBIDRequest{SiteId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetClientsByMBIDRequest{SiteId: 1, MbIds: []int64{123, 456}}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.GetClientsByMBID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClientsByMBID(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	clients := []*accountPb.Client{
		&accountPb.Client{
			Id: 1, Uuid: "1234567890123456", SiteId: 1, MbId: 123, Gender: 1, Email: "", EmailOptin: true, City: "city", State: "state", Country: "country",
			Status: "status", Username: "username", IsCompany: false, FirstName: "fname", LastName: "lname", MiddleName: "mname", Inactive: false,
			CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03",
		},
		&accountPb.Client{
			Id: 2, Uuid: "1234567890123456", SiteId: 1, MbId: 124, Gender: 1, Email: "", EmailOptin: true, City: "city", State: "state", Country: "country",
			Status: "status", Username: "username", IsCompany: false, FirstName: "fname", LastName: "lname", MiddleName: "mname", Inactive: false,
			CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03",
		},
		&accountPb.Client{
			Id: 3, Uuid: "1234567890123456", SiteId: 2, MbId: 125, Gender: 1, Email: "", EmailOptin: true, City: "city", State: "state", Country: "country",
			Status: "status", Username: "username", IsCompany: false, FirstName: "fname", LastName: "lname", MiddleName: "mname", Inactive: false,
			CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03",
		},
	}

	rows := sqlmock.NewRows(clientColumns)
	for _, c := range clients {
		rows.AddRow(c.Id, c.Uuid, c.SiteId, c.MbId, c.FirstName, c.MiddleName, c.LastName, c.Gender, c.Email, c.EmailOptin, c.City, c.Country,
			c.State, c.Status, c.Username, c.IsCompany, c.Inactive, c.CreatedAt, c.ModifiedAt, c.DeletedAt)
	}

	mock.ExpectQuery("^SELECT (.+) FROM client WHERE").WillReturnRows(rows)

	response, err := server.GetClientsByMBID(context.Background(), &accountPb.GetClientsByMBIDRequest{SiteId: 1, MbIds: []int64{123, 456}})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.Len(response.Clients, len(clients))
	// Sanity check
	assert.Equal(clients[1].MbId, response.Clients[1].MbId)
}

func TestDeleteClientsForSiteIDExceptMBIDsValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.DeleteClientsForSiteIDExceptMBIDsRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.DeleteClientsForSiteIDExceptMBIDsRequest{}, codes.InvalidArgument},
		{&accountPb.DeleteClientsForSiteIDExceptMBIDsRequest{SiteID: 1}, codes.InvalidArgument},
		{&accountPb.DeleteClientsForSiteIDExceptMBIDsRequest{SiteID: 1, MBIDs: []int64{}}, codes.InvalidArgument},
		{&accountPb.DeleteClientsForSiteIDExceptMBIDsRequest{MBIDs: []int64{123, 456}}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.DeleteClientsForSiteIDExceptMBIDsRequest{SiteID: 1, MBIDs: []int64{123, 456}}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.DeleteClientsForSiteIDExceptMBIDs(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClientsForSiteIDExceptMBIDs(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE client SET (.*)deleted_at = CURRENT_TIMESTAMP").WithArgs(1, 123, 456).WillReturnResult(sqlmock.NewResult(0, 3))

	_, err = server.DeleteClientsForSiteIDExceptMBIDs(context.Background(), &accountPb.DeleteClientsForSiteIDExceptMBIDsRequest{
		SiteID: 1,
		MBIDs:  []int64{123, 456},
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestCreateAccountClientLinkValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.AccountClientLinkRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.AccountClientLinkRequest{}, codes.InvalidArgument},
		{&accountPb.AccountClientLinkRequest{AccountUuid: "foo"}, codes.InvalidArgument},
		{&accountPb.AccountClientLinkRequest{ClientUuid: "bar"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.AccountClientLinkRequest{AccountUuid: "foo", ClientUuid: "bar"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.CreateAccountClientLink(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateAccountClientLinkExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT COUNT\\(\\*\\) FROM account_client").WillReturnRows(sqlmock.NewRows([]string{"count(*)"}).AddRow(1))

	_, err = server.CreateAccountClientLink(context.Background(), &accountPb.AccountClientLinkRequest{
		AccountUuid: "foo",
		ClientUuid:  "bar",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestCreateAccountClientLinkSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT COUNT\\(\\*\\) FROM account_client").WithArgs("foo", "bar").WillReturnRows(sqlmock.NewRows([]string{"count(*)"}).AddRow(0))
	mock.ExpectExec("^INSERT INTO account_client").WithArgs("foo", "bar").WillReturnResult(sqlmock.NewResult(1, 1))

	_, err = server.CreateAccountClientLink(context.Background(), &accountPb.AccountClientLinkRequest{
		AccountUuid: "foo",
		ClientUuid:  "bar",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestDeleteAccountClientLinkValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.AccountClientLinkRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.AccountClientLinkRequest{}, codes.InvalidArgument},
		{&accountPb.AccountClientLinkRequest{AccountUuid: "foo"}, codes.InvalidArgument},
		{&accountPb.AccountClientLinkRequest{ClientUuid: "bar"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.AccountClientLinkRequest{AccountUuid: "foo", ClientUuid: "bar"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.DeleteAccountClientLink(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteAccountClientLinkMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^DELETE FROM account_client").WithArgs("foo", "bar").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteAccountClientLink(context.Background(), &accountPb.AccountClientLinkRequest{
		AccountUuid: "foo",
		ClientUuid:  "bar",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestDeleteAccountClientLinkSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^DELETE FROM account_client").WithArgs("foo", "bar").WillReturnResult(sqlmock.NewResult(0, 1))

	_, err = server.DeleteAccountClientLink(context.Background(), &accountPb.AccountClientLinkRequest{
		AccountUuid: "foo",
		ClientUuid:  "bar",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestGetAccountsClientsValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetAccountsClientsRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetAccountsClientsRequest{}, codes.InvalidArgument},
		{&accountPb.GetAccountsClientsRequest{AccountUuids: ","}, codes.InvalidArgument},
		{&accountPb.GetAccountsClientsRequest{AccountUuids: " "}, codes.InvalidArgument},
		{&accountPb.GetAccountsClientsRequest{AccountUuids: " ,  "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetAccountsClientsRequest{AccountUuids: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.GetAccountsClients(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetAccountsClientsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	clients := []*accountPb.Client{
		&accountPb.Client{
			Id: 1, Uuid: "1234567890123456", SiteId: 1, MbId: 123, Gender: 1, Email: "", EmailOptin: true, City: "city", State: "state", Country: "country",
			Status: "status", Username: "username", IsCompany: false, FirstName: "fname", LastName: "lname", MiddleName: "mname", Inactive: false,
			CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03", AccountUuid: "1234567890123456",
		},
		&accountPb.Client{
			Id: 2, Uuid: "1234567890123457", SiteId: 1, MbId: 124, Gender: 1, Email: "", EmailOptin: true, City: "city", State: "state", Country: "country",
			Status: "status", Username: "username", IsCompany: false, FirstName: "fname", LastName: "lname", MiddleName: "mname", Inactive: false,
			CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03", AccountUuid: "1234567890123457",
		},
		&accountPb.Client{
			Id: 3, Uuid: "1234567890123456", SiteId: 2, MbId: 125, Gender: 1, Email: "", EmailOptin: true, City: "city", State: "state", Country: "country",
			Status: "status", Username: "username", IsCompany: false, FirstName: "fname", LastName: "lname", MiddleName: "mname", Inactive: false,
			CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03", AccountUuid: "1234567890123456",
		},
	}

	rows := sqlmock.NewRows(append(clientColumns, "account_uuid"))
	for _, c := range clients {
		rows.AddRow(c.Id, c.Uuid, c.SiteId, c.MbId, c.FirstName, c.MiddleName, c.LastName, c.Gender, c.Email, c.EmailOptin, c.City, c.Country,
			c.State, c.Status, c.Username, c.IsCompany, c.Inactive, c.CreatedAt, c.ModifiedAt, c.DeletedAt, c.AccountUuid)
	}

	mock.ExpectQuery("^SELECT (.+) FROM account_client").WillReturnRows(rows)

	response, err := server.GetAccountsClients(context.Background(), &accountPb.GetAccountsClientsRequest{AccountUuids: "1234567890123456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.Len(response.AccountClients, 2)
}

func TestGetAccountClientsEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM account_client").WillReturnRows(sqlmock.NewRows(append(clientColumns, "account_uuid")))

	response, err := server.GetAccountsClients(context.Background(), &accountPb.GetAccountsClientsRequest{AccountUuids: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.Len(response.AccountClients, 0)
}
