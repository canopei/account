package main

import (
	"time"

	accountPb "bitbucket.org/canopei/account/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// GetAccountPermissions retrieves the permissions for the given Account
func (s *Server) GetAccountPermissions(ctx context.Context, req *accountPb.GetAccountPermissionsRequest) (*accountPb.PermissionsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetAccountPermissions: %v", req)
	defer timeTrack(logger, time.Now(), "GetAccountPermissions")

	// Validations
	if req.SiteId < 0 {
		logger.Error("The site ID is invalid.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is invalid.")
	}
	if req.Uuid == "" {
		logger.Error("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}

	// fetch the permissions
	rows, err := s.DB.Queryx(`SELECT p.*, r.site_id
		FROM account_role ar
		INNER JOIN account a ON ar.account_id = a.account_id
		INNER JOIN role r ON ar.role_id = r.role_id
		INNER JOIN role_permission rp ON rp.role_id = r.role_id
		INNER JOIN permission p ON p.permission_id = rp.permission_id
		WHERE a.uuid = UNHEX(REPLACE(?,'-',''))
			AND (r.site_id = ? OR ? = 0)
			AND a.deleted_at = '0'
			AND r.deleted_at = '0'
			AND p.deleted_at = '0'`, req.Uuid, req.SiteId, req.SiteId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the permissions")
	}

	permissionsList := &accountPb.PermissionsList{}
	for rows.Next() {
		permission := accountPb.Permission{}
		err := rows.StructScan(&permission)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the permissions")
		}
		permissionsList.Permissions = append(permissionsList.Permissions, &permission)
	}

	return permissionsList, nil
}

// HasAccountPermission checks if the given Account has the given Permission
func (s *Server) HasAccountPermission(ctx context.Context, req *accountPb.HasAccountPermissionRequest) (*accountPb.HasAccountPermissionResponse, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("HasAccountPermission: %v", req)
	defer timeTrack(logger, time.Now(), "HasAccountPermission")

	// Validations
	if req.SiteId == 0 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	if req.Uuid == "" {
		logger.Error("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}
	if req.PermissionCode == "" {
		logger.Error("The permission code is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The permission code is required.")
	}

	// check the permission
	rows, err := s.DB.Queryx(`SELECT p.permission_id
		FROM account_role ar
		INNER JOIN account a ON ar.account_id = a.account_id
		INNER JOIN role r ON ar.role_id = r.role_id
		INNER JOIN role_permission rp ON rp.role_id = r.role_id
		INNER JOIN permission p ON p.permission_id = rp.permission_id
		WHERE a.uuid = UNHEX(REPLACE(?,'-',''))
			AND r.site_id = ?
			AND p.permission_code = ?
			AND a.deleted_at = '0'
			AND r.deleted_at = '0',
			AND p.deleted_at = '0'`, req.Uuid, req.SiteId, req.PermissionCode)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the permissions")
	}

	return &accountPb.HasAccountPermissionResponse{HasPermission: rows.Next()}, nil
}
