package main

import (
	"testing"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	permissionColumns = []string{"permission_id", "code", "name", "is_global", "created_at", "modified_at", "deleted_at"}
)

func TestGetAccountPermissionsValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetAccountPermissionsRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetAccountPermissionsRequest{SiteId: 1}, codes.InvalidArgument},
		{&accountPb.GetAccountPermissionsRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetAccountPermissionsRequest{Uuid: "foo"}, codes.Unknown},
		{&accountPb.GetAccountPermissionsRequest{SiteId: 0, Uuid: "foo"}, codes.Unknown},
		{&accountPb.GetAccountPermissionsRequest{SiteId: 1, Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		permissionsList, err := server.GetAccountPermissions(context.Background(), validationTest.Request)
		// we will always get nil permissionsList
		assert.Nil(permissionsList, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetAccountPermissionsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	permissions := []*accountPb.Permission{
		&accountPb.Permission{
			Id: 1, Code: "foo", Name: "foo", Global: false, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
		&accountPb.Permission{
			Id: 2, Code: "foo", Name: "foo2", Global: true, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
		&accountPb.Permission{
			Id: 4, Code: "bar", Name: "bar", Global: false, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
	}

	rows := sqlmock.NewRows(permissionColumns)
	for _, permission := range permissions {
		rows.AddRow(permission.Id, permission.Code, permission.Name, permission.Global, permission.CreatedAt,
			permission.ModifiedAt, permission.DeletedAt)
	}
	mock.ExpectQuery("^SELECT (.+) FROM account_role (.+)").WillReturnRows(rows)

	permissionsList, err := server.GetAccountPermissions(context.Background(), &accountPb.GetAccountPermissionsRequest{SiteId: 1, Uuid: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(permissionsList)
	assert.Len(permissionsList.Permissions, len(permissions))
	for _, permission := range permissions {
		assert.Contains(permissionsList.Permissions, permission)
	}
}

func TestGetAccountPermissionsWithEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(permissionColumns)
	mock.ExpectQuery("^SELECT (.+) FROM account_role (.+)").WillReturnRows(rows)

	permissionsList, err := server.GetAccountPermissions(context.Background(), &accountPb.GetAccountPermissionsRequest{SiteId: 1, Uuid: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(permissionsList)
	assert.Len(permissionsList.Permissions, 0)
}

func TestHasAccountPermissionValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.HasAccountPermissionRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.HasAccountPermissionRequest{SiteId: 1, PermissionCode: "foo"}, codes.InvalidArgument},
		{&accountPb.HasAccountPermissionRequest{Uuid: "foo", PermissionCode: "foo"}, codes.InvalidArgument},
		{&accountPb.HasAccountPermissionRequest{SiteId: 1, Uuid: "foo"}, codes.InvalidArgument},
		{&accountPb.HasAccountPermissionRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.HasAccountPermissionRequest{SiteId: 1, Uuid: "foo", PermissionCode: "bar"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.HasAccountPermission(context.Background(), validationTest.Request)
		// we will always get nil permissionsList
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestHasAccountPermissionSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	columns := []string{"permissions_id"}

	// Multiple results
	rows := sqlmock.NewRows(columns).AddRow(1).AddRow(2)
	mock.ExpectQuery("^SELECT (.+) FROM account_role (.+) WHERE").WithArgs("foo", 1, "bar").WillReturnRows(rows)

	response, err := server.HasAccountPermission(context.Background(), &accountPb.HasAccountPermissionRequest{SiteId: 1, Uuid: "foo", PermissionCode: "bar"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.True(response.HasPermission)

	// Single result
	rows = sqlmock.NewRows(columns).AddRow(1)
	mock.ExpectQuery("^SELECT (.+) FROM account_role (.+) WHERE").WithArgs("foo", 1, "bar").WillReturnRows(rows)

	response, err = server.HasAccountPermission(context.Background(), &accountPb.HasAccountPermissionRequest{SiteId: 1, Uuid: "foo", PermissionCode: "bar"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.True(response.HasPermission)

	// No result
	rows = sqlmock.NewRows(columns)
	mock.ExpectQuery("^SELECT (.+) FROM account_role (.+) WHERE").WithArgs("foo", 1, "bar").WillReturnRows(rows)

	response, err = server.HasAccountPermission(context.Background(), &accountPb.HasAccountPermissionRequest{SiteId: 1, Uuid: "foo", PermissionCode: "bar"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.False(response.HasPermission)
}
