package main

import (
	"time"

	"github.com/Sirupsen/logrus"
)

const sqlDatetimeFormat = "2006-01-02 15:04:05"

func timeTrack(logger *logrus.Entry, start time.Time, name string) {
	elapsed := time.Since(start)
	logger.Debugf("%s took %s", name, elapsed)
}
