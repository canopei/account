package main

import (
	"context"
	"fmt"
	"testing"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	accountColumns = []string{"account_id", "uuid", "email", "password_hash", "password_salt", "gender", "full_name",
		"profile_image_url", "confirmed_at", "confirmation_code", "created_at", "modified_at", "deleted_at"}
	externalAuthenticationProviderColumns = []string{"external_authentication_provider_id", "name"}
)

func TestCreateAccountValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.CreateAccountRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.CreateAccountRequest{}, codes.InvalidArgument},
		{&accountPb.CreateAccountRequest{FullName: "foo", Email: "foo@bar.com"}, codes.InvalidArgument},
		{&accountPb.CreateAccountRequest{FullName: "foo", Password: "bar"}, codes.InvalidArgument},
		{&accountPb.CreateAccountRequest{FullName: "foo", Email: "foo", Password: "bar"}, codes.InvalidArgument},
		{&accountPb.CreateAccountRequest{FullName: "foo", Email: "foo@.com", Password: "bar"}, codes.InvalidArgument},
		{&accountPb.CreateAccountRequest{Email: "foo@bar.com", Password: "foo"}, codes.InvalidArgument},
		{&accountPb.CreateAccountRequest{FullName: "foo", Email: "foo@bar.com", Password: "12345"}, codes.InvalidArgument},
		{&accountPb.CreateAccountRequest{FullName: "12", Email: "foo@bar.com", Password: "123456"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.CreateAccountRequest{FullName: "foo", Email: "foo@bar.com", Password: "123456"}, codes.Unknown},
		{&accountPb.CreateAccountRequest{FullName: "foo", Email: "foo@bar", Password: "123456"}, codes.Unknown},
		{&accountPb.CreateAccountRequest{FullName: "foo", Email: "foo+test@bar.com", Password: "123456"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.CreateAccount(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateAccountSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WithArgs("foo@bar.com").WillReturnRows(sqlmock.NewRows([]string{"account_id"}))
	rows := sqlmock.NewRows(accountColumns).AddRow(1, "uuid", "foo@bar.com", "hash", "salt", "gender", "name", "http://www.example.com/image.jpg",
		"0000-00-00", "confcode", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectExec("^INSERT INTO account").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WithArgs(sqlmock.AnyArg()).WillReturnRows(rows)

	account, err := server.CreateAccount(context.Background(), &accountPb.CreateAccountRequest{
		FullName: "foo",
		Email:    "foo@bar.com",
		Password: "foobar",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(account)
	if account != nil {
		assert.Equal("foo@bar.com", account.Email)
		// We should get the generated UUID
		assert.NotEmpty(account.Uuid)
		assert.NotEqual("uuid", account.Uuid)
	}
}

func TestCreateAccountExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WithArgs("foo@bar.com").WillReturnRows(
		sqlmock.NewRows([]string{"account_id"}).AddRow(1),
	)

	account, err := server.CreateAccount(context.Background(), &accountPb.CreateAccountRequest{
		FullName: "foo",
		Email:    "foo@bar.com",
		Password: "foobar",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
	assert.Nil(account)
}

func TestGetAccountValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetAccountRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetAccountRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetAccountRequest{Id: 1}, codes.Unknown},
		{&accountPb.GetAccountRequest{Search: "foo@bar.com"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.GetAccount(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetAccountSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// The account SELECT query
	rows := sqlmock.NewRows(accountColumns).AddRow(123, "123456789012345", "foo@bar.com", "hash", "salt", "gender", "name", "http://www.example.com/image.jpg",
		"0000-00-00", "confcode", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WithArgs("uuid", "uuid", 1).WillReturnRows(rows)

	// The roles SELECT query
	roles := []*accountPb.Role{
		&accountPb.Role{
			Id: 1, Code: "foo", Name: "foo", SiteId: 1, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
		&accountPb.Role{
			Id: 2, Code: "foo", Name: "foo2", SiteId: 1, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
	}
	rolesRows := sqlmock.NewRows(roleColumns)
	for _, role := range roles {
		rolesRows.AddRow(role.Id, role.Code, role.Name, role.SiteId, role.CreatedAt, role.ModifiedAt, role.DeletedAt)
	}
	mock.ExpectQuery("^SELECT (.+) FROM account_role (.+) WHERE").WithArgs(123).WillReturnRows(rolesRows)

	// Tests all the arguments in one go
	account, err := server.GetAccount(context.Background(), &accountPb.GetAccountRequest{Search: "uuid", Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(account)
	if account != nil {
		assert.Equal(account.Email, "foo@bar.com")
		assert.Len(account.Roles, len(roles))
	}
}

func TestGetAccountSuccessfulNoRoles(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// The account SELECT query
	rows := sqlmock.NewRows(accountColumns).AddRow(123, "123456789012345", "foo@bar.com", "hash", "salt", "gender", "name",
		"http://www.example.com/image.jpg", "0000-00-00", "confcode", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WithArgs("uuid", "uuid", 1).WillReturnRows(rows)

	// The roles SELECT query
	rolesRows := sqlmock.NewRows(roleColumns)
	mock.ExpectQuery("^SELECT (.+) FROM account_role (.+) WHERE").WithArgs(123).WillReturnRows(rolesRows)

	// Tests all the arguments in one go
	account, err := server.GetAccount(context.Background(), &accountPb.GetAccountRequest{Search: "uuid", Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(account)
	if account != nil {
		assert.Equal(account.Email, "foo@bar.com")
		assert.Len(account.Roles, 0)
	}
}

func TestGetAccountMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// No rows returned
	rows := sqlmock.NewRows(accountColumns)
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WithArgs("uuid", "uuid", 1).WillReturnRows(rows)

	account, err := server.GetAccount(context.Background(), &accountPb.GetAccountRequest{Search: "uuid", Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestDeleteAccountValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.DeleteAccountRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.DeleteAccountRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.DeleteAccountRequest{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.DeleteAccount(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteAccount(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE account SET").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteAccount(context.Background(), &accountPb.DeleteAccountRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE account SET").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 1))

	_, err = server.DeleteAccount(context.Background(), &accountPb.DeleteAccountRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestUpdateAccountValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.Account
		ExpectedCode codes.Code
	}{
		{&accountPb.Account{}, codes.InvalidArgument},
		{&accountPb.Account{Uuid: "foo"}, codes.InvalidArgument},
		{&accountPb.Account{FullName: "bar"}, codes.InvalidArgument},
		{&accountPb.Account{Uuid: "foo", FullName: " "}, codes.InvalidArgument},
		{&accountPb.Account{Uuid: "foo", FullName: "12"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.Account{Uuid: "foo", FullName: "bar"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.UpdateAccount(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateAccountMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE account SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WithArgs("foo").WillReturnRows(sqlmock.NewRows(accountColumns))

	account, err := server.UpdateAccount(context.Background(), &accountPb.Account{Uuid: "foo", FullName: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateAccountSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testAccount := &accountPb.Account{
		Uuid:     "123456789012345",
		Gender:   "m",
		FullName: "name",
	}

	mock.ExpectExec("^UPDATE account SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(accountColumns).
		AddRow(1, testAccount.Uuid, "foo@bar.com", "hash", "salt", "m", "name", "http://www.example.com/image.jpg", "2017-01-01", "ccode",
			"2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WithArgs(testAccount.Uuid).WillReturnRows(rows)

	account, err := server.UpdateAccount(context.Background(), testAccount)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(account)
}

func TestChangeAccountPasswordValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ChangeAccountPasswordRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.ChangeAccountPasswordRequest{}, codes.InvalidArgument},
		{&accountPb.ChangeAccountPasswordRequest{NewPassword: "bar"}, codes.InvalidArgument},
		{&accountPb.ChangeAccountPasswordRequest{AccountUuid: "foo"}, codes.InvalidArgument},
		{&accountPb.ChangeAccountPasswordRequest{AccountUuid: "foo", NewPassword: " "}, codes.InvalidArgument},
		{&accountPb.ChangeAccountPasswordRequest{AccountUuid: "foo", CurrentPassword: "foo"}, codes.InvalidArgument},
		{&accountPb.ChangeAccountPasswordRequest{AccountUuid: "foo", NewPassword: "foo", CurrentPassword: "    "}, codes.InvalidArgument},
		{&accountPb.ChangeAccountPasswordRequest{AccountUuid: "foo", NewPassword: "12345", CurrentPassword: "foo"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ChangeAccountPasswordRequest{AccountUuid: "foo", NewPassword: "foobar"}, codes.Unknown},
		{&accountPb.ChangeAccountPasswordRequest{AccountUuid: "foo", NewPassword: "foobar", CurrentPassword: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.ChangeAccountPassword(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestChangeAccountPasswordMissingAccount(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	mock.ExpectQuery("^SELECT (.+) FROM account").WillReturnRows(sqlmock.NewRows(accountColumns))

	server := NewServerMock(sqlxDB)

	result, err := server.ChangeAccountPassword(context.Background(), &accountPb.ChangeAccountPasswordRequest{
		AccountUuid: "uuid",
		NewPassword: "password",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(result)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestChangeAccountPasswordWithCurrentMismatch(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	rows := sqlmock.NewRows(accountColumns).AddRow(123, "123456789012345", "foo@bar.com", "hash", "salt", "gender", "name",
		"http://www.example.com/image.jpg", "0000-00-00", "confcode", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account").WillReturnRows(rows)

	server := NewServerMock(sqlxDB)

	result, err := server.ChangeAccountPassword(context.Background(), &accountPb.ChangeAccountPasswordRequest{
		AccountUuid:     "uuid",
		NewPassword:     "password",
		CurrentPassword: "foo",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(result)
	assert.NotNil(err)
	assert.Equal(codes.InvalidArgument, grpc.Code(err))
}

func TestChangeAccountPasswordSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	rows := sqlmock.NewRows(accountColumns).AddRow(123, "123456789012345", "foo@bar.com", "hash", "salt", "gender", "name",
		"http://www.example.com/image.jpg", "0000-00-00", "confcode", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WillReturnRows(rows)
	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE account").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("^UPDATE password_change_request").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	server := NewServerMock(sqlxDB)

	result, err := server.ChangeAccountPassword(context.Background(), &accountPb.ChangeAccountPasswordRequest{
		AccountUuid: "uuid",
		NewPassword: "password",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(result)
}

func TestChangeAccountPasswordShouldRollbackOnError(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	rows := sqlmock.NewRows(accountColumns).AddRow(123, "123456789012345", "foo@bar.com", "hash", "salt", "gender", "name",
		"http://www.example.com/image.jpg", "0000-00-00", "confcode", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WillReturnRows(rows)
	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE account").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("^UPDATE password_change_request").WillReturnError(fmt.Errorf("error"))
	mock.ExpectRollback()

	server := NewServerMock(sqlxDB)

	result, err := server.ChangeAccountPassword(context.Background(), &accountPb.ChangeAccountPasswordRequest{
		AccountUuid: "uuid",
		NewPassword: "password",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(result)
	assert.Equal(codes.Unknown, grpc.Code(err))
}

func TestConfirmAccountValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.ConfirmAccountRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.ConfirmAccountRequest{}, codes.InvalidArgument},
		{&accountPb.ConfirmAccountRequest{AccountUuid: "foo"}, codes.InvalidArgument},
		{&accountPb.ConfirmAccountRequest{ConfirmationCode: "code"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.ConfirmAccountRequest{AccountUuid: "foo", ConfirmationCode: "code"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.ConfirmAccount(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestConfirmAccountMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	mock.ExpectQuery("^SELECT (.+) FROM account").WillReturnRows(sqlmock.NewRows(accountColumns))

	server := NewServerMock(sqlxDB)

	account, err := server.ConfirmAccount(context.Background(), &accountPb.ConfirmAccountRequest{
		AccountUuid:      "uuid",
		ConfirmationCode: "code",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestConfirmAccountSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	rows := sqlmock.NewRows(accountColumns).
		AddRow(1, "123456789012345", "foo@bar.com", "hash", "salt", "m", "name", "http://www.example.com/image.jpg", "0000-00-00 00:00:00", "ccode",
			"2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WillReturnRows(rows)
	mock.ExpectExec("^UPDATE account SET (.*)confirmed_at = CURRENT_TIMESTAMP").WillReturnResult(sqlmock.NewResult(0, 1))

	server := NewServerMock(sqlxDB)

	account, err := server.ConfirmAccount(context.Background(), &accountPb.ConfirmAccountRequest{
		AccountUuid:      "123456789012345",
		ConfirmationCode: "code",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(account)
	// tiny sanity check
	if account != nil {
		assert.Equal("foo@bar.com", account.Email)
		assert.NotEmpty(account.ConfirmedAt)
	}
}

func TestConfirmAccountAlreadyConfirmed(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	rows := sqlmock.NewRows(accountColumns).
		AddRow(1, "123456789012345", "foo@bar.com", "hash", "salt", "m", "name", "http://www.example.com/image.jpg", "2017-05-12 00:00:00", "ccode",
			"2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account WHERE").WillReturnRows(rows)

	server := NewServerMock(sqlxDB)

	account, err := server.ConfirmAccount(context.Background(), &accountPb.ConfirmAccountRequest{
		AccountUuid:      "123456789012345",
		ConfirmationCode: "code",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetAccountByExternalUserIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.GetAccountByExternalUserIDRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.GetAccountByExternalUserIDRequest{}, codes.InvalidArgument},
		{&accountPb.GetAccountByExternalUserIDRequest{ExternalProviderName: "facebook"}, codes.InvalidArgument},
		{&accountPb.GetAccountByExternalUserIDRequest{ExternalUserId: "12345"}, codes.InvalidArgument},
		{&accountPb.GetAccountByExternalUserIDRequest{ExternalProviderName: " ", ExternalUserId: "12345"}, codes.InvalidArgument},
		{&accountPb.GetAccountByExternalUserIDRequest{ExternalProviderName: "facebook", ExternalUserId: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.GetAccountByExternalUserIDRequest{ExternalProviderName: "facebook", ExternalUserId: "12345"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.GetAccountByExternalUserID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetAccountByExternalUserIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// The account SELECT query
	rows := sqlmock.NewRows(accountColumns).AddRow(123, "123456789012345", "foo@bar.com", "hash", "salt", "gender", "name",
		"http://www.example.com/image.jpg", "0000-00-00", "confcode", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account_external_login").WillReturnRows(rows)

	// The roles SELECT query
	roles := []*accountPb.Role{
		&accountPb.Role{
			Id: 1, Code: "foo", Name: "foo", SiteId: 1, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
		&accountPb.Role{
			Id: 2, Code: "foo", Name: "foo2", SiteId: 1, CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02",
			DeletedAt: "2017-01-03",
		},
	}
	rolesRows := sqlmock.NewRows(roleColumns)
	for _, role := range roles {
		rolesRows.AddRow(role.Id, role.Code, role.Name, role.SiteId, role.CreatedAt, role.ModifiedAt, role.DeletedAt)
	}
	mock.ExpectQuery("^SELECT (.+) FROM account_role").WillReturnRows(rolesRows)

	// Tests all the arguments in one go
	account, err := server.GetAccountByExternalUserID(
		context.Background(),
		&accountPb.GetAccountByExternalUserIDRequest{ExternalProviderName: "facebook", ExternalUserId: "12345"},
	)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(account)
	if account != nil {
		assert.Equal(account.Email, "foo@bar.com")
		assert.Len(account.Roles, len(roles))
	}
}

func TestGetAccountByExternalUserIDSuccessfulNoRoles(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// The account SELECT query
	rows := sqlmock.NewRows(accountColumns).AddRow(123, "123456789012345", "foo@bar.com", "hash", "salt", "gender", "name",
		"http://www.example.com/image.jpg", "0000-00-00", "confcode", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM account_external_login").WillReturnRows(rows)

	// The roles SELECT query
	rolesRows := sqlmock.NewRows(roleColumns)
	mock.ExpectQuery("^SELECT (.+) FROM account_role").WillReturnRows(rolesRows)

	// Tests all the arguments in one go
	account, err := server.GetAccountByExternalUserID(
		context.Background(),
		&accountPb.GetAccountByExternalUserIDRequest{ExternalProviderName: "facebook", ExternalUserId: "12345"},
	)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(account)
	if account != nil {
		assert.Equal(account.Email, "foo@bar.com")
		assert.Len(account.Roles, 0)
	}
}

func TestGetAccountByExternalUserIDMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// No rows returned
	rows := sqlmock.NewRows(accountColumns)
	mock.ExpectQuery("^SELECT (.+) FROM account_external_login").WillReturnRows(rows)

	account, err := server.GetAccountByExternalUserID(
		context.Background(),
		&accountPb.GetAccountByExternalUserIDRequest{ExternalProviderName: "facebook", ExternalUserId: "12345"},
	)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetExternalAuthProviders(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(externalAuthenticationProviderColumns)
	rows.AddRow(1, "facebook").AddRow(2, "google")
	mock.ExpectQuery("^SELECT (.+) FROM external_authentication_provider").WillReturnRows(rows)

	providers, err := server.GetExternalAuthProviders()

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(providers)
	assert.Len(providers, 2)
}

func TestCreateAccountWithExternalLoginValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *accountPb.CreateAccountWithExternalLoginRequest
		ExpectedCode codes.Code
	}{
		{&accountPb.CreateAccountWithExternalLoginRequest{}, codes.InvalidArgument},
		{&accountPb.CreateAccountWithExternalLoginRequest{ExternalUserId: "12345", Email: "foo@bar.com"}, codes.InvalidArgument},
		{&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: "facebook", Email: "foo@bar.com"}, codes.InvalidArgument},
		{&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: " ", ExternalUserId: "12345", Email: "foo@bar.com"}, codes.InvalidArgument},
		{&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: "facebook", ExternalUserId: " ", Email: "foo@bar.com"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: "facebook", ExternalUserId: "12345"}, codes.Unknown},
		{&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: "facebook", ExternalUserId: "12345", Email: "foo@bar.com", LastName: "lname"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		account, err := server.CreateAccountWithExternalLogin(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(account, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateAccountWithExternalLoginMissingProviderName(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(externalAuthenticationProviderColumns)
	rows.AddRow(1, "facebook").AddRow(2, "google")
	mock.ExpectQuery("^SELECT (.+) FROM external_authentication_provider").WillReturnRows(rows)

	account, err := server.CreateAccountWithExternalLogin(
		context.Background(),
		&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: "foo", ExternalUserId: "12345"},
	)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.InvalidArgument, grpc.Code(err))
}

func TestCreateAccountWithExternalLoginExistingEmail(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(externalAuthenticationProviderColumns)
	rows.AddRow(1, "facebook").AddRow(2, "google")
	mock.ExpectQuery("^SELECT (.+) FROM external_authentication_provider").WillReturnRows(rows)

	accountRow := sqlmock.NewRows([]string{"account_id"}).AddRow("1")
	mock.ExpectQuery("^SELECT (.+) FROM account").WithArgs("foo@bar.com").WillReturnRows(accountRow)

	account, err := server.CreateAccountWithExternalLogin(
		context.Background(),
		&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: "facebook", ExternalUserId: "12345", Email: "foo@bar.com"},
	)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestCreateAccountWithExternalLoginExistingExternalUserID(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(externalAuthenticationProviderColumns)
	rows.AddRow(1, "facebook").AddRow(2, "google")
	mock.ExpectQuery("^SELECT (.+) FROM external_authentication_provider").WillReturnRows(rows)

	// No rows
	mock.ExpectQuery("^SELECT (.+) FROM account").WithArgs("foo@bar.com").WillReturnRows(sqlmock.NewRows([]string{"account_id"}))

	externalUIDows := sqlmock.NewRows([]string{"account_id"}).AddRow(1)
	mock.ExpectQuery("^SELECT (.+) FROM account_external_login").WillReturnRows(externalUIDows)

	account, err := server.CreateAccountWithExternalLogin(
		context.Background(),
		&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: "facebook", ExternalUserId: "12345", Email: "foo@bar.com"},
	)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestCreateAccountWithExternalLoginErrorRollback(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(externalAuthenticationProviderColumns)
	rows.AddRow(1, "facebook").AddRow(2, "google")
	mock.ExpectQuery("^SELECT (.+) FROM external_authentication_provider").WillReturnRows(rows)

	// No rows
	mock.ExpectQuery("^SELECT (.+) FROM account").WithArgs("foo@bar.com").WillReturnRows(sqlmock.NewRows([]string{"account_id"}))

	// No rows
	mock.ExpectQuery("^SELECT (.+) FROM account_external_login").WillReturnRows(sqlmock.NewRows([]string{"account_id"}))

	mock.ExpectBegin()
	mock.ExpectExec("^INSERT INTO account").WillReturnResult(sqlmock.NewErrorResult(fmt.Errorf("error")))
	mock.ExpectRollback()

	account, err := server.CreateAccountWithExternalLogin(
		context.Background(),
		&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: "facebook", ExternalUserId: "12345", Email: "foo@bar.com", FirstName: "fname"},
	)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(account)
	assert.NotNil(err)
	assert.Equal(codes.Unknown, grpc.Code(err))
}

func TestCreateAccountWithExternalLoginSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(externalAuthenticationProviderColumns)
	rows.AddRow(1, "facebook").AddRow(2, "google")
	mock.ExpectQuery("^SELECT (.+) FROM external_authentication_provider").WillReturnRows(rows)

	// No rows
	mock.ExpectQuery("^SELECT (.+) FROM account").WithArgs("foo@bar.com").WillReturnRows(sqlmock.NewRows([]string{"account_id"}))

	// No rows
	mock.ExpectQuery("^SELECT (.+) FROM account_external_login").WillReturnRows(sqlmock.NewRows([]string{"account_id"}))

	mock.ExpectBegin()
	mock.ExpectExec("^INSERT INTO account").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO account_external_login").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	newAccountRows := sqlmock.NewRows(accountColumns).AddRow(1, "uuid", "foo@bar.com", "hash", "salt", "gender", "fname lname",
		"http://www.example.com/image.jpg", "0000-00-00", "confcode", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.*) FROM account").WillReturnRows(newAccountRows)

	account, err := server.CreateAccountWithExternalLogin(
		context.Background(),
		&accountPb.CreateAccountWithExternalLoginRequest{ExternalProviderName: "facebook", ExternalUserId: "12345", Email: "foo@bar.com", FirstName: "fname", LastName: "lname"},
	)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(account)
	if account != nil {
		assert.Equal("foo@bar.com", account.Email)
		assert.Equal("fname lname", account.FullName)
	}
}
