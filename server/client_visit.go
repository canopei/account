package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/empty"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"golang.org/x/net/context"

	pb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
)

const (
	// MaxClientVisitsFetched is the limit of number of client visits that can be fetched
	// at once.
	MaxClientVisitsFetched int32 = 50
)

// CreateClientVisit creates a ClientVisit
func (s *Server) CreateClientVisit(ctx context.Context, req *pb.ClientVisit) (*pb.ClientVisit, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateClientVisit: %v", req)
	defer timeTrack(logger, time.Now(), "CreateClientVisit")

	// Validations
	if req.ClientId < 1 {
		logger.Error("The client ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client ID is required.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	if req.LocationId < 1 {
		logger.Error("The location ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location ID is required.")
	}
	if req.ClassId < 1 {
		logger.Error("The class ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class ID is required.")
	}
	if req.Name == "" {
		logger.Error("The name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The name is required.")
	}

	if req.MbId > 0 {
		// Check if a ClientVisit with this MB ID already exists
		err := s.DB.Get(req, "SELECT * FROM client_visit WHERE mb_id = ?", req.MbId)
		if err != sql.ErrNoRows {
			if err == nil {
				logger.Errorf("A visit already exists with this MB ID (%d).", req.MbId)
				return nil, grpc.Errorf(codes.Aborted, "A visit already exists with this MB ID (%d).", req.MbId)
			}
			return nil, grpcUtils.InternalError(logger, err, "Error while checking the client visit.")
		}
	}

	// Create the client visit
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	_, err = s.DB.NamedExec(`INSERT INTO client_visit
			(uuid, mb_id, client_id, site_id, name, staff_id, location_id, class_id, service_mb_id, service_count,
			service_remaining, late_cancelled, make_up, web_signup, signed_in, start_datetime, end_datetime)
		VALUES
			(unhex(replace(:uuid,'-','')), :mb_id, :client_id, :site_id, :name, :staff_id, :location_id, :class_id, :service_mb_id,
			:service_count, :service_remaining, :late_cancelled, :make_up, :web_signup, :signed_in, :start_datetime, :end_datetime)`, req)
	if err != nil {
		logger.Errorf("Cannot create database entry: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Created client visit '%s'.", req.Uuid)

	err = s.DB.Get(req, "SELECT * FROM client_visit WHERE uuid = unhex(replace(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the new client visit.")
	}
	req.Uuid = uuid.String()

	return req, nil
}

// GetClientVisit retrieves a ClientVisit by UUID or ID
func (s *Server) GetClientVisit(ctx context.Context, req *pb.ClientVisitRequest) (*pb.ClientVisit, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClientVisit: %v", req)
	defer timeTrack(logger, time.Now(), "GetClientVisit")

	if req.Uuid == "" {
		logger.Error("The visit UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The visit UUID is required.")
	}

	ID := int32(0)
	if !strings.Contains(req.Uuid, "-") {
		rid, err := strconv.ParseInt(req.Uuid, 10, 32)
		if err != nil {
			logger.Error("The provided ID is out of range.")
			return nil, grpc.Errorf(codes.InvalidArgument, "The provided ID is out of range.")
		}
		ID = int32(rid)
	}

	var clientVisit pb.ClientVisit
	err := s.DB.Get(&clientVisit, `SELECT *
		FROM client_visit
		WHERE (uuid = UNHEX(REPLACE(?,'-','')) OR client_id = ?)
			AND deleted_at = '0'`, req.Uuid, ID)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find client visit '%s' (%d).", req.Uuid, ID)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the client visit")
	}

	unpackedUUID, err := crypto.Parse([]byte(clientVisit.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	clientVisit.Uuid = unpackedUUID.String()

	return &clientVisit, nil
}

// GetClientVisits fetches a list of client visits
func (s *Server) GetClientVisits(ctx context.Context, req *pb.GetClientVisitsRequest) (*pb.ClientVisitsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClientVisits: %v", req)
	defer timeTrack(logger, time.Now(), "GetClientVisits")

	if req.Limit == 0 || req.Limit > MaxClientVisitsFetched {
		req.Limit = MaxClientVisitsFetched
	}

	// Search by client UUID
	reqClientUuids := []string{}
	req.ClientUuids = strings.TrimSpace(req.ClientUuids)
	if req.ClientUuids != "" {
		uuids := strings.Split(req.ClientUuids, ",")
		for _, uuid := range uuids {
			uuid := strings.TrimSpace(uuid)
			if uuid != "" {
				reqClientUuids = append(reqClientUuids, uuid)
			}
		}
	}

	logger.Infof("Fetching %d client visits, offset %d, client UUIDS (if any): %s", req.Limit, req.Offset, req.ClientUuids)

	// Fetch the sites
	queryParams := map[string]interface{}{
		"limit":  req.Limit,
		"offset": req.Offset,
	}

	condition := ""
	if len(reqClientUuids) > 0 {
		condition = "AND c.uuid IN ("
		for k, v := range reqClientUuids {
			paramKey := "uuid" + strconv.Itoa(k)
			condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
	}

	rows, err := s.DB.NamedQuery(`SELECT cv.*, c.uuid "client_uuid"
		FROM client_visit cv
		INNER JOIN client c ON c.client_id = cv.client_id
		WHERE 1=1 `+condition+`
		ORDER BY cv.client_visit_id DESC
		LIMIT :limit
		OFFSET :offset`, queryParams)
	if err != nil {
		logger.Errorf("Error while fetching the client visits: %v", err)
		return nil, err
	}

	clientVisitsList := &pb.ClientVisitsList{}
	for rows.Next() {
		clientVisit := pb.ClientVisit{}
		err := rows.StructScan(&clientVisit)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the client visit")
		}

		unpackedUUID, err := crypto.Parse([]byte(clientVisit.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		clientVisit.Uuid = unpackedUUID.String()

		clientVisitsList.ClientVisits = append(clientVisitsList.ClientVisits, &clientVisit)
	}

	return clientVisitsList, nil
}

// GetClientVisitsByMBID fetches a list of client visits by their MB IDs
func (s *Server) GetClientVisitsByMBID(ctx context.Context, req *pb.GetClientsVisitsByMBIDRequest) (*pb.ClientVisitsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClientVisitsByMBID: %d IDs", len(req.MbIds))
	defer timeTrack(logger, time.Now(), "GetClientVisitsByMBID")

	// Validations
	if len(req.MbIds) == 0 {
		logger.Error("At least one MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one MB ID is required.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}

	// Fetch the clients
	queryParams := map[string]interface{}{
		"site_id": req.SiteId,
	}
	condition := " AND mb_id IN ("
	for k, v := range req.MbIds {
		paramKey := "id" + strconv.Itoa(k)
		condition += ":" + paramKey + ", "
		queryParams[paramKey] = v
	}
	condition = strings.TrimSuffix(condition, ", ") + ")"

	rows, err := s.DB.NamedQuery(`SELECT *
		FROM client_visit
		WHERE site_id = :site_id`+condition+`
		ORDER BY client_visit_id DESC`, queryParams)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		return nil, err
	}

	clientVisitsList := &pb.ClientVisitsList{}
	for rows.Next() {
		clientVisit := pb.ClientVisit{}
		err := rows.StructScan(&clientVisit)
		if err != nil {
			fmt.Println(err)
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the client visits")
		}

		unpackedUUID, err := crypto.Parse([]byte(clientVisit.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		clientVisit.Uuid = unpackedUUID.String()

		clientVisitsList.ClientVisits = append(clientVisitsList.ClientVisits, &clientVisit)
	}

	return clientVisitsList, nil
}

// GetClientVisitsByClientUUID fetches a list of client visits for a given Client (UUID)
func (s *Server) GetClientVisitsByClientUUID(ctx context.Context, req *pb.GetClientVisitsRequest) (*pb.ClientVisitsList, error) {
	return s.GetClientVisits(ctx, req)
}

// UpdateClientVisit updates a ClientVisit
func (s *Server) UpdateClientVisit(ctx context.Context, req *pb.ClientVisit) (*pb.ClientVisit, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateClientVisit: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateClientVisit")

	if req.Uuid == "" {
		logger.Error("The client visit UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client visit UUID is required.")
	}
	if req.Name == "" {
		logger.Error("The name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The name is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE client_visit SET
			name = :name, service_count = :service_count, service_remaining = :service_remaining, late_cancelled = :late_cancelled,
			make_up = :make_up, web_signup = :web_signup, signed_in = :signed_in, start_datetime = :start_datetime, end_datetime = :end_datetime,
			service_mb_id = :service_mb_id, service_count = :service_count, service_remaining = :service_remaining
		WHERE uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var clientVisit pb.ClientVisit
	err = s.DB.Get(&clientVisit, "SELECT * FROM client_visit WHERE uuid = unhex(replace(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateClientVisit - client visit not found: %s", req.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	clientVisit.Uuid = req.Uuid

	return &clientVisit, nil
}

// DeleteClientVisit removes a ClientVisit
func (s *Server) DeleteClientVisit(ctx context.Context, req *pb.ClientVisitRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClientVisit: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteClientVisit")

	if req.Uuid == "" {
		logger.Error("The client visit UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client visit UUID is required.")
	}

	result, err := s.DB.Exec("DELETE FROM client_visit WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteClientVisit - client visit not found: %s", req.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
