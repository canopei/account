package main

import (
	"net/http"

	"github.com/Sirupsen/logrus"
)

const sqlDatetimeFormat = "2006-01-02 15:04:05"

// GetRequestLogger builds an instance of a logger for the current request using an original logger
// It adds a requestid Field to the logger
func GetRequestLogger(req *http.Request, originalLogger *logrus.Entry) *logrus.Entry {
	log := logrus.New()
	log.Formatter = originalLogger.Logger.Formatter
	log.Level = originalLogger.Logger.Level
	log.Hooks = originalLogger.Logger.Hooks
	newLogger := log.WithFields(originalLogger.Data)

	requestID := req.Header.Get("X-Request-Id")
	if requestID != "" {
		newLogger.Data["requestid"] = requestID
	}

	return newLogger
}
