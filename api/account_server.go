package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/canopei/golibs/slices"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"

	"fmt"

	"bitbucket.org/canopei/account"
	"bitbucket.org/canopei/account/config"
	accountPb "bitbucket.org/canopei/account/protobuf"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/mindbody"
	mbHelpers "bitbucket.org/canopei/mindbody/helpers"
	clientMb "bitbucket.org/canopei/mindbody/services/client"
	salePb "bitbucket.org/canopei/sale/protobuf"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
)

// AccountServer is the implementation of an Account server
type AccountServer struct {
	Logger         *logrus.Entry
	SiteService    sitePb.SiteServiceClient
	ClientService  accountPb.ClientServiceClient
	AccountService accountPb.AccountServiceClient
	ClassService   classPb.ClassServiceClient
	StaffService   staffPb.StaffServiceClient
	SaleService    salePb.SaleServiceClient
	ClientMBClient *clientMb.Client_x0020_ServiceSoap
	MindbodyConfig *config.MindbodyConfig
}

// ClientWithSite holds a Client and its Site
type ClientWithSite struct {
	Client *accountPb.Client `json:"client"`
	Site   *sitePb.Site      `json:"site"`
}

// SearchClientsByEmailResponse holds a response
type SearchClientsByEmailResponse struct {
	Clients []*ClientWithSite `json:"clients"`
}

// GetAccountClientsResponse holds a response
type GetAccountClientsResponse struct {
	Clients []*ClientWithSite `json:"clients"`
}

// InstagramItem holds an Instagram media item
type InstagramItem struct {
	ID          string `json:"id"`
	Code        string `json:"code"`
	Image       string `json:"image"`
	CreatedTime int64  `json:"createdTime"`
	Caption     string `json:"caption"`
	LikesCount  int    `json:"likesCount"`
	Link        string `json:"link"`
}

// NewAccountServer creates an instance of an ClassServer
func NewAccountServer(
	logger *logrus.Entry,
	siteService sitePb.SiteServiceClient,
	clientService accountPb.ClientServiceClient,
	accountService accountPb.AccountServiceClient,
	classService classPb.ClassServiceClient,
	staffService staffPb.StaffServiceClient,
	saleService salePb.SaleServiceClient,
	clientMBClient *clientMb.Client_x0020_ServiceSoap,
	mindbodyConfig *config.MindbodyConfig,
) *AccountServer {
	return &AccountServer{
		Logger:         logger,
		SiteService:    siteService,
		ClassService:   classService,
		StaffService:   staffService,
		ClientService:  clientService,
		AccountService: accountService,
		SaleService:    saleService,
		ClientMBClient: clientMBClient,
		MindbodyConfig: mindbodyConfig,
	}
}

// HandleSearchClientsByEmail handles a HTTP request for listing clients by e-mail
func (s *AccountServer) HandleSearchClientsByEmail(res http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	reqEmail := vars["email"]

	var err error
	var siteID int
	reqSiteID := req.URL.Query().Get("siteId")
	if reqSiteID == "" {
		siteID = 0
	} else {
		siteID, err = strconv.Atoi(reqSiteID)
		if err != nil {
			logger.Errorf("Error while converting the limit to int.")
			res.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	logger := GetRequestLogger(req, s.Logger)

	logger.Infof("HandleSearchClientsByEmail %s, site ID %d", reqEmail, siteID)

	ctx := context.Background()

	response := &SearchClientsByEmailResponse{}
	clientsResponse, err := s.ClientService.GetClients(ctx, &accountPb.GetClientsRequest{Email: reqEmail, SiteId: int32(siteID)})
	if err != nil {
		logger.Errorf("Error while fetching the clients from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	sites, err := s.getSitesForClients(clientsResponse.Clients)
	if err != nil {
		logger.Errorf("Error while fetching the sites from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	for _, dbClient := range clientsResponse.Clients {
		site := &sitePb.Site{}
		if _, ok := sites[dbClient.SiteId]; ok {
			site = sites[dbClient.SiteId]
		}

		response.Clients = append(response.Clients, &ClientWithSite{
			Client: dbClient,
			Site:   site,
		})
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// HandleGetAccountClients handles a HTTP request for listing clients for the given account UUID
func (s *AccountServer) HandleGetAccountClients(res http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	reqAccountUUID := vars["account_uuid"]

	logger := GetRequestLogger(req, s.Logger)
	logger.Infof("HandleGetAccountClients %s", reqAccountUUID)

	response, err := s.getAccountClients(reqAccountUUID)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// HandleGetInstagramItems fetches a list of instagram media items for the given username
func (s *AccountServer) HandleGetInstagramItems(res http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	reqUsername := vars["username"]

	var err error

	var limit int
	reqLimit := req.URL.Query().Get("limit")
	if reqLimit == "" {
		limit = 10
	} else {
		limit, err = strconv.Atoi(reqLimit)
		if err != nil {
			logger.Errorf("Error while converting the limit to int.")
			res.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	logger := GetRequestLogger(req, s.Logger)
	logger.Infof("HandleGetInstagramItems %s, limit %d", reqUsername, limit)

	url := fmt.Sprintf("https://www.instagram.com/%s/media/", reqUsername)
	logger.Infof("Requesting Instagram '%s'...", url)
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		logger.Errorf("Error while fetching the media from Instagram.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	logger.Infof("Got Instagram response in %s.", time.Since(start))

	if resp.StatusCode == http.StatusNotFound {
		logger.Infof("Instagram account '%s' not found.", reqUsername)
		res.WriteHeader(http.StatusNotFound)
		return
	}

	decoder := json.NewDecoder(resp.Body)
	var reqResponse map[string]interface{}
	err = decoder.Decode(&reqResponse)
	if err != nil {
		logger.Errorf("Failed while unmarshaling the Instagram response.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	itemsEl := reqResponse["items"].([]interface{})

	items := []*InstagramItem{}
	for _, itemEl := range itemsEl {
		item := itemEl.(map[string]interface{})

		images := item["images"].(map[string]interface{})
		lowResImage := images["low_resolution"].(map[string]interface{})
		likes := item["likes"].(map[string]interface{})
		likesCount := int(likes["count"].(float64))
		caption := item["caption"].(map[string]interface{})
		createdTimeStr := item["created_time"].(string)
		createdTime, _ := strconv.ParseInt(createdTimeStr, 10, 64)

		items = append(items, &InstagramItem{
			ID:          item["id"].(string),
			Code:        item["code"].(string),
			Image:       lowResImage["url"].(string),
			CreatedTime: createdTime,
			Caption:     caption["text"].(string),
			LikesCount:  likesCount,
			Link:        item["link"].(string),
		})

		if len(items) >= limit {
			break
		}
	}

	response := map[string]interface{}{
		"items": items,
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// ClientService represents a MB service
type ClientService struct {
	MBID           int64  `json:"mbId"`
	Name           string `json:"name"`
	SiteName       string `json:"siteName"`
	PaymentDate    string `json:"paymentDate"`
	ActiveDate     string `json:"activeDate"`
	ExpirationDate string `json:"expirationDate"`
	Current        bool   `json:"current"`
	Count          int32  `json:"count"`
	Remaining      int32  `json:"remaining"`
}

// GetClientServicesResponse is a response
type GetClientServicesResponse struct {
	Services []*ClientService `json:"services"`
}

// HandleGetClientServices fetches the list of services that a client owns (optional: and can be used for the given class)
func (s *AccountServer) HandleGetClientServices(res http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	reqClientUUID := vars["client_uuid"]
	reqClassUUID, hasClassUUID := vars["class_uuid"]

	var err error

	logger := GetRequestLogger(req, s.Logger)
	logger.Infof("HandleGetClientServices %v", vars)

	ctx := context.Background()

	dbClient, err := s.ClientService.GetClient(ctx, &accountPb.GetClientRequest{Uuid: reqClientUUID})
	if err != nil {
		if codes.NotFound == grpc.Code(err) {
			logger.Infof("Client UUID '%s' not found in DB.", reqClientUUID)
			res.WriteHeader(http.StatusNotFound)
			return
		}

		logger.Errorf("Cannot fetch from DB client UUID '%s': %v", reqClientUUID, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	dbSite, err := s.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Id: dbClient.SiteId})
	if err != nil {
		if codes.NotFound == grpc.Code(err) {
			logger.Infof("Site ID %d not found in DB.", dbClient.SiteId)
			res.WriteHeader(http.StatusNotFound)
			return
		}

		logger.Errorf("Cannot fetch from DB site ID %d: %v", dbClient.SiteId, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Sync the clients page by page
	mbRequest := mbHelpers.CreateClientMBRequest([]int32{dbSite.MbId}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)

	credsUsername := "_" + s.MindbodyConfig.SourceName
	credsPassword := s.MindbodyConfig.SourcePassword
	// This is an exception ONLY for the Sandbox Site, where we cannot authorize our master API creds
	if dbSite.MbId == -99 {
		credsUsername = "Siteowner"
		credsPassword = "apitest1234"
	}

	mbClientServicesRequest := &clientMb.GetClientServicesRequest{
		MBRequest: mbRequest,
		ClientID:  strconv.FormatInt(dbClient.MbId, 10),
	}

	// Complete the request with params
	if hasClassUUID {
		dbClassesResponse, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Uuids: reqClassUUID})
		if err != nil {
			logger.Errorf("Cannot fetch from DB class '%s': %v", reqClassUUID, err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		if len(dbClassesResponse.Classes) == 0 {
			logger.Errorf("Cannot find in DB class '%s'", reqClassUUID)
			res.WriteHeader(http.StatusNotFound)
			return
		}
		dbClass := dbClassesResponse.Classes[0]

		dbProgram, err := s.ClassService.GetProgram(ctx, &classPb.ProgramRequest{Id: dbClass.Description.SessionType.Program.Id})
		if err != nil {
			logger.Errorf("Cannot find in DB program ID %d", dbClass.Description.SessionType.Program.Id)
			res.WriteHeader(http.StatusBadRequest)
			return
		}

		mbClientServicesRequest.ProgramIDs = &clientMb.ArrayOfInt{Int: []*mindbody.CustomInt32{
			&mindbody.CustomInt32{Int: dbProgram.MbId},
		}}
	}

	mbRequest.UserCredentials = &clientMb.UserCredentials{Username: credsUsername, Password: credsPassword, SiteIDs: mbRequest.SourceCredentials.SiteIDs}
	mbClientServicesResponse, err := s.ClientMBClient.GetClientServices(&clientMb.GetClientServices{Request: mbClientServicesRequest})
	if err != nil {
		s.Logger.Errorf("Cannot fetch the client services from Mindbody: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	mbClientServicesResult := mbClientServicesResponse.GetClientServicesResult
	if mbClientServicesResult.ErrorCode.Int != 200 {
		s.Logger.Errorf("Mindbody API response error: %s", mbClientServicesResult.Message)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	s.Logger.Infof("Got %d client services from MB for %s.", len(mbClientServicesResult.ClientServices.ClientService), reqClientUUID)

	response := &GetClientServicesResponse{
		Services: []*ClientService{},
	}
	for _, clientService := range mbClientServicesResult.ClientServices.ClientService {
		response.Services = append(response.Services, &ClientService{
			MBID:           clientService.ID.Int,
			Name:           clientService.Name,
			SiteName:       dbSite.Name,
			PaymentDate:    clientService.PaymentDate.Time.Format(sqlDatetimeFormat),
			ActiveDate:     clientService.ActiveDate.Time.Format(sqlDatetimeFormat),
			ExpirationDate: clientService.ExpirationDate.Time.Format(sqlDatetimeFormat),
			Current:        clientService.Current,
			Count:          clientService.Count.Int,
			Remaining:      clientService.Remaining.Int,
		})
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// CreateMBClientRequest is a request
type CreateMBClientRequest struct {
	SiteMBID int32             `json:"siteMbId"`
	Client   *accountPb.Client `json:"client"`
}

// HandleCreateMBClient creates a client in Mindbody and in SNG DB
func (s *AccountServer) HandleCreateMBClient(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)
	logger.Infof("HandleCreateMBClient 1/2")

	// Read body
	b, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		s.Logger.Errorf("Cannot fetch the request body: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	var request CreateMBClientRequest
	err = json.Unmarshal(b, &request)
	if err != nil {
		s.Logger.Errorf("Cannot parse the request body: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	logger.Infof("HandleCreateMBClient 2/2 %v", request)

	// Fetch the site
	dbSite, err := s.SiteService.GetSiteByMBID(context.Background(), &sitePb.GetSiteRequest{MbId: request.SiteMBID})
	if err != nil {
		if codes.NotFound == grpc.Code(err) {
			s.Logger.Errorf("Cannot find site MB ID %d in DB: %v", request.SiteMBID, err)
			res.WriteHeader(http.StatusNotFound)
			return
		}

		s.Logger.Errorf("Cannot fetch the site MB ID %d from DB: %v", request.SiteMBID, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Sync the clients page by page
	mbRequest := mbHelpers.CreateClientMBRequest([]int32{request.SiteMBID}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)

	credsUsername := "_" + s.MindbodyConfig.SourceName
	credsPassword := s.MindbodyConfig.SourcePassword
	// This is an exception ONLY for the Sandbox Site, where we cannot authorize our master API creds
	if request.SiteMBID == -99 {
		credsUsername = "Siteowner"
		credsPassword = "apitest1234"
	}

	gender := ""
	intToGender := map[int32]string{}
	for g, i := range account.GenderToInt {
		intToGender[i] = g
	}
	if genderName, ok := intToGender[request.Client.Gender]; ok {
		gender = genderName
	}

	lastName := strings.TrimSpace(request.Client.LastName)
	if lastName == "" {
		lastName = "-"
	}

	// Just fill in all the fields to avoid required fields errors
	mbAddOrUpdateClientsRequest := &clientMb.AddOrUpdateClientsRequest{
		MBRequest: mbRequest,
		Clients: &clientMb.ArrayOfClient{Client: []*clientMb.Client{
			&clientMb.Client{
				Email:                            request.Client.Email,
				FirstName:                        request.Client.FirstName,
				LastName:                         lastName,
				MiddleName:                       request.Client.MiddleName,
				AddressLine1:                     "-",
				City:                             request.Client.City,
				State:                            request.Client.State,
				PostalCode:                       "-",
				HomePhone:                        "12345",
				WorkPhone:                        "12345",
				MobilePhone:                      "12345",
				ReferredBy:                       "-",
				EmergencyContactInfoName:         "-",
				EmergencyContactInfoEmail:        "-",
				EmergencyContactInfoPhone:        "12345",
				EmergencyContactInfoRelationship: "-",
				BirthDate:                        &mindbody.CustomTime{Time: time.Unix(0, 0)},
				Gender:                           gender,
				EmailOptIn:                       request.Client.EmailOptin,
				IsCompany:                        request.Client.IsCompany,
			},
		}},
	}

	mbRequest.UserCredentials = &clientMb.UserCredentials{Username: credsUsername, Password: credsPassword, SiteIDs: mbRequest.SourceCredentials.SiteIDs}
	mbAddOrUpdateClientsResponse, err := s.ClientMBClient.AddOrUpdateClients(&clientMb.AddOrUpdateClients{Request: mbAddOrUpdateClientsRequest})
	if err != nil {
		s.Logger.Errorf("Cannot create the new client in Mindbody: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	mbAddOrUpdateClientsResult := mbAddOrUpdateClientsResponse.AddOrUpdateClientsResult
	if mbAddOrUpdateClientsResult.ErrorCode.Int != 200 {
		s.Logger.Errorf("Mindbody API response error: %s : %v", mbAddOrUpdateClientsResult.Message, mbAddOrUpdateClientsResult.Clients.Client[0].Messages)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	mbClient := mbAddOrUpdateClientsResult.Clients.Client[0]

	s.Logger.Infof("Client '%s' (MB ID %d) created in Mindbody for site MB ID %d.", request.Client.Email, mbClient.UniqueID.Int, request.SiteMBID)

	// Save to DB
	newClient := account.MbClientToPbClient(mbClient)
	newClient.SiteId = dbSite.Id
	dbClient, err := s.ClientService.CreateClient(context.Background(), newClient)
	if err != nil {
		s.Logger.Errorf("Cannot create the new client in DB: %v", err)

		s.Logger.Infof("Removing client MB ID %d.", mbClient.UniqueID.Int)
		mbAddOrUpdateClientsRequest.Clients.Client[0].ID = mbClient.ID
		mbAddOrUpdateClientsRequest.Clients.Client[0].Email = "deleted-" + mbClient.ID + "@sng.com"
		mbAddOrUpdateClientsRequest.Clients.Client[0].FirstName = "Deleted by SNG"
		mbAddOrUpdateClientsRequest.Clients.Client[0].LastName = "-"
		mbAddOrUpdateClientsRequest.Clients.Client[0].MiddleName = "-"
		mbAddOrUpdateClientsRequest.Clients.Client[0].Notes = fmt.Sprintf("Original e-mail: %s; Name: %s %s", newClient.Email, newClient.FirstName, newClient.LastName)
		mbAddOrUpdateClientsResponse, err := s.ClientMBClient.AddOrUpdateClients(&clientMb.AddOrUpdateClients{Request: mbAddOrUpdateClientsRequest})
		if err != nil {
			s.Logger.Errorf("Cannot inactivate the new error'ed client in Mindbody: %v", err)
		}
		mbAddOrUpdateClientsResult := mbAddOrUpdateClientsResponse.AddOrUpdateClientsResult
		if mbAddOrUpdateClientsResult.ErrorCode.Int != 200 {
			s.Logger.Errorf("Mindbody API response error while inactivating client: %s : %v", mbAddOrUpdateClientsResult.Message, mbAddOrUpdateClientsResult.Clients.Client[0].Messages)
		}

		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	s.writeJSONResponse(res, dbClient)
	res.WriteHeader(http.StatusOK)
}

// Visit represents a past class visit
type Visit struct {
	UUID               string `json:"uuid,omitempty"`
	ClassUUID          string `json:"classUuid"`
	ClassName          string `json:"className"`
	LevelName          string `json:"levelName"`
	StartDatetime      string `json:"startDatetime"`
	EndDatetime        string `json:"endDatetime"`
	LocationName       string `json:"locationName"`
	LateCancelInterval int32  `json:"lateCancelInterval"`
	StaffFirstName     string `json:"staffFirstName"`
	StaffLastName      string `json:"staffLastName"`
	HideCancel         bool   `json:"hideCancel"`
}

// GetAccountVisitsResponse is a response
type GetAccountVisitsResponse struct {
	Items []*Visit `json:"items"`
}

// HandleGetAccountVisits fetches an account visits history
func (s *AccountServer) HandleGetAccountVisits(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	var err error

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]

	var limit int32
	reqLimit := req.URL.Query().Get("limit")
	if reqLimit == "" || reqLimit == "0" {
		limit = 10
	} else {
		i, err := strconv.ParseInt(reqLimit, 10, 32)
		if err != nil {
			logger.Errorf("Error while converting the limit to int.")
			res.WriteHeader(http.StatusBadRequest)
			return
		}
		limit = int32(i)
	}

	var offset int32
	reqOffset := req.URL.Query().Get("offset")

	if reqOffset == "" || reqOffset == "0" {
		offset = 0
	} else {
		i, err := strconv.ParseInt(reqOffset, 10, 32)
		if err != nil {
			logger.Errorf("Error while converting the offset to int.")
			res.WriteHeader(http.StatusBadRequest)
			return
		}
		offset = int32(i)
	}

	logger.Infof("HandleGetAccountVisits for account '%s', limit %d, offset %d", accountUUID, limit, offset)

	ctx := context.Background()
	response := &GetAccountVisitsResponse{Items: []*Visit{}}

	// Get the account's clients
	accountClientsResponse, err := s.getAccountClients(accountUUID)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(accountClientsResponse.Clients) > 0 {
		clientUUIDs := []string{}
		for _, accountClient := range accountClientsResponse.Clients {
			clientUUIDs = append(clientUUIDs, accountClient.Client.Uuid)
		}

		logger.Infof("Got %d clients for account '%s'.", len(clientUUIDs), accountUUID)

		// get the visits history for these clients.
		clientsVisits, err := s.ClientService.GetClientVisits(ctx, &accountPb.GetClientVisitsRequest{ClientUuids: strings.Join(clientUUIDs, ","), Limit: limit, Offset: offset})
		if err != nil {
			logger.Errorf("Error while fetching the visits for clients %s: %v", strings.Join(clientUUIDs, ", "), err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		logger.Infof("Got %d visits for account '%s'.", len(clientsVisits.ClientVisits), accountUUID)
		if len(clientsVisits.ClientVisits) > 0 {
			locationsMap := map[int32]*sitePb.Location{}
			staffMap := map[int32]*staffPb.Staff{}
			classesMap := map[int32]*classPb.Class{}

			locationIDs := []int32{}
			staffIDs := []int32{}
			siteIDs := []int32{}
			classIDs := []int32{}

			for _, clientVisit := range clientsVisits.ClientVisits {
				if !slices.Icontains32(locationIDs, clientVisit.LocationId) {
					locationIDs = append(locationIDs, clientVisit.LocationId)
				}
				if !slices.Icontains32(staffIDs, clientVisit.StaffId) {
					staffIDs = append(staffIDs, clientVisit.StaffId)
				}
				if !slices.Icontains32(siteIDs, clientVisit.SiteId) {
					siteIDs = append(siteIDs, clientVisit.SiteId)
				}
				if !slices.Icontains32(classIDs, clientVisit.ClassId) {
					classIDs = append(classIDs, clientVisit.ClassId)
				}
			}

			// Fetch the classes
			classes, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Ids: classIDs})
			if err != nil {
				logger.Errorf("Error while fetching the classes: %v", err)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}
			for _, dbClass := range classes.Classes {
				classesMap[dbClass.Id] = dbClass
			}

			// Fetch the staff
			staff, err := s.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{Ids: staffIDs})
			if err != nil {
				logger.Errorf("Error while fetching the staff members: %v", err)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}
			for _, dbStaff := range staff.Staff {
				staffMap[dbStaff.Id] = dbStaff
			}

			// Fetch the locations
			locations, err := s.SiteService.GetLocations(ctx, &sitePb.GetLocationsRequest{Ids: locationIDs})
			if err != nil {
				logger.Errorf("Error while fetching the locations: %v", err)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}
			for _, dbLocation := range locations.Locations {
				locationsMap[dbLocation.Id] = dbLocation
			}

			for _, clientVisit := range clientsVisits.ClientVisits {
				location, ok := locationsMap[clientVisit.LocationId]
				if !ok {
					logger.Errorf("Location ID %d not found for client visit ID %d: %v", clientVisit.LocationId, clientVisit.Id, err)
					continue
				}

				staff, ok := staffMap[clientVisit.StaffId]
				if !ok {
					logger.Errorf("Staff ID %d not found for client visit ID %d: %v", clientVisit.StaffId, clientVisit.Id, err)
					continue
				}

				class, ok := classesMap[clientVisit.ClassId]
				if !ok {
					logger.Errorf("Class ID %d not found for client visit ID %d: %v", clientVisit.ClassId, clientVisit.Id, err)
					continue
				}

				newVisit := &Visit{
					UUID:               clientVisit.Uuid,
					ClassUUID:          class.Uuid,
					ClassName:          class.Description.Name,
					LevelName:          class.Level.Name,
					StartDatetime:      clientVisit.StartDatetime,
					EndDatetime:        clientVisit.EndDatetime,
					LocationName:       location.Name,
					LateCancelInterval: location.LateCancelInterval,
					StaffFirstName:     staff.FirstName,
					StaffLastName:      staff.LastName,
				}

				response.Items = append(response.Items, newVisit)
			}
		}
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// Service represents a service
type Service struct {
	MBID           int64  `json:"mbId"`
	ProgramMBID    int32  `json:"programMbId"`
	Name           string `json:"name"`
	SiteName       string `json:"siteName"`
	Current        bool   `json:"current"`
	Count          int32  `json:"count"`
	Remaining      int32  `json:"remaining"`
	PaymentDate    string `json:"paymentDate"`
	ActiveDate     string `json:"activeDate"`
	ExpirationDate string `json:"expirationDate"`
}

// GetAccountServicesResponse is a response
type GetAccountServicesResponse struct {
	Items []*Service `json:"items"`
}

// HandleGetAccountServices fetches an account's services
func (s *AccountServer) HandleGetAccountServices(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	var err error

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]
	showActiveOnly := req.URL.Query().Get("showActiveOnly") != ""

	logger.Infof("HandleGetAccountServices for account '%s' (active only: %v)", accountUUID, showActiveOnly)

	response := &GetAccountServicesResponse{Items: []*Service{}}

	// Get the account's clients
	accountClientsResponse, err := s.getAccountClients(accountUUID)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(accountClientsResponse.Clients) > 0 {
		clientUUIDs := []string{}
		for _, accountClient := range accountClientsResponse.Clients {
			clientUUIDs = append(clientUUIDs, accountClient.Client.Uuid)
		}

		logger.Infof("Got %d clients for account '%s'.", len(clientUUIDs), accountUUID)

		for _, dbClientWithSite := range accountClientsResponse.Clients {
			// Get all the programs for this site
			programsResponse, err := s.ClassService.GetProgramsBySiteID(context.Background(), &classPb.GetProgramsBySiteIDRequest{
				SiteId: dbClientWithSite.Site.Id,
			})
			if err != nil {
				logger.Errorf("Error while fetching the programs for site ID %d: %v", dbClientWithSite.Site.Id, err)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}
			programIDs := []*mindbody.CustomInt32{}
			for _, dbProgram := range programsResponse.Programs {
				programIDs = append(programIDs, &mindbody.CustomInt32{Int: dbProgram.MbId})
			}

			mbRequest := mbHelpers.CreateClientMBRequest([]int32{dbClientWithSite.Site.MbId}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)
			mbRequest.PageSize = &mindbody.CustomInt32{Int: 50}
			mbClientServicesRequest := &clientMb.GetClientServicesRequest{
				MBRequest:      mbRequest,
				ClientID:       strconv.FormatInt(dbClientWithSite.Client.MbId, 10),
				ProgramIDs:     &clientMb.ArrayOfInt{Int: programIDs},
				ShowActiveOnly: showActiveOnly,
			}

			mbClientServicesResponse, err := s.ClientMBClient.GetClientServices(&clientMb.GetClientServices{Request: mbClientServicesRequest})
			if err != nil {
				s.Logger.Errorf("Cannot fetch the client services from Mindbody: %v", err)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}
			mbClientServicesResult := mbClientServicesResponse.GetClientServicesResult
			if mbClientServicesResult.ErrorCode.Int != 200 {
				s.Logger.Errorf("Mindbody API response error: %s", mbClientServicesResult.Message)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}

			s.Logger.Infof("Got %d client services from MB for client ID %d.", len(mbClientServicesResult.ClientServices.ClientService), dbClientWithSite.Client.Id)

			if len(mbClientServicesResult.ClientServices.ClientService) > 0 {
				for _, mbService := range mbClientServicesResult.ClientServices.ClientService {
					response.Items = append(response.Items, &Service{
						MBID:           mbService.ID.Int,
						ProgramMBID:    mbService.Program.ID.Int,
						Name:           mbService.Name,
						SiteName:       dbClientWithSite.Site.Name,
						Current:        mbService.Current,
						Count:          mbService.Count.Int,
						Remaining:      mbService.Remaining.Int,
						PaymentDate:    mbService.PaymentDate.Format(sqlDatetimeFormat),
						ActiveDate:     mbService.ActiveDate.Format(sqlDatetimeFormat),
						ExpirationDate: mbService.ExpirationDate.Format(sqlDatetimeFormat),
					})
				}
			}
		}
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// GetAccountScheduleResponse is a response
type GetAccountScheduleResponse struct {
	Items []*Visit `json:"items"`
}

// HandleGetAccountSchedule fetches an account's schedule
func (s *AccountServer) HandleGetAccountSchedule(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	var err error

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]

	var endDate time.Time
	reqEndDate := req.URL.Query().Get("endDate")
	if reqEndDate == "" || reqEndDate == "0" {
		endDate = time.Now().AddDate(0, 0, 1)
	} else {
		endDate, err = time.Parse(sqlDatetimeFormat, reqEndDate)
		if err != nil {
			logger.Errorf("Error while parsing the endDate.")
			res.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	logger.Infof("HandleGetAccountSchedule for account '%s', end date %s", accountUUID, reqEndDate)

	response := &GetAccountScheduleResponse{Items: []*Visit{}}

	// Get the account's clients
	accountClientsResponse, err := s.getAccountClients(accountUUID)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(accountClientsResponse.Clients) > 0 {
		clientUUIDs := []string{}
		for _, accountClient := range accountClientsResponse.Clients {
			clientUUIDs = append(clientUUIDs, accountClient.Client.Uuid)
		}

		logger.Infof("Got %d clients for account '%s'.", len(clientUUIDs), accountUUID)

		for _, dbClientWithSite := range accountClientsResponse.Clients {
			mbRequest := mbHelpers.CreateClientMBRequest([]int32{dbClientWithSite.Site.MbId}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)
			mbRequest.PageSize = &mindbody.CustomInt32{Int: 50}
			mbClientScheduleRequest := &clientMb.GetClientScheduleRequest{
				MBRequest: mbRequest,
				ClientID:  strconv.FormatInt(dbClientWithSite.Client.MbId, 10),
				StartDate: &mindbody.CustomTime{Time: time.Now()},
				EndDate:   &mindbody.CustomTime{Time: endDate},
			}

			mbClientScheduleResponse, err := s.ClientMBClient.GetClientSchedule(&clientMb.GetClientSchedule{Request: mbClientScheduleRequest})
			if err != nil {
				s.Logger.Errorf("Cannot fetch the client schedule from Mindbody: %v", err)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}
			mbClientScheduleResult := mbClientScheduleResponse.GetClientScheduleResult
			if mbClientScheduleResult.ErrorCode.Int != 200 {
				s.Logger.Errorf("Mindbody API response error: %s", mbClientScheduleResult.Message)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}

			s.Logger.Infof("Got %d client visits from MB for client ID %d.", len(mbClientScheduleResult.Visits.Visit), dbClientWithSite.Client.Id)

			if len(mbClientScheduleResult.Visits.Visit) > 0 {
				locationsMap := map[int32]*sitePb.Location{}
				staffMap := map[int64]*staffPb.Staff{}
				classesMap := map[int32]*classPb.Class{}

				locationIDs := []int32{}
				staffIDs := []int64{}
				classIDs := []int32{}

				for _, mbVisit := range mbClientScheduleResult.Visits.Visit {
					if !slices.Icontains32(locationIDs, mbVisit.Location.ID.Int) {
						locationIDs = append(locationIDs, mbVisit.Location.ID.Int)
					}
					if !slices.Icontains64(staffIDs, mbVisit.Staff.ID.Int) {
						staffIDs = append(staffIDs, mbVisit.Staff.ID.Int)
					}
					if !slices.Icontains32(classIDs, mbVisit.ClassID.Int) {
						classIDs = append(classIDs, mbVisit.ClassID.Int)
					}
				}

				ctx := context.Background()

				// Fetch the classes
				classes, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{MbIds: classIDs, SiteId: dbClientWithSite.Site.Id})
				if err != nil {
					logger.Errorf("Error while fetching the classes: %v", err)
					res.WriteHeader(http.StatusInternalServerError)
					return
				}
				for _, dbClass := range classes.Classes {
					classesMap[dbClass.MbId] = dbClass
				}

				// Fetch the staff
				staff, err := s.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{MbIds: staffIDs, SiteId: dbClientWithSite.Site.Id})
				if err != nil {
					logger.Errorf("Error while fetching the staff members: %v", err)
					res.WriteHeader(http.StatusInternalServerError)
					return
				}
				for _, dbStaff := range staff.Staff {
					staffMap[dbStaff.MbId] = dbStaff
				}

				// Fetch the locations
				locations, err := s.SiteService.GetSitesLocations(ctx, &sitePb.GetSitesLocationsRequest{Uuids: dbClientWithSite.Site.Uuid})
				if err != nil {
					logger.Errorf("Error while fetching the locations: %v", err)
					res.WriteHeader(http.StatusInternalServerError)
					return
				}
				for _, dbLocation := range locations.Locations {
					locationsMap[dbLocation.MbId] = dbLocation
				}

				for _, mbVisit := range mbClientScheduleResult.Visits.Visit {
					location, ok := locationsMap[mbVisit.Location.ID.Int]
					if !ok {
						logger.Errorf("Location ID %d not found for client visit MB ID %d: %v", mbVisit.Location.ID.Int, mbVisit.ID.Int, err)
						continue
					}

					staff, ok := staffMap[mbVisit.Staff.ID.Int]
					if !ok {
						logger.Errorf("Staff ID %d not found for client visit ID %d: %v", mbVisit.Staff.ID.Int, mbVisit.ID.Int, err)
						continue
					}

					class, ok := classesMap[mbVisit.ClassID.Int]
					if !ok {
						logger.Errorf("Class ID %d not found for client visit ID %d: %v", mbVisit.ClassID.Int, mbVisit.ID.Int, err)
						continue
					}

					newVisit := &Visit{
						ClassUUID:          class.Uuid,
						ClassName:          class.Description.Name,
						LevelName:          class.Level.Name,
						StartDatetime:      mbVisit.StartDateTime.Time.Format(sqlDatetimeFormat),
						EndDatetime:        mbVisit.EndDateTime.Time.Format(sqlDatetimeFormat),
						LocationName:       location.Name,
						LateCancelInterval: location.LateCancelInterval,
						StaffFirstName:     staff.FirstName,
						StaffLastName:      staff.LastName,
						HideCancel:         class.HideCancel,
					}

					response.Items = append(response.Items, newVisit)
				}
			}
		}
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// MembershipsList represents a list of account memberships
type MembershipsList struct {
	Items []*accountPb.AccountMembership `json:"items"`
}

// HandleGetAccountMemberships fetches an account's memberships
func (s *AccountServer) HandleGetAccountMemberships(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	var err error
	ctx := context.Background()

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]

	logger.Infof("HandleGetAccountMemberships for account '%s'", accountUUID)

	account, err := s.AccountService.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			s.Logger.Infof("Account UUID '%s' was not found.", accountUUID)
			res.WriteHeader(http.StatusNotFound)
		} else {
			s.Logger.Errorf("Error while fetching the account '%s': %v", accountUUID, err)
			res.WriteHeader(http.StatusInternalServerError)
		}

		return
	}

	accountMemberships, err := s.AccountService.GetAccountMemberships(ctx, &accountPb.AccountMembership{AccountId: account.Id})
	if err != nil {
		s.Logger.Errorf("Error while fetching the account memberships for ID %d: %v", account.Id, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	memberships, err := s.SaleService.GetMemberships(ctx, &salePb.GetMembershipsRequest{})
	if err != nil {
		s.Logger.Errorf("Error while fetching the memberships list: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	membershipsMap := map[int32]*salePb.Membership{}
	for _, membership := range memberships.Memberships {
		membershipsMap[membership.Id] = membership
	}

	response := &MembershipsList{}

	for _, accountMembership := range accountMemberships.Memberships {
		if memb, ok := membershipsMap[accountMembership.MembershipId]; ok {
			accountMembership.Membership = memb

			response.Items = append(response.Items, accountMembership)
		} else {
			s.Logger.Warningf("Could not find membership for ID %d. Skipping from listing.", accountMembership.MembershipId)
		}
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// HandleCreateAccountMembership adds a membership to an account
func (s *AccountServer) HandleCreateAccountMembership(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	var err error
	ctx := context.Background()

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]
	membershipUUID := vars["membership_uuid"]

	logger.Infof("HandleCreateAccountMembership for account '%s', membership '%s'", accountUUID, membershipUUID)

	account, err := s.AccountService.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			s.Logger.Infof("Account UUID '%s' was not found.", accountUUID)
			res.WriteHeader(http.StatusNotFound)
		} else {
			s.Logger.Errorf("Error while fetching the account '%s': %v", accountUUID, err)
			res.WriteHeader(http.StatusInternalServerError)
		}

		return
	}

	memberships, err := s.SaleService.GetMemberships(ctx, &salePb.GetMembershipsRequest{Uuids: membershipUUID})
	if err != nil {
		s.Logger.Errorf("Error while fetching the membership %s from DB: %v", membershipUUID, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(memberships.Memberships) == 0 {
		s.Logger.Errorf("Membership '%s' not found in DB.", membershipUUID)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	membership := memberships.Memberships[0]

	remaining := membership.Count
	if remaining == 0 {
		remaining = -1
	}
	validUntil := "0"
	if membership.ValidForDays > 0 {
		validUntil = time.Now().Add(time.Hour * 24 * time.Duration(membership.ValidForDays)).Format(sqlDatetimeFormat)
	}

	accountMembership, err := s.AccountService.CreateAccountMembership(ctx, &accountPb.AccountMembership{
		AccountId:    account.Id,
		MembershipId: membership.Id,
		Remaining:    remaining,
		ValidUntil:   validUntil,
	})
	if err != nil {
		s.Logger.Errorf("Error while creating the account membership: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	accountMembership.Membership = membership

	s.writeJSONResponse(res, accountMembership)
	res.WriteHeader(http.StatusOK)
}

// ExpireAccountServiceRequest represends a HTTP request
type ExpireAccountServiceRequest struct {
	SiteUUID string `json:"siteUuid"`
}

// HandleExpireAccountService expires a client service in MB
func (s *AccountServer) HandleExpireAccountService(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	var err error
	ctx := context.Background()

	vars := mux.Vars(req)
	serviceID, err := strconv.ParseInt(vars["service_id"], 10, 32)
	if err != nil {
		logger.Errorf("Cannot parse the service ID to int: %v", err)
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	var request ExpireAccountServiceRequest
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&request)
	defer req.Body.Close()
	if err != nil {
		logger.Errorf("Cannot parse the request body JSON: %v", err)
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	logger.Infof("HandleExpireAccountService for site '%s', service ID '%d'", request.SiteUUID, serviceID)

	// Validations
	if request.SiteUUID == "" {
		logger.Error("Empty site UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	site, err := s.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Uuid: request.SiteUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			s.Logger.Infof("Site UUID '%s' was not found.", request.SiteUUID)
			res.WriteHeader(http.StatusNotFound)
		} else {
			s.Logger.Errorf("Error while fetching the site '%s': %v", request.SiteUUID, err)
			res.WriteHeader(http.StatusInternalServerError)
		}

		return
	}

	credsUsername := "_" + s.MindbodyConfig.SourceName
	credsPassword := s.MindbodyConfig.SourcePassword
	// This is an exception ONLY for the Sandbox Site, where we cannot authorize our master API creds
	if site.MbId == -99 {
		credsUsername = "Siteowner"
		credsPassword = "apitest1234"
	}

	mbRequest := mbHelpers.CreateClientMBRequest([]int32{site.MbId}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)
	mbRequest.UserCredentials = &clientMb.UserCredentials{Username: credsUsername, Password: credsPassword, SiteIDs: mbRequest.SourceCredentials.SiteIDs}
	mbUpdateClientServicesRequest := &clientMb.UpdateClientServicesRequest{
		MBRequest: mbRequest,
		ClientServices: &clientMb.ArrayOfClientService{
			ClientService: []*clientMb.ClientService{
				&clientMb.ClientService{
					ID:             &mindbody.CustomInt64{Int: serviceID},
					ActiveDate:     &mindbody.CustomTime{Time: time.Now().AddDate(0, 0, -2)},
					ExpirationDate: &mindbody.CustomTime{Time: time.Now().AddDate(0, 0, -1)},
				},
			},
		},
	}

	mbUpdateClientServiceResponse, err := s.ClientMBClient.UpdateClientServices(&clientMb.UpdateClientServices{Request: mbUpdateClientServicesRequest})
	if err != nil {
		s.Logger.Errorf("Cannot update the client service in Mindbody: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	mbUpdateClientServiceResult := mbUpdateClientServiceResponse.UpdateClientServicesResult
	if mbUpdateClientServiceResult.ErrorCode.Int != 200 {
		s.Logger.Errorf("Mindbody API response error: %s", mbUpdateClientServiceResult.Message)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusNoContent)
}

// RegisterRoutes registers the routes to a mux router
func (s *AccountServer) RegisterRoutes(r *mux.Router) {
	r.HandleFunc("/v1/clients", s.HandleCreateMBClient).Methods("POST")
	r.HandleFunc("/v1/accounts/{account_uuid}/clients", s.HandleGetAccountClients).Methods("GET")
	r.HandleFunc("/v1/clients/{client_uuid}/services/class", s.HandleGetClientServices).Methods("GET")
	r.HandleFunc("/v1/clients/{client_uuid}/services/class/{class_uuid}", s.HandleGetClientServices).Methods("GET")
	r.HandleFunc("/v1/clients/search/{email}", s.HandleSearchClientsByEmail).Methods("GET")
	r.HandleFunc("/v1/accounts/{username}/instagram/media", s.HandleGetInstagramItems).Methods("GET")
	r.HandleFunc("/v1/accounts/{account_uuid}/visits", s.HandleGetAccountVisits).Methods("GET")
	r.HandleFunc("/v1/accounts/{account_uuid}/services", s.HandleGetAccountServices).Methods("GET")
	r.HandleFunc("/v1/accounts/services/{service_id}/expire", s.HandleExpireAccountService).Methods("PUT")
	r.HandleFunc("/v1/accounts/{account_uuid}/schedule", s.HandleGetAccountSchedule).Methods("GET")
	r.HandleFunc("/v1/accounts/{account_uuid}/memberships", s.HandleGetAccountMemberships).Methods("GET")
	r.HandleFunc("/v1/accounts/{account_uuid}/memberships/{membership_uuid}", s.HandleCreateAccountMembership).Methods("POST")
}

func (s *AccountServer) writeJSONResponse(res http.ResponseWriter, response interface{}) error {
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		s.Logger.Error("Failed to Marshal the response.")
		res.WriteHeader(http.StatusInternalServerError)
		return err
	}

	res.Write([]byte(jsonResponse))
	return nil
}

func (s *AccountServer) getSitesForClients(clients []*accountPb.Client) (map[int32]*sitePb.Site, error) {
	sites := map[int32]*sitePb.Site{}
	siteIDs := []int32{}
	for _, dbClient := range clients {
		siteIDs = append(siteIDs, dbClient.SiteId)
	}
	sitesResponse, err := s.SiteService.GetSites(context.Background(), &sitePb.GetSitesRequest{Ids: siteIDs})
	if err != nil {
		return nil, fmt.Errorf("error while fetching the sites from DB")
	}
	for _, dbSite := range sitesResponse.Sites {
		sites[dbSite.Id] = dbSite
	}

	return sites, nil
}

func (s *AccountServer) getAccountClients(accountUUID string) (*GetAccountClientsResponse, error) {
	ctx := context.Background()

	response := &GetAccountClientsResponse{}

	accountsClientsResponse, err := s.ClientService.GetAccountsClients(ctx, &accountPb.GetAccountsClientsRequest{AccountUuids: accountUUID})
	if err != nil {
		return nil, err
	}

	if accountClients, ok := accountsClientsResponse.AccountClients[accountUUID]; ok {
		clients := accountClients.Clients

		sites, err := s.getSitesForClients(clients)
		if err != nil {
			return nil, err
		}

		for _, dbClient := range clients {
			site := &sitePb.Site{}
			if _, ok := sites[dbClient.SiteId]; ok {
				site = sites[dbClient.SiteId]
			}

			response.Clients = append(response.Clients, &ClientWithSite{
				Client: dbClient,
				Site:   site,
			})
		}
	}

	return response, nil
}
