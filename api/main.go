package main

import (
	"context"
	"encoding/json"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"google.golang.org/grpc"

	"bitbucket.org/canopei/account"
	"bitbucket.org/canopei/account/config"
	accountPb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/class"
	classPb "bitbucket.org/canopei/class/protobuf"
	cApi "bitbucket.org/canopei/golibs/api"
	cGrpc "bitbucket.org/canopei/golibs/grpc/utils"
	"bitbucket.org/canopei/golibs/healthcheck"
	cHttp "bitbucket.org/canopei/golibs/http"
	"bitbucket.org/canopei/golibs/logging"
	clientMb "bitbucket.org/canopei/mindbody/services/client"
	"bitbucket.org/canopei/sale"
	salePb "bitbucket.org/canopei/sale/protobuf"
	"bitbucket.org/canopei/site"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"bitbucket.org/canopei/staff"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/gorilla/mux"

	"fmt"

	"time"

	pb "bitbucket.org/canopei/account/protobuf"
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/handlers"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/urfave/negroni"
)

var (
	conf           *config.Config
	logger         *logrus.Entry
	siteService    sitePb.SiteServiceClient
	classService   classPb.ClassServiceClient
	staffService   staffPb.StaffServiceClient
	clientService  accountPb.ClientServiceClient
	accountService accountPb.AccountServiceClient
	saleService    salePb.SaleServiceClient
	clientMBClient *clientMb.Client_x0020_ServiceSoap
	version        string
)

func run() error {
	var err error

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := mux.NewRouter()
	mux.HandleFunc(healthcheck.Healthpath, HealthcheckHandler)

	accountServer := NewAccountServer(logger, siteService, clientService, accountService, classService, staffService, saleService, clientMBClient, &conf.Mindbody)
	accountServer.RegisterRoutes(mux)

	gwmux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONBuiltin{}))
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBackoffMaxDelay(8 * time.Second)}

	err = pb.RegisterAccountServiceHandlerFromEndpoint(ctx, gwmux, fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort), opts)
	if err != nil {
		return err
	}
	err = pb.RegisterClientServiceHandlerFromEndpoint(ctx, gwmux, fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort), opts)
	if err != nil {
		return err
	}

	mux.PathPrefix("/").Handler(cGrpc.RestGatewayResponseInterceptor(gwmux))

	n := negroni.New()
	n.Use(cHttp.NewXRequestIDMiddleware(16))
	n.Use(cApi.NewServiceAPIMiddleware(logger))
	n.Use(cApi.NewCombinedLoggingMiddleware(logger, mux, handlers.CombinedLoggingHandler))
	n.Use(negroni.NewRecovery())

	logger.Infof("Serving on %d.", conf.Service.ApiPort)
	return http.ListenAndServe(fmt.Sprintf(":%d", conf.Service.ApiPort), n)
}

func main() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("ACCOUNT_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "api",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	logger.Infof("Booting '%s' API (%s)...", conf.Service.Name, version)

	// prepare the MB SOAP client
	clientMBClient = clientMb.NewClient_x0020_ServiceSoap("", false, nil)

	siteService, _, err = site.NewClient(conf.SiteService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the site service: %v", err)
	}

	clientService, _, err = account.NewClientClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot start the client service: %v", err)
	}

	accountService, _, err = account.NewAccountClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot start the account service: %v", err)
	}

	classService, _, err = class.NewClient(conf.ClassService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the class service: %v", err)
	}

	staffService, _, err = staff.NewClient(conf.StaffService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the staff service: %v", err)
	}

	saleService, _, err = sale.NewClient(conf.SaleService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the staff service: %v", err)
	}

	if err := run(); err != nil {
		logger.Fatal(err)
	}
}

// HealthcheckHandler handle the healthcheck request
func HealthcheckHandler(res http.ResponseWriter, req *http.Request) {
	localLogger := GetRequestLogger(req, logger)

	_, err := clientService.Ping(context.Background(), &empty.Empty{})
	if err != nil {
		localLogger.Errorf("Ping error: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "application/json")
	msg, _ := json.Marshal(map[string]string{"status": "ok", "version": version})
	res.Write([]byte(msg))
	return
}
