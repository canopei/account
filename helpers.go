package account

import (
	accountPb "bitbucket.org/canopei/account/protobuf"
	clientMb "bitbucket.org/canopei/mindbody/services/client"
)

const sqlDatetimeFormat = "2006-01-02 15:04:05"

var GenderToInt = map[string]int32{
	"Female": 1,
	"Male":   2,
}

func hasClientChanged(dbClient *accountPb.Client, mbClient *accountPb.Client) bool {
	return dbClient.FirstName != mbClient.FirstName ||
		dbClient.LastName != mbClient.LastName ||
		dbClient.MiddleName != mbClient.MiddleName ||
		dbClient.Gender != mbClient.Gender ||
		dbClient.Email != mbClient.Email ||
		dbClient.EmailOptin != mbClient.EmailOptin ||
		dbClient.City != mbClient.City ||
		dbClient.Country != mbClient.Country ||
		dbClient.State != mbClient.State ||
		dbClient.Inactive != mbClient.Inactive ||
		dbClient.IsCompany != mbClient.IsCompany ||
		dbClient.Status != mbClient.Status ||
		dbClient.Username != mbClient.Username
}

func hasClientPurchaseChanged(dbClientPurchase *accountPb.ClientPurchase, mbClientPurchase *accountPb.ClientPurchase) bool {
	return dbClientPurchase.Price != mbClientPurchase.Price ||
		dbClientPurchase.Description != mbClientPurchase.Description ||
		dbClientPurchase.AmountPaid != mbClientPurchase.AmountPaid ||
		dbClientPurchase.Discount != mbClientPurchase.Discount ||
		dbClientPurchase.Tax != mbClientPurchase.Tax ||
		dbClientPurchase.Returned != mbClientPurchase.Returned ||
		dbClientPurchase.Quantity != mbClientPurchase.Quantity ||
		dbClientPurchase.SaleTime != mbClientPurchase.SaleTime ||
		dbClientPurchase.PaymentId != mbClientPurchase.PaymentId ||
		dbClientPurchase.PaymentMethod != mbClientPurchase.PaymentMethod ||
		dbClientPurchase.PaymentType != mbClientPurchase.PaymentType ||
		dbClientPurchase.PaymentNotes != mbClientPurchase.PaymentNotes
}

func MbClientToPbClient(mbClient *clientMb.Client) *accountPb.Client {
	gender := int32(0)
	if val, ok := GenderToInt[mbClient.Gender]; ok {
		gender = val
	}

	return &accountPb.Client{
		MbId:       mbClient.UniqueID.Int,
		FirstName:  mbClient.FirstName,
		MiddleName: mbClient.MiddleName,
		LastName:   mbClient.LastName,
		Gender:     gender,
		Email:      mbClient.Email,
		EmailOptin: mbClient.EmailOptIn,
		City:       mbClient.City,
		Country:    mbClient.Country,
		State:      mbClient.State,
		Inactive:   mbClient.Inactive,
		IsCompany:  mbClient.IsCompany,
		Status:     mbClient.Status,
		Username:   mbClient.Username,
	}
}

func mbClientPurchaseToPbClientPurchase(mbClientPurchase *clientMb.SaleItem) *accountPb.ClientPurchase {
	return &accountPb.ClientPurchase{
		SaleMbId:      mbClientPurchase.Sale.ID.Int,
		Description:   mbClientPurchase.Description,
		Price:         mbClientPurchase.Price.Float,
		AmountPaid:    mbClientPurchase.AmountPaid.Float,
		Discount:      mbClientPurchase.Discount.Float,
		Tax:           mbClientPurchase.Tax.Float,
		Returned:      mbClientPurchase.Returned,
		Quantity:      mbClientPurchase.Quantity.Int,
		SaleTime:      mbClientPurchase.Sale.SaleDateTime.Format(sqlDatetimeFormat),
		PaymentId:     mbClientPurchase.Sale.Payments.Payment[0].ID.Int,
		PaymentMethod: mbClientPurchase.Sale.Payments.Payment[0].Method.Int,
		PaymentType:   mbClientPurchase.Sale.Payments.Payment[0].Type,
		PaymentNotes:  mbClientPurchase.Sale.Payments.Payment[0].Notes,
	}
}

func mbClientVisitToPbClientVisit(mbClientVisit *clientMb.Visit) *accountPb.ClientVisit {
	pbClientVisit := &accountPb.ClientVisit{
		MbId:          mbClientVisit.ID.Int,
		Name:          mbClientVisit.Name,
		LateCancelled: mbClientVisit.LateCancelled,
		MakeUp:        mbClientVisit.MakeUp,
		WebSignup:     mbClientVisit.WebSignup,
		SignedIn:      mbClientVisit.SignedIn,
		StartDatetime: mbClientVisit.StartDateTime.Format(sqlDatetimeFormat),
		EndDatetime:   mbClientVisit.EndDateTime.Format(sqlDatetimeFormat),
	}

	if mbClientVisit.Service != nil {
		pbClientVisit.ServiceMbId = mbClientVisit.Service.ID.Int
		pbClientVisit.ServiceCount = mbClientVisit.Service.Count.Int
		pbClientVisit.ServiceRemaining = mbClientVisit.Service.Remaining.Int
	}

	return pbClientVisit
}

func hasClientVisitChanged(dbClientVisit *accountPb.ClientVisit, mbClientVisit *accountPb.ClientVisit) bool {
	return dbClientVisit.Name != mbClientVisit.Name ||
		dbClientVisit.ServiceMbId != mbClientVisit.ServiceMbId ||
		dbClientVisit.ServiceCount != mbClientVisit.ServiceCount ||
		dbClientVisit.ServiceRemaining != mbClientVisit.ServiceRemaining ||
		dbClientVisit.LateCancelled != mbClientVisit.LateCancelled ||
		dbClientVisit.MakeUp != mbClientVisit.MakeUp ||
		dbClientVisit.WebSignup != mbClientVisit.WebSignup ||
		dbClientVisit.SignedIn != mbClientVisit.SignedIn ||
		dbClientVisit.StartDatetime != mbClientVisit.StartDatetime ||
		dbClientVisit.EndDatetime != mbClientVisit.EndDatetime
}
