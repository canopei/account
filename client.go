package account

import (
	"fmt"

	pb "bitbucket.org/canopei/account/protobuf"
	"google.golang.org/grpc"
)

// NewAccountClient returns a gRPC client for interacting with the Account service.
func NewAccountClient(addr string) (pb.AccountServiceClient, func() error, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, fmt.Errorf("did not connect: %v", err)
	}

	return pb.NewAccountServiceClient(conn), conn.Close, nil
}

// NewClientClient returns a gRPC client for interacting with the Client service.
func NewClientClient(addr string) (pb.ClientServiceClient, func() error, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, fmt.Errorf("did not connect: %v", err)
	}

	return pb.NewClientServiceClient(conn), conn.Close, nil
}
