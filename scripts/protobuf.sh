#!/bin/sh
set -e

cd ./protobuf

for service in account client; do
    echo "Generating the files for service '${service}'..."

    # the pb file
    protoc \
        -I .\
        -I ../vendor \
        -I ../vendor/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
        --gogo_out=Mgoogle/api/annotations.proto=github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis/google/api,plugins=grpc:. \
        "${service}.proto"

    # the gateway
    protoc \
        -I .\
        -I ../vendor \
        -I ../vendor/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
        --grpc-gateway_out=logtostderr=true:. \
        "${service}.proto"
done

# we don't want to omit empties in JSON, except for the ones we really want (omitempty!)
sed -i 's/,omitempty!/,omt!/g' *.pb.go
sed -i 's/,omitempty//g' *.pb.go
sed -i 's/omt!/omitempty/g' *.pb.go

echo "Done."