package account

import (
	"time"

	"github.com/stretchr/testify/assert"

	"testing"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/mindbody"
	clientMb "bitbucket.org/canopei/mindbody/services/client"
)

// Just a simple sanity check
func TestHasClientChanged(t *testing.T) {
	assert := assert.New(t)

	dbClient := &accountPb.Client{FirstName: "foo"}
	mbClient := &accountPb.Client{FirstName: "bar"}
	assert.True(hasClientChanged(dbClient, mbClient))

	dbClient = &accountPb.Client{FirstName: "foo"}
	mbClient = &accountPb.Client{FirstName: "foo"}
	assert.False(hasClientChanged(dbClient, mbClient))
}

// Test the gender conversion to int
func TestMbClientToPbClient(t *testing.T) {
	assert := assert.New(t)

	mbClientF := &clientMb.Client{UniqueID: &mindbody.CustomInt64{Int: 123}, Gender: "Female"}
	result := MbClientToPbClient(mbClientF)
	assert.Equal(int32(1), result.Gender)

	mbClientM := &clientMb.Client{UniqueID: &mindbody.CustomInt64{Int: 123}, Gender: "Male"}
	result = MbClientToPbClient(mbClientM)
	assert.Equal(int32(2), result.Gender)

	mbClientUnknown := &clientMb.Client{UniqueID: &mindbody.CustomInt64{Int: 123}, Gender: "Foo"}
	result = MbClientToPbClient(mbClientUnknown)
	assert.Equal(int32(0), result.Gender)
}

// Just a simple sanity check
func TestHasClientPurchaseChanged(t *testing.T) {
	assert := assert.New(t)

	dbClientPurchase := &accountPb.ClientPurchase{Returned: true}
	mbClientPurchase := &accountPb.ClientPurchase{Returned: false}
	assert.True(hasClientPurchaseChanged(dbClientPurchase, mbClientPurchase))

	dbClientPurchase = &accountPb.ClientPurchase{Returned: true}
	mbClientPurchase = &accountPb.ClientPurchase{Returned: true}
	assert.False(hasClientPurchaseChanged(dbClientPurchase, mbClientPurchase))
}

func TestMbClientPurchaseToPbClientPurchase(t *testing.T) {
	assert := assert.New(t)

	mbClientPurchase := &clientMb.SaleItem{
		Returned:   false,
		Price:      &mindbody.CustomFloat64{Float: 12.234},
		AmountPaid: &mindbody.CustomFloat64{Float: 12.234},
		Discount:   &mindbody.CustomFloat64{Float: -12.234},
		Tax:        &mindbody.CustomFloat64{Float: 12.234},
		Quantity:   &mindbody.CustomInt32{Int: 1},
		Sale: &clientMb.Sale{
			ID: &mindbody.CustomInt64{Int: 12},
			Payments: &clientMb.ArrayOfPayment{Payment: []*clientMb.Payment{
				&clientMb.Payment{
					ID:     &mindbody.CustomInt64{Int: 12},
					Type:   "cart",
					Method: &mindbody.CustomInt32{Int: 12},
					Notes:  "notes",
				},
			}},
			SaleDateTime: &mindbody.CustomTime{Time: time.Now()},
		},
	}
	result := mbClientPurchaseToPbClientPurchase(mbClientPurchase)
	assert.Equal(float64(12.234), result.Price)
	assert.Equal(false, result.Returned)
	assert.Equal(int64(12), result.PaymentId)
}

func TestMbClientVisitToPbClientVisit(t *testing.T) {
	assert := assert.New(t)

	mbClientVisit := &clientMb.Visit{
		ID:   &mindbody.CustomInt64{Int: 123},
		Name: "name",
		Service: &clientMb.Service{
			ClientService: &clientMb.ClientService{
				ID:        &mindbody.CustomInt64{Int: 234},
				Count:     &mindbody.CustomInt32{Int: 5},
				Remaining: &mindbody.CustomInt32{Int: 2},
			},
		},
		LateCancelled: false,
		MakeUp:        true,
		WebSignup:     false,
		SignedIn:      true,
		StartDateTime: &mindbody.CustomTime{Time: time.Now()},
		EndDateTime:   &mindbody.CustomTime{Time: time.Now()},
	}

	result := mbClientVisitToPbClientVisit(mbClientVisit)
	assert.Equal("name", result.Name)
	assert.Equal(false, result.LateCancelled)
	assert.Equal(true, result.MakeUp)
	assert.Equal(int32(2), result.ServiceRemaining)
}

func TestHasClientVisitChanged(t *testing.T) {
	assert := assert.New(t)

	dbClientVisit := &accountPb.ClientVisit{LateCancelled: true}
	mbClientVisit := &accountPb.ClientVisit{LateCancelled: false}
	assert.True(hasClientVisitChanged(dbClientVisit, mbClientVisit))

	dbClientVisit = &accountPb.ClientVisit{LateCancelled: true}
	mbClientVisit = &accountPb.ClientVisit{LateCancelled: true}
	assert.False(hasClientVisitChanged(dbClientVisit, mbClientVisit))
}
