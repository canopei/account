package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"time"

	"bitbucket.org/canopei/account"
	"bitbucket.org/canopei/account/config"
	accountPb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/class"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/logging"
	clientMb "bitbucket.org/canopei/mindbody/services/client"
	"bitbucket.org/canopei/site"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"bitbucket.org/canopei/staff"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	_ "github.com/go-sql-driver/mysql"
	nsq "github.com/nsqio/go-nsq"
	"github.com/robfig/cron"
)

var (
	conf                 *config.Config
	logger               *logrus.Entry
	clientMBClient       *clientMb.Client_x0020_ServiceSoap
	queue                *nsq.Producer
	elasticsearchService *elasticsearch.Service
	clientService        accountPb.ClientServiceClient
	accountService       accountPb.AccountServiceClient
	siteService          sitePb.SiteServiceClient
	staffService         staffPb.StaffServiceClient
	classService         classPb.ClassServiceClient
	version              string
)

func main() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("ACCOUNT_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "sync",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	logger.Infof("Booting '%s' sync (%s)...", conf.Service.Name, version)

	// prepare the MB SOAP client
	clientMBClient = clientMb.NewClient_x0020_ServiceSoap("", false, nil)

	// connect to the Client service
	clientService, _, err = account.NewClientClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot connect to the Client service: %v", err)
	}

	// connect to the Account service
	accountService, _, err = account.NewAccountClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot connect to the Account service: %v", err)
	}

	// connect to the Site service
	siteService, _, err = site.NewClient(conf.SiteService.Addr)
	if err != nil {
		logger.Fatalf("Cannot connect to the Site service: %v", err)
	}

	// connect to the Staff service
	staffService, _, err = staff.NewClient(conf.StaffService.Addr)
	if err != nil {
		logger.Fatalf("Cannot connect to the Staff service: %v", err)
	}

	// connect to the Class service
	classService, _, err = class.NewClient(conf.ClassService.Addr)
	if err != nil {
		logger.Fatalf("Cannot connect to the Class service: %v", err)
	}

	// connect as a producer to the queue service
	logger.Infof("Connecting to the queue service at %s", conf.Queue.Addr)
	cfg := nsq.NewConfig()
	queue, err = nsq.NewProducer(conf.Queue.Addr, cfg)
	if err != nil {
		logger.Fatalf("Cannot start the queue Producer: %v", err)
	}
	queue.SetLogger(logging.NewNSQLogrusLogger(logger), nsq.LogLevelInfo)

	// initialize the ES service
	logger.Infof("Connecting to the ES service at %s", conf.Elasticsearch.Addr)
	elasticsearchService, err = elasticsearch.NewService(&conf.Elasticsearch, logging.CloneLogrusEntry(logger))
	if err != nil {
		logger.Fatalf("Cannot initialize the Elasticsearch service: %v", err)
	}

	// Configure the NSQ consumer
	consumerConfig := nsq.NewConfig()
	consumerConfig.MaxInFlight = conf.Sync.MaxInFlight
	consumerConfig.LookupdPollInterval = time.Duration(conf.Sync.ReconnectInterval) * time.Second

	q, _ := nsq.NewConsumer(conf.Queue.SyncTopic, "sync_account", consumerConfig)
	q.SetLogger(logging.NewNSQLogrusLogger(logger), nsq.LogLevelInfo)

	// Create and start the workers
	logger.Infof("Starting workers %d on '%s'", conf.Sync.Workers, conf.Queue.Addr)
	for i := 1; i <= conf.Sync.Workers; i++ {
		worker := CreateWorker(i)
		worker.Start()
		q.AddHandler(nsq.HandlerFunc(worker.HandleMessage))
	}

	err = q.ConnectToNSQD(conf.Queue.Addr)
	if err != nil {
		logger.Fatal("Could not connect to NSQd")
	}

	logger.Info("Workers started.")

	c := cron.New()
	c.AddFunc(conf.Sync.HistorySyncCronTime, syncHistoryJob)
	c.Start()
	logger.Info("Sync scheduled.")

	// Gracefully handle SIGINT and SIGTERM
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		case <-q.StopChan:
			return
		case <-sigChan:
			q.Stop()
		}
	}
}

// CreateWorker creates a new Worker instance
func CreateWorker(id int) *Worker {
	return NewWorker(id, conf, logger, clientMBClient, clientService, accountService, siteService, staffService, classService, queue, elasticsearchService)
}

func syncHistoryJob() {
	result, err := accountService.GetAccountsToSync(context.Background(), &accountPb.GetAccountsToSyncRequest{
		SyncInterval: int32(86400), // Don't sync more often than once per day
		Limit:        50,           // Sync max 50 accounts per day (~100-200 requests in MB)
	})
	if err != nil {
		logger.Errorf("Error while fetching the accounts to sync.")
		return
	}

	if len(result.Accounts) == 0 {
		logger.Info("Got no accounts to sync.")
		return
	}

	accountsUuids := []string{}
	for _, account := range result.Accounts {
		accountsUuids = append(accountsUuids, account.Uuid)
	}

	logger.Infof("[Job] Got %d accounts to sync: %s", len(result.Accounts), strings.Join(accountsUuids, ", "))

	accountService.SyncAccountsClientsWithMB(context.Background(), &accountPb.SyncAccountsClientsWithMBRequest{Uuids: accountsUuids})
}
