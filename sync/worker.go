package main

import (
	"encoding/json"
	"strings"
	"sync"

	"time"

	"bitbucket.org/canopei/account"
	"bitbucket.org/canopei/account/config"
	accountPb "bitbucket.org/canopei/account/protobuf"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/logging"
	queueHelper "bitbucket.org/canopei/golibs/queue"
	"bitbucket.org/canopei/golibs/slices"
	clientMb "bitbucket.org/canopei/mindbody/services/client"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
)

// Worker is a queue consumer worker implementation
type Worker struct {
	ID                   int
	Config               *config.SyncConfig
	Logger               *logrus.Entry
	ClientMBClient       *clientMb.Client_x0020_ServiceSoap
	Queue                *nsq.Producer
	ElasticsearchService *elasticsearch.Service
	SyncService          *account.SyncService
	ClientService        accountPb.ClientServiceClient
	AccountService       accountPb.AccountServiceClient
	SiteService          sitePb.SiteServiceClient

	Buffers map[string]chan ChanEntry
	Locks   map[string]*sync.Mutex
}

// ChanEntry represents a channel entry containing the original NSQ message and a sync message
type ChanEntry struct {
	NSQMessage  *nsq.Message
	SyncMessage *queueHelper.SyncMessage
}

// NewWorker creates a new Worker instance
func NewWorker(
	id int,
	config *config.Config,
	logger *logrus.Entry,
	clientMBClient *clientMb.Client_x0020_ServiceSoap,
	clientService accountPb.ClientServiceClient,
	accountService accountPb.AccountServiceClient,
	siteService sitePb.SiteServiceClient,
	staffService staffPb.StaffServiceClient,
	classService classPb.ClassServiceClient,
	queue *nsq.Producer,
	elasticsearchService *elasticsearch.Service,
) *Worker {
	workerLogger := logging.CloneLogrusEntry(logger)
	workerLogger.Data["workerID"] = id

	// Get a new SyncService
	syncService := account.NewSyncService(workerLogger, clientMBClient, queue, elasticsearchService, accountService, clientService, siteService, staffService, classService, &config.Mindbody)

	return &Worker{
		ID:                   id,
		Config:               &config.Sync,
		Logger:               workerLogger,
		ClientMBClient:       clientMBClient,
		ClientService:        clientService,
		AccountService:       accountService,
		SiteService:          siteService,
		Queue:                queue,
		ElasticsearchService: elasticsearchService,
		SyncService:          syncService,
	}
}

// HandleMessage handles any queue message
func (w *Worker) HandleMessage(message *nsq.Message) error {
	// Do not auto-finish or requeue messages
	message.DisableAutoResponse()

	// If we cannot unmarshal it into a generic queue message, then drop it
	syncMessage := &queueHelper.SyncMessage{}
	err := json.Unmarshal(message.Body, syncMessage)
	if err != nil {
		w.Logger.Warningf("Unable to unmarshal message: %v", message)
		message.Finish()
		return nil
	}

	// Push the message to the according channel
	w.Logger.Debugf("Got message with type '%s'.", syncMessage.ObjectType)
	switch syncMessage.ObjectType {
	case queueHelper.ObjectTypeSitesClients:
		fallthrough
	case queueHelper.ObjectTypeAccountClientsHistory:
		w.Buffers[string(syncMessage.ObjectType)] <- ChanEntry{NSQMessage: message, SyncMessage: syncMessage}
	default:
		// If we don't recognize the type, warning and drop the message.
		w.Logger.Warningf("Skipping message with type '%s'.", syncMessage.ObjectType)
		message.Finish()
	}

	return nil
}

// Start makes a worker process the channels
func (w *Worker) Start() {
	w.Buffers = map[string]chan ChanEntry{}

	siteClientsChannelName := string(queueHelper.ObjectTypeSitesClients)
	accountsClientsHistoryChannelName := string(queueHelper.ObjectTypeAccountClientsHistory)

	channels := map[string]func([]ChanEntry){
		siteClientsChannelName:            w.ProcessSiteClients,
		accountsClientsHistoryChannelName: w.ProcessAccountsClientsHistory,
	}

	w.startQueueProcessing(siteClientsChannelName, channels[siteClientsChannelName])
	w.startQueueBatchProcessing(accountsClientsHistoryChannelName, channels[accountsClientsHistoryChannelName])
}

func (w *Worker) startQueueProcessing(name string, process func([]ChanEntry)) {
	w.Locks = map[string]*sync.Mutex{}
	w.Buffers[name] = make(chan ChanEntry, w.Config.MaxBatchSize*3)

	go func() {
		for {
			select {
			case f := <-w.Buffers[name]:
				process([]ChanEntry{f})
			}
		}
	}()
}

func (w *Worker) startQueueBatchProcessing(name string, process func([]ChanEntry)) {
	// Initialize
	w.Locks[name] = &sync.Mutex{}
	w.Buffers[name] = make(chan ChanEntry, w.Config.MaxBatchSize*3)

	go func() {
		batch := []ChanEntry{}
		ticker := time.NewTicker(time.Duration(w.Config.BatchInterval) * time.Second)
		tickerC := ticker.C

		for {
			// If we hit the batch limit or the ticker ticks, process the batch
			select {
			case f := <-w.Buffers[name]:
				batch = append(batch, f)
				if len(batch) >= w.Config.MaxBatchSize {
					process(batch)

					w.Locks[name].Lock()
					batch = []ChanEntry{}
					w.Locks[name].Unlock()
				}

				// Reset the ticker
				ticker = time.NewTicker(time.Duration(w.Config.BatchInterval) * time.Second)
				tickerC = ticker.C
			case <-tickerC:
				w.Locks[name].Lock()
				if len(batch) > 0 {
					process(batch)
					batch = []ChanEntry{}
				}
				w.Locks[name].Unlock()
			}
		}
	}()
}

// ProcessSiteClients processes one Staff message (we will get one entry at a time)
func (w *Worker) ProcessSiteClients(entries []ChanEntry) {
	entry := entries[0]
	siteUUID := entry.SyncMessage.Object.ID

	w.Logger.Infof("Processing sync for site clients '%s'", siteUUID)
	entry.NSQMessage.Finish()

	err := w.SyncService.SyncSiteClients(siteUUID)
	if err != nil {
		w.Logger.Errorf("Failed to sync clients for site '%s'. Requeuing...: %v", siteUUID, err)
		entry.NSQMessage.Requeue(30 * time.Second)
		return
	}
}

// ProcessAccountsClientsHistory processes multiple accounts clients history
func (w *Worker) ProcessAccountsClientsHistory(entries []ChanEntry) {
	w.Logger.Debugf("Processing clients history for %d accounts.", len(entries))

	var accountUUIDs []string
	messagesMap := map[string][]ChanEntry{}
	for _, entry := range entries {
		entry.NSQMessage.Finish()

		accountUUID := entry.SyncMessage.Object.ID
		// dedup
		if !slices.Scontains(accountUUIDs, accountUUID) {
			accountUUIDs = append(accountUUIDs, accountUUID)
			messagesMap[accountUUID] = append(messagesMap[accountUUID], entry)
		}
	}

	w.Logger.Infof("Processing clients history sync for accounts: %s", strings.Join(accountUUIDs, ", "))

	messagesToFinish1, err := w.SyncService.SyncAccountsClientsPurchasesHistory(accountUUIDs)
	messagesToFinish2, err := w.SyncService.SyncAccountsClientsVisitsHistory(accountUUIDs)

	// the intersection of the two
	finalMessagesToFinish := []string{}
	fullStr := strings.Join(messagesToFinish1, " ")
	for _, msg := range messagesToFinish2 {
		if strings.Contains(fullStr, msg) {
			finalMessagesToFinish = append(finalMessagesToFinish, msg)
		}
	}

	if err == nil {
		w.Logger.Infof("Will finish AccountsClientsHistory messages for: %s", strings.Join(finalMessagesToFinish, ", "))
		for _, entry := range entries {
			accountUUID := entry.SyncMessage.Object.ID

			if !slices.Scontains(finalMessagesToFinish, accountUUID) {
				if entry.NSQMessage.Attempts < 3 {
					entry.NSQMessage.Requeue(time.Second * 180)
				}
			}
		}
	}
}
